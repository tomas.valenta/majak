from celery import shared_task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task
def update_calendar_source(calendar_id):
    from .models import Calendar  # noqa circular import

    cal = Calendar.objects.get(id=calendar_id)
    cal.update_source()
