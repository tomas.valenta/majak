from django.apps import AppConfig


class CalendarUtilsConfig(AppConfig):
    name = "calendar_utils"
