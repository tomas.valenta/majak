from django import template

register = template.Library()


def event_list(calendar, full_list):
    """
    Outputs a list of events, in case the calendar is on calendar page, it will print all future events,
    otherwise will print only a fraction
    """
    return calendar.future_events if full_list else calendar.current_events


register.filter("event_list", event_list)
