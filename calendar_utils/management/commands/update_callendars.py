import logging

from django.core.management.base import BaseCommand

from ...models import Calendar

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Removing orphaned calendars...")
        for cal in Calendar.objects.filter(
            districtcalendarpage=None,
            districtcenterpage=None,
            districthomepage=None,
            districtpersonpage=None,
            elections2021calendarpage=None,
            mainpersonpage=None,
            senatcampaignhomepage=None,
            uniwebhomepage=None,
            uniwebcalendarpage=None,
        ):
            try:
                self.stdout.write(f"- {cal.id} | {cal.url}")
                cal.delete()
            except Exception as e:
                logger.error("Calendar delete failed for %s", cal.url, exc_info=True)
                self.stdout.write("  - failed")
                self.stdout.write(str(e))

        self.stdout.write("Updating calendars...")
        for cal in Calendar.objects.all():
            self.stdout.write(f"\n@ {cal.id} | {cal.url}")

            try:
                cal.update_source()
                self.stdout.write("+ ok")
            except Exception as e:
                logger.error("Calendar update failed for %s", cal.url, exc_info=True)
                self.stdout.write("- failed")
                self.stdout.write(str(e))

        self.stdout.write("\nUpdating calendars finished!")
