# Generated by Django 4.0.4 on 2022-05-05 10:27

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("calendar_utils", "0002_auto_20200523_0243"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="calendar",
            name="source",
        ),
        migrations.AddField(
            model_name="calendar",
            name="event_hash",
            field=models.CharField(max_length=256, null=True),
        ),
    ]
