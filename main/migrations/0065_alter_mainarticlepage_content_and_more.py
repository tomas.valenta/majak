# Generated by Django 4.1.10 on 2024-01-06 16:43

import django.db.models.deletion
import modelcluster.fields
import wagtail.blocks
import wagtail.documents.blocks
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations

import shared.blocks


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0064_alter_mainhomepage_content"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mainarticlepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "text",
                        wagtail.blocks.RichTextBlock(
                            template="styleguide2/includes/atoms/text/prose_richtext.html"
                        ),
                    ),
                    (
                        "quote",
                        wagtail.blocks.StructBlock(
                            [
                                ("quote", wagtail.blocks.CharBlock(label="Citace")),
                                (
                                    "autor_name",
                                    wagtail.blocks.CharBlock(label="Jméno autora"),
                                ),
                            ]
                        ),
                    ),
                    (
                        "download",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "file",
                                    wagtail.documents.blocks.DocumentChooserBlock(
                                        label="Stáhnutelný soubor"
                                    ),
                                )
                            ]
                        ),
                    ),
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Článek",
            ),
        ),
        migrations.AlterField(
            model_name="mainarticletag",
            name="content_object",
            field=modelcluster.fields.ParentalKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="main_tagged_items",
                to="main.mainarticlepage",
            ),
        ),
        migrations.AlterField(
            model_name="maincrossroadpage",
            name="headlined_cards_content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "headlined_cards",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "headline",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek bloku", required=False
                                    ),
                                ),
                                (
                                    "card_items",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "image",
                                                    wagtail.images.blocks.ImageChooserBlock(
                                                        label="Obrázek"
                                                    ),
                                                ),
                                                (
                                                    "title",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek", required=True
                                                    ),
                                                ),
                                                (
                                                    "text",
                                                    wagtail.blocks.RichTextBlock(
                                                        label="Krátký text pod nadpisem",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "page",
                                                    wagtail.blocks.PageChooserBlock(
                                                        label="Stránka",
                                                        page_type=[
                                                            "main.MainArticlesPage",
                                                            "main.MainArticlePage",
                                                            "main.MainProgramPage",
                                                            "main.MainPeoplePage",
                                                            "main.MainPersonPage",
                                                            "main.MainSimplePage",
                                                            "main.MainContactPage",
                                                            "main.MainCrossroadPage",
                                                        ],
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "link",
                                                    wagtail.blocks.URLBlock(
                                                        label="Odkaz", required=False
                                                    ),
                                                ),
                                            ],
                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                        ),
                                        label="Karty s odkazy",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Karty rozcestníku s nadpisem",
            ),
        ),
        migrations.AlterField(
            model_name="mainhomepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "carousel",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "desktop_line_1",
                                    wagtail.blocks.TextBlock(
                                        label="Desktop první řádek"
                                    ),
                                ),
                                (
                                    "desktop_line_2",
                                    wagtail.blocks.TextBlock(
                                        label="Desktop druhý řádek"
                                    ),
                                ),
                                (
                                    "mobile_line_1",
                                    wagtail.blocks.TextBlock(
                                        label="První mobilní řádek"
                                    ),
                                ),
                                (
                                    "mobile_line_2",
                                    wagtail.blocks.TextBlock(
                                        label="Druhý mobilní řádek"
                                    ),
                                ),
                                (
                                    "mobile_line_3",
                                    wagtail.blocks.TextBlock(
                                        label="Třetí mobilní řádek"
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "news",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title",
                                    wagtail.blocks.CharBlock(
                                        help_text="Nejnovější články se načtou automaticky",
                                        label="Titulek",
                                    ),
                                ),
                                (
                                    "description",
                                    wagtail.blocks.TextBlock(label="Popis"),
                                ),
                            ],
                            template="styleguide2/includes/organisms/articles/articles_section.html",
                        ),
                    ),
                    ("europarl_news", wagtail.blocks.StructBlock([])),
                    (
                        "people",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title_line_1",
                                    wagtail.blocks.CharBlock(
                                        label="První řádek titulku"
                                    ),
                                ),
                                (
                                    "title_line_2",
                                    wagtail.blocks.CharBlock(
                                        label="Druhý řádek titulku"
                                    ),
                                ),
                                (
                                    "description",
                                    wagtail.blocks.TextBlock(label="Popis"),
                                ),
                                (
                                    "list",
                                    wagtail.blocks.ListBlock(
                                        shared.blocks.PersonBoxBlock, label="Boxíky"
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "regions",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title",
                                    wagtail.blocks.CharBlock(
                                        help_text="Články pro regiony se načtou automaticky",
                                        label="Titulek",
                                    ),
                                )
                            ]
                        ),
                    ),
                    (
                        "boxes",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Nadpis")),
                                (
                                    "list",
                                    wagtail.blocks.ListBlock(
                                        shared.blocks.PersonBoxBlock, label="Boxíky"
                                    ),
                                ),
                                (
                                    "image",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Obrázek pozadí", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Hlavní obsah",
            ),
        ),
        migrations.AlterField(
            model_name="mainhomepage",
            name="footer_other_links",
            field=wagtail.fields.StreamField(
                [
                    (
                        "other_links",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titulek")),
                                (
                                    "list",
                                    wagtail.blocks.ListBlock(
                                        shared.blocks.LinkBlock,
                                        label="Seznam odkazů s titulkem",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Odkazy v zápatí webu",
            ),
        ),
        migrations.AlterField(
            model_name="mainpeoplepage",
            name="people",
            field=wagtail.fields.StreamField(
                [
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titulek")),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug skupiny",
                                        required=False,
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.PageChooserBlock(
                                            label="Detail osoby",
                                            page_type=["main.MainPersonPage"],
                                        ),
                                        label="Skupina osob",
                                    ),
                                ),
                            ],
                            label="Seznam osob",
                        ),
                    ),
                    (
                        "team_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title",
                                    wagtail.blocks.CharBlock(label="Název sekce týmů"),
                                ),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug sekce",
                                        required=False,
                                    ),
                                ),
                                (
                                    "team_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "headline",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek bloku",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "card_items",
                                                    wagtail.blocks.ListBlock(
                                                        wagtail.blocks.StructBlock(
                                                            [
                                                                (
                                                                    "image",
                                                                    wagtail.images.blocks.ImageChooserBlock(
                                                                        label="Obrázek"
                                                                    ),
                                                                ),
                                                                (
                                                                    "title",
                                                                    wagtail.blocks.CharBlock(
                                                                        label="Titulek",
                                                                        required=True,
                                                                    ),
                                                                ),
                                                                (
                                                                    "text",
                                                                    wagtail.blocks.RichTextBlock(
                                                                        label="Krátký text pod nadpisem",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "page",
                                                                    wagtail.blocks.PageChooserBlock(
                                                                        label="Stránka",
                                                                        page_type=[
                                                                            "main.MainArticlesPage",
                                                                            "main.MainArticlePage",
                                                                            "main.MainProgramPage",
                                                                            "main.MainPeoplePage",
                                                                            "main.MainPersonPage",
                                                                            "main.MainSimplePage",
                                                                            "main.MainContactPage",
                                                                            "main.MainCrossroadPage",
                                                                        ],
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "link",
                                                                    wagtail.blocks.URLBlock(
                                                                        label="Odkaz",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                            ],
                                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                                        ),
                                                        label="Karty s odkazy",
                                                    ),
                                                ),
                                            ],
                                            label="Karta týmu",
                                        ),
                                        label="Týmy",
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Lidé a týmy",
            ),
        ),
        migrations.AlterField(
            model_name="mainsimplepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "text",
                        wagtail.blocks.RichTextBlock(
                            template="styleguide2/includes/atoms/text/prose_richtext.html"
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Hlavní obsah",
            ),
        ),
    ]
