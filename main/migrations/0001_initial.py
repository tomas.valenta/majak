# Generated by Django 4.0.4 on 2022-08-10 11:39

import django.db.models.deletion
import wagtail.blocks
import wagtail.fields
import wagtailmetadata.models
from django.db import migrations, models

import shared.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("wagtailcore", "0069_log_entry_jsonfield"),
        ("wagtailimages", "0024_index_image_file_hash"),
    ]

    operations = [
        migrations.CreateModel(
            name="MainHomePage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "menu",
                    wagtail.fields.StreamField(
                        [
                            (
                                "menu_item",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(
                                                label="Titulek", required=True
                                            ),
                                        ),
                                        (
                                            "page",
                                            wagtail.blocks.PageChooserBlock(
                                                label="Stránka", required=False
                                            ),
                                        ),
                                        (
                                            "link",
                                            wagtail.blocks.URLBlock(
                                                label="Odkaz", required=False
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                            (
                                "menu_parent",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(
                                                label="Titulek", required=True
                                            ),
                                        ),
                                        (
                                            "menu_items",
                                            wagtail.blocks.ListBlock(
                                                wagtail.blocks.StructBlock(
                                                    [
                                                        (
                                                            "title",
                                                            wagtail.blocks.CharBlock(
                                                                label="Titulek",
                                                                required=True,
                                                            ),
                                                        ),
                                                        (
                                                            "page",
                                                            wagtail.blocks.PageChooserBlock(
                                                                label="Stránka",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "link",
                                                            wagtail.blocks.URLBlock(
                                                                label="Odkaz",
                                                                required=False,
                                                            ),
                                                        ),
                                                    ]
                                                )
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                        ],
                        blank=True,
                        use_json_field=None,
                        verbose_name="Menu",
                    ),
                ),
                (
                    "title_suffix",
                    models.CharField(
                        blank=True,
                        help_text="Umožňuje přidat příponu k základnímu titulku stránky. Pokud je např. titulek stránky pojmenovaný 'Kontakt' a do přípony vyplníte 'MS Pardubice | Piráti', výsledný titulek bude 'Kontakt | MS Pardubice | Piráti'. Pokud příponu nevyplníte, použije se název webu.",
                        max_length=100,
                        null=True,
                        verbose_name="Přípona titulku stránky",
                    ),
                ),
                (
                    "matomo_id",
                    models.IntegerField(
                        blank=True,
                        null=True,
                        verbose_name="Matomo ID pro sledování návštěvnosti",
                    ),
                ),
                (
                    "footer",
                    wagtail.fields.StreamField(
                        [
                            (
                                "item",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "position",
                                            wagtail.blocks.CharBlock(
                                                label="Název pozice", required=False
                                            ),
                                        ),
                                        (
                                            "person",
                                            wagtail.blocks.PageChooserBlock(
                                                label="Osoba",
                                                page_type=[
                                                    "district.DistrictPersonPage"
                                                ],
                                            ),
                                        ),
                                    ]
                                ),
                            )
                        ],
                        blank=True,
                        use_json_field=None,
                        verbose_name="Kontaktní boxy",
                    ),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "HomePage pirati.cz",
            },
            bases=(
                "wagtailcore.page",
                wagtailmetadata.models.WagtailImageMetadataMixin,
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="MainContactPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "contact_people",
                    wagtail.fields.StreamField(
                        [
                            (
                                "item",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "position",
                                            wagtail.blocks.CharBlock(
                                                label="Název pozice", required=False
                                            ),
                                        ),
                                        (
                                            "person",
                                            wagtail.blocks.PageChooserBlock(
                                                label="Osoba",
                                                page_type=[
                                                    "district.DistrictPersonPage"
                                                ],
                                            ),
                                        ),
                                    ]
                                ),
                            )
                        ],
                        blank=True,
                        use_json_field=None,
                        verbose_name="Kontaktní osoby",
                    ),
                ),
                (
                    "contact_boxes",
                    wagtail.fields.StreamField(
                        [
                            (
                                "item",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "position",
                                            wagtail.blocks.CharBlock(
                                                label="Název pozice", required=False
                                            ),
                                        ),
                                        (
                                            "person",
                                            wagtail.blocks.PageChooserBlock(
                                                label="Osoba",
                                                page_type=[
                                                    "district.DistrictPersonPage"
                                                ],
                                            ),
                                        ),
                                    ]
                                ),
                            )
                        ],
                        blank=True,
                        use_json_field=None,
                        verbose_name="Kontaktní boxy",
                    ),
                ),
                ("text", wagtail.fields.RichTextField(blank=True, verbose_name="Text")),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Kontakty",
            },
            bases=(
                shared.models.SubpageMixin,
                wagtailmetadata.models.WagtailImageMetadataMixin,
                "wagtailcore.page",
                models.Model,
            ),
        ),
    ]
