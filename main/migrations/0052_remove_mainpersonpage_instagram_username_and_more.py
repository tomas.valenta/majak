# Generated by Django 4.1.6 on 2023-04-06 12:36

import wagtail.blocks
import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0051_remove_mainhomepage_twitter_usernames_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="mainpersonpage",
            name="instagram_username",
        ),
        migrations.RemoveField(
            model_name="mainpersonpage",
            name="twitter_username",
        ),
        migrations.AddField(
            model_name="mainpersonpage",
            name="instagram_access",
            field=wagtail.fields.StreamField(
                [
                    (
                        "instagram_access",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "name",
                                    wagtail.blocks.CharBlock(label="Zobrazované jméno"),
                                ),
                                (
                                    "username",
                                    wagtail.blocks.CharBlock(
                                        help_text="Např. pirati.cz, bez @ na začátku!",
                                        label="Username",
                                    ),
                                ),
                                (
                                    "access_token",
                                    wagtail.blocks.CharBlock(label="Přístupový token"),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Synchronizace s Instagramem",
            ),
        ),
    ]
