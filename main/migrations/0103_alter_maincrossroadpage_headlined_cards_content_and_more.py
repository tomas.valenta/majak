# Generated by Django 5.0.6 on 2024-07-03 10:12

import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0102_rename_people_mainpeoplepage_content_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="maincrossroadpage",
            name="headlined_cards_content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "headlined_cards",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "headline",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek bloku", required=False
                                    ),
                                ),
                                (
                                    "card_items",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "image",
                                                    wagtail.images.blocks.ImageChooserBlock(
                                                        label="Obrázek"
                                                    ),
                                                ),
                                                (
                                                    "title",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek", required=True
                                                    ),
                                                ),
                                                (
                                                    "text",
                                                    wagtail.blocks.RichTextBlock(
                                                        label="Krátký text pod nadpisem",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "page",
                                                    wagtail.blocks.PageChooserBlock(
                                                        label="Stránka",
                                                        page_type=[
                                                            "main.MainArticlesPage",
                                                            "main.MainArticlePage",
                                                            "main.MainProgramPage",
                                                            "main.MainPeoplePage",
                                                            "main.MainPersonPage",
                                                            "main.MainSimplePage",
                                                            "main.MainContactPage",
                                                            "main.MainCrossroadPage",
                                                        ],
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "link",
                                                    wagtail.blocks.URLBlock(
                                                        label="Odkaz", required=False
                                                    ),
                                                ),
                                            ],
                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                        ),
                                        label="Karty",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                verbose_name="Karty rozcestníku s nadpisem",
            ),
        ),
        migrations.AlterField(
            model_name="mainpeoplepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titulek")),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug skupiny",
                                        required=False,
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.PageChooserBlock(
                                            label="Detail osoby",
                                            page_type=["main.MainPersonPage"],
                                        ),
                                        label="Skupina osob",
                                    ),
                                ),
                            ],
                            label="Seznam osob",
                        ),
                    ),
                    (
                        "team_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title",
                                    wagtail.blocks.CharBlock(label="Název sekce týmů"),
                                ),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug sekce",
                                        required=False,
                                    ),
                                ),
                                (
                                    "team_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "headline",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek bloku",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "card_items",
                                                    wagtail.blocks.ListBlock(
                                                        wagtail.blocks.StructBlock(
                                                            [
                                                                (
                                                                    "image",
                                                                    wagtail.images.blocks.ImageChooserBlock(
                                                                        label="Obrázek"
                                                                    ),
                                                                ),
                                                                (
                                                                    "title",
                                                                    wagtail.blocks.CharBlock(
                                                                        label="Titulek",
                                                                        required=True,
                                                                    ),
                                                                ),
                                                                (
                                                                    "text",
                                                                    wagtail.blocks.RichTextBlock(
                                                                        label="Krátký text pod nadpisem",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "page",
                                                                    wagtail.blocks.PageChooserBlock(
                                                                        label="Stránka",
                                                                        page_type=[
                                                                            "main.MainArticlesPage",
                                                                            "main.MainArticlePage",
                                                                            "main.MainProgramPage",
                                                                            "main.MainPeoplePage",
                                                                            "main.MainPersonPage",
                                                                            "main.MainSimplePage",
                                                                            "main.MainContactPage",
                                                                            "main.MainCrossroadPage",
                                                                        ],
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "link",
                                                                    wagtail.blocks.URLBlock(
                                                                        label="Odkaz",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                            ],
                                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                                        ),
                                                        label="Karty",
                                                    ),
                                                ),
                                            ],
                                            label="Karta týmu",
                                        ),
                                        label="Týmy",
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Lidé a týmy",
            ),
        ),
    ]
