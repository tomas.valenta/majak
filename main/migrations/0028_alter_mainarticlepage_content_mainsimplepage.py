# Generated by Django 4.0.7 on 2022-08-31 14:14

import django.db.models.deletion
import wagtail.blocks
import wagtail.documents.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtailmetadata.models
from django.db import migrations, models

import shared.models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailimages", "0024_index_image_file_hash"),
        ("wagtailcore", "0069_log_entry_jsonfield"),
        ("main", "0027_alter_mainpersonpage_people"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mainarticlepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "text",
                        wagtail.blocks.RichTextBlock(
                            template="main/blocks/rich_text_block.html"
                        ),
                    ),
                    (
                        "quote",
                        wagtail.blocks.StructBlock(
                            [
                                ("quote", wagtail.blocks.CharBlock(label="Citace")),
                                (
                                    "autor_name",
                                    wagtail.blocks.CharBlock(label="Jméno autora"),
                                ),
                            ]
                        ),
                    ),
                    (
                        "download",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "file",
                                    wagtail.documents.blocks.DocumentChooserBlock(
                                        label="Stáhnutelný soubor"
                                    ),
                                )
                            ]
                        ),
                    ),
                    (
                        "image",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "image",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="Obrázek"
                                    ),
                                ),
                                (
                                    "href",
                                    wagtail.blocks.URLBlock(label="Odkaz k textu"),
                                ),
                                ("text", wagtail.blocks.CharBlock(label="Text")),
                            ]
                        ),
                    ),
                ],
                blank=True,
                use_json_field=None,
                verbose_name="Článek",
            ),
        ),
        migrations.CreateModel(
            name="MainSimplePage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "content",
                    wagtail.fields.StreamField(
                        [
                            (
                                "text",
                                wagtail.blocks.RichTextBlock(
                                    template="main/blocks/rich_text_block.html"
                                ),
                            )
                        ],
                        blank=True,
                        use_json_field=None,
                        verbose_name="Hlavní obsah",
                    ),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Jednoduchá stárnka",
            },
            bases=(
                shared.models.SubpageMixin,
                wagtailmetadata.models.WagtailImageMetadataMixin,
                "wagtailcore.page",
                models.Model,
            ),
        ),
    ]
