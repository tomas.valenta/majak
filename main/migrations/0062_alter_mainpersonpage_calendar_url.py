# Generated by Django 4.1.10 on 2023-08-23 10:39

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0061_remove_mainhomepage_shared_tags"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mainpersonpage",
            name="calendar_url",
            field=models.URLField(
                blank=True,
                help_text="Kalendář se po uložení stránky aktualizuje na pozadí. U plnějších kalendářů to může trvat i desítky sekund.",
                null=True,
                verbose_name="URL kalendáře ve formátu iCal",
            ),
        ),
    ]
