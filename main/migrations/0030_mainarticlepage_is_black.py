# Generated by Django 4.0.7 on 2022-09-06 11:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0029_mainhomepage_gdpr_and_cookies_page"),
    ]

    operations = [
        migrations.AddField(
            model_name="mainarticlepage",
            name="is_black",
            field=models.BooleanField(default=False, verbose_name="Má tmavé pozadí?"),
        ),
    ]
