# Generated by Django 5.0.7 on 2024-08-08 12:30

import modelcluster.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0113_alter_mainarticlespage_show_tags"),
        ("shared", "0010_alter_octopusperson_photo"),
    ]

    operations = [
        migrations.AddField(
            model_name="mainarticlespage",
            name="displayed_shared_tags",
            field=modelcluster.fields.ParentalManyToManyField(
                blank=True,
                related_name="+",
                to="shared.sharedtag",
                verbose_name="Pro sdílení mezi weby",
            ),
        ),
        migrations.AlterField(
            model_name="mainarticlespage",
            name="displayed_tags",
            field=modelcluster.fields.ParentalManyToManyField(
                blank=True,
                related_name="+",
                to="main.mainarticletag",
                verbose_name="Z tohoto webu",
            ),
        ),
    ]
