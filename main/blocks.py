from django.utils.text import slugify
from wagtail.blocks import (
    CharBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from shared.blocks import CandidateBlock as SharedCandidateBlockMixin
from shared.blocks import CandidateListBlock
from shared.blocks import CandidateListBlock as SharedCandidateListBlockMixin
from shared.blocks import (
    CandidateSecondaryListBlock as SharedCandidateSecondaryListBlockMixin,
)
from shared.blocks import (
    CardLinkBlockMixin,
    CardLinkWithHeadlineBlockMixin,
    CarouselProgramBlock,
    CustomPrimaryCandidateBlock,
    LinkBlock,
    PersonBoxBlock,
    PersonContactBlockMixin,
    ProgramGroupBlock,
    ProgramGroupBlockMixin,
)
from shared.blocks import ProgramGroupBlockPopout as SharedProgramGroupBlockPopout
from shared.blocks import (
    ProgramGroupWithCandidatesBlock as SharedProgramGroupWithCandidatesBlockMixin,
)
from shared.blocks import SecondaryCandidateBlock as SharedSecondaryCandidateBlockMixin
from shared.blocks import StreamBlock, TeamBlockMixin


class CardLinkBlock(CardLinkBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "main.MainArticlesPage",
            "main.MainArticlePage",
            "main.MainProgramPage",
            "main.MainPeoplePage",
            "main.MainPersonPage",
            "main.MainSimplePage",
            "main.MainContactPage",
            "main.MainCrossroadPage",
        ],
        required=False,
    )

    class Meta:
        template = "styleguide2/includes/molecules/boxes/card_box_block.html"
        icon = "link"
        label = "Karta s odkazem"


class BoxesBlock(StructBlock):
    title = CharBlock(label="Nadpis")
    list = ListBlock(PersonBoxBlock(), label="Boxíky")
    image = ImageChooserBlock(label="Obrázek pozadí", required=False)

    class Meta:
        template = "main/blocks/boxes_block.html"
        icon = "grip"
        label = "Skupina boxů"


class HomePageCarouseSlideBlock(StructBlock):
    desktop_line_1 = TextBlock(label="Desktop první řádek")
    desktop_line_2 = TextBlock(label="Desktop druhý řádek")

    mobile_line_1 = TextBlock(label="První mobilní řádek")
    mobile_line_2 = TextBlock(label="Druhý mobilní řádek")
    mobile_line_3 = TextBlock(label="Třetí mobilní řádek")

    desktop_image = ImageChooserBlock(
        label="Obrázek nahrazující animaci (desktop)", required=False
    )
    mobile_image = ImageChooserBlock(
        label="Obrázek nahrazující animaci (mobil / tablet)", required=False
    )

    button_url = URLBlock(
        label="Odkaz tlačítka",
        help_text="Bez odkazu tlačítko nebude viditelné.",
        required=False,
    )
    button_text = CharBlock(
        label="Text tlačítka",
        help_text="Odkaz funguje i bez tlačítka. Pokud chceš tlačítko skrýt, nevyplňuj text.",
        required=False,
    )

    class Meta:
        template = "styleguide2/includes/molecules/menus/carousel.html"
        icon = "form"
        label = "Carousel"


class EuroparlNewsBlock(StructBlock):
    class Meta:
        template = (
            "styleguide2/includes/organisms/articles/europarl_articles_section.html"
        )
        icon = "doc-full-inverse"
        label = "Novinky z Eurovoleb"


class PeopleGroupBlock(StructBlock):
    title = CharBlock(label="Titulek")
    slug = CharBlock(
        label="Slug skupiny",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
    )
    person_list = ListBlock(
        PageChooserBlock(page_type="main.MainPersonPage", label="Detail osoby"),
        label="Skupina osob",
    )

    class Meta:
        icon = "group"
        label = "Slug skupiny"

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value


class ProgramGroupBlockCrossroad(ProgramGroupBlockMixin):
    point_list = ListBlock(CardLinkBlock(), label="Karty programu")

    class Meta:
        icon = "date"
        label = "Rozcestníkový program"


class RegionsBlock(StructBlock):
    title = CharBlock(
        label="Titulek",
        help_text="Články pro regiony se načtou automaticky",
    )

    class Meta:
        template = "styleguide2/includes/organisms/main_section/region_section.html"
        icon = "view"
        label = "Články pro regiony"


# ARTICLE BLOCKS
class ArticleImageMixin(StructBlock):
    image = ImageChooserBlock(label="Obrázek")
    image_source = CharBlock(
        label="Zdroj obrázku",
        help_text="Např. 'europoslankyně Markéta Gregorová'",
        required=False,
    )

    text = RichTextBlock(label="Text")


class ArticleLeftImageBlock(ArticleImageMixin):
    class Meta:
        icon = "image"
        label = "Text s obrázkem vlevo"
        template = "styleguide2/includes/molecules/articles/article_richtext_content_with_left_image.html"


class ArticleRightImageBlock(ArticleImageMixin):
    class Meta:
        icon = "image"
        label = "Text s obrázkem vpravo"
        template = "styleguide2/includes/molecules/articles/article_richtext_content_with_right_image.html"


class CardLinkWithHeadlineBlock(CardLinkWithHeadlineBlockMixin):
    card_items = ListBlock(
        CardLinkBlock(
            template="styleguide2/includes/molecules/boxes/card_box_block.html"
        ),
        label="Karty",
    )

    class Meta:
        template = (
            "styleguide2/includes/molecules/boxes/card_box_with_headline_block.html"
        )
        icon = "link"
        label = "Karty"


class HoaxBlock(StructBlock):
    title = CharBlock(label="Titulek")
    hoax = RichTextBlock(label="Hoax")
    image = ImageChooserBlock(label="Obrázek")
    image_explanation = CharBlock(label="Popis obrázku", required=False)
    reality = RichTextBlock(label="Realita")

    class Meta:
        icon = "view"
        label = "Hoax"
        template = "styleguide2/includes/molecules/popouts/hoax_popout_point.html"


class TeamBlock(TeamBlockMixin):
    team_list = ListBlock(
        CardLinkWithHeadlineBlock(label="Karta týmu"),
        label="Týmy",
    )


class ElectionsProgramBlock(StructBlock):
    title = CharBlock(
        label="Název programu",
        help_text="Např. 'Krajské volby 2024', 'Evropské volby 2024', ...",
    )

    program_page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "elections.ElectionsFullProgramPage",
        ],
        required=False,
    )

    class Meta:
        icon = "view"
        label = "Program z volebního webu"
        template = "styleguide2/includes/molecules/program/program_block.html"


class ProgramGroupBlockPopout(SharedProgramGroupBlockPopout, ProgramGroupBlockMixin):
    pass


class PersonContactBlock(PersonContactBlockMixin):
    person = PageChooserBlock(
        label="Osoba",
        page_type=["main.MainPersonPage"],
    )


class CandidateBlock(SharedCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=["district.DistrictPersonPage", "district.DistrictOctopusPersonPage"],
    )


class SecondaryCandidateBlock(SharedSecondaryCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=["district.DistrictPersonPage", "district.DistrictOctopusPersonPage"],
    )


class CandidateListBlock(SharedCandidateListBlockMixin):
    stream_candidates = StreamBlock(
        [
            ("candidate", CandidateBlock()),
            ("custom_candidate", CustomPrimaryCandidateBlock()),
        ],
        required=False,
        label=" ",  # Hacky way to show no label
    )


class CandidateSecondaryListBlock(SharedCandidateSecondaryListBlockMixin):
    candidates = ListBlock(
        SecondaryCandidateBlock(),
        min_num=0,
        default=[],
        label=" ",  # Hacky way to show no label
    )


class ProgramGroupWithCandidatesBlock(SharedProgramGroupWithCandidatesBlockMixin):
    primary_candidates = CandidateListBlock(
        label="Osoby na čele kandidátky",
        help_text="Zobrazí se ve velkých blocích na začátku stránky.",
    )

    secondary_candidates = CandidateSecondaryListBlock(
        label="Ostatní osoby na kandidátce",
        help_text="Zobrazí se v kompaktním seznamu pod čelem kandidátky. Níže můžeš změnit nadpis.",
    )

    program = StreamBlock(
        [
            ("program_group", ProgramGroupBlock()),
            ("program_group_crossroad", ProgramGroupBlockCrossroad()),
            ("program_group_popout", ProgramGroupBlockPopout()),
            (
                "carousel_program",
                CarouselProgramBlock(
                    template="styleguide2/includes/molecules/program/program_block.html"
                ),
            ),
        ],
        required=False,
    )


# --- TODO: Remove legacy blocks used in migrations only


class LinkBlock(StructBlock):
    pass
