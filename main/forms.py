import os
import tempfile

from shared.forms import ArticlesPageForm as SharedArticlesPageForm
from shared.forms import JekyllImportForm as SharedJekyllImportForm

from .tasks import import_jekyll_articles


class JekyllImportForm(SharedJekyllImportForm):
    def handle_import(self):
        lock_file_name = os.path.join(
            tempfile.gettempdir(), f".{self.instance.id}.articles-import-lock"
        )

        if os.path.isfile(lock_file_name):
            return

        open(lock_file_name, "w").close()

        import_jekyll_articles.delay(
            article_parent_page_id=self.instance.id,
            collection_id=self.cleaned_data["collection"].id,
            url=self.cleaned_data["jekyll_repo_url"],
            dry_run=self.cleaned_data["dry_run"],
            use_git=True,
        )


class MainArticlesPageForm(SharedArticlesPageForm, JekyllImportForm):
    def __init__(self, *args, **kwargs):
        from shared.models import SharedTag

        from .models import MainArticleTag

        super().__init__(*args, **kwargs)

        self.fields["shared_tags"].queryset = SharedTag.objects.order_by("name")

        if self.instance.pk:
            valid_tag_ids = (
                MainArticleTag.objects.filter(
                    content_object__in=self.instance.get_children().specific()
                )
                .values_list("tag_id", flat=True)
                .distinct()
            )
            valid_shared_tag_ids = (
                self.instance.shared_tags.values_list("id", flat=True).distinct().all()
            )

            self.fields["displayed_tags"].queryset = (
                MainArticleTag.objects.filter(id__in=valid_tag_ids)
                .order_by("tag__name")
                .distinct("tag__name")
            )
            self.fields["displayed_shared_tags"].queryset = (
                SharedTag.objects.filter(id__in=valid_shared_tag_ids)
                .order_by("name")
                .distinct("name")
            )
        else:
            self.fields["displayed_tags"].queryset = MainArticleTag.objects.filter(
                id=-1
            )
            self.fields["displayed_shared_tags"].queryset = SharedTag.objects.filter(
                id=-1
            )
