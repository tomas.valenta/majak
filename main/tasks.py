import logging

from celery import shared_task

from shared.jekyll_import import JekyllArticleImporter

logger = logging.getLogger(__name__)


@shared_task()
def import_jekyll_articles(
    article_parent_page_id,
    collection_id,
    url,
    dry_run,
    use_git,
):
    from main.models import MainArticlePage, MainArticlesPage

    return JekyllArticleImporter(
        article_parent_page_id=article_parent_page_id,
        article_parent_page_model=MainArticlesPage,
        collection_id=collection_id,
        url=url,
        dry_run=dry_run,
        use_git=use_git,
        page_model=MainArticlePage,
    ).perform_import()
