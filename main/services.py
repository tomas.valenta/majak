import json
import logging
from typing import TYPE_CHECKING

import requests

from main.models import MainArticlePage, MainArticlesPage
from shared.utils import create_image_from_url

if TYPE_CHECKING:
    pass

logger = logging.getLogger()


class TimelineArticleDownloadService:
    api_url = "https://piratipracuji.cz/api/"

    @staticmethod
    def get_existing_articles_slugs() -> list[str]:
        return MainArticlePage.objects.values_list("slug", flat=True)

    def get_articles_response(self):
        response = requests.get(self.api_url)
        data = json.loads(response.text)

        return data

    def perform_update(self):
        existing_articles_slug_list = self.get_existing_articles_slugs()
        parent = MainArticlesPage.objects.all().first()

        if not parent:
            RuntimeError("No MainArticlesPage to import to")

        for article in self.get_articles_response():
            if article["slug"] not in existing_articles_slug_list:
                if "thumbnail" in article:
                    img_filename = "article-" + str(article["id"]) + ".jpg"
                    img_obj = create_image_from_url(
                        url=article["thumbnail"], filename=img_filename
                    )
                else:
                    img_obj = None

                article_to_save = MainArticlePage(
                    title=article["title"],
                    slug=article["slug"],
                    perex=article["description"].replace("&nbsp;", " "),
                    author="ČESKÁ PIRÁTSKÁ STRANA",
                    date=article["start_date"],
                    image=img_obj,
                    is_black=article["isFeatured"],
                    content=json.dumps(
                        [
                            {
                                "type": "text",
                                "value": article["content"].replace("</p>", "</p><br>"),
                            }
                        ]
                    ),
                )

                parent.add_child(instance=article_to_save)
