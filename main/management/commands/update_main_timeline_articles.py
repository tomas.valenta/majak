from django.core.management.base import BaseCommand

from main.services import TimelineArticleDownloadService


class Command(BaseCommand):
    def handle(self, *args, **options):
        ads = TimelineArticleDownloadService()
        ads.perform_update()

        self.stdout.write("\nUpdate of articles finished!")
