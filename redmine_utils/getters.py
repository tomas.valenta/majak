import json
import logging
from typing import Dict, List

import requests

logger = logging.getLogger(__name__)


def get_issue_overall_percentage(issue_id: int) -> int or None:
    """
    Getne stav issue na redmine a vrátí její procento dokončení (nebo None)
    """
    response = requests.get("https://redmine.pirati.cz/issues/{}.json".format(issue_id))

    if response.status_code != 200 or not response.text:
        logger.error(
            "Nepodařilo se stáhnout info o Redmine issue",
            extra={"redmine_issue": issue_id},
        )
        return None

    return json.loads(response.text).get("issue", {}).get("done_ratio", None)


def get_issue_list(issue_id: int) -> List[Dict]:
    """
    Getne list dceřiných issues a vrátí je jako list dictionaries.
    """
    response = requests.get(
        "https://redmine.pirati.cz/issues.json?parent_id={}&sort=id:as".format(issue_id)
    )

    if response.status_code != 200 or not response.text:
        logger.error(
            "Nepodařilo se stáhnout dílčí issues pro Redmine issue",
            extra={"redmine_issue": issue_id},
        )
        return []

    return json.loads(response.text).get("issues", [])
