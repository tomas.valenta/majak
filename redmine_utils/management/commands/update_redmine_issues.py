from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        Pro updated_models (DistrictProgramPage) projedu obsah, zda má v sobě
        `redmine_program_block` - ten má IntegerBlock `redmine_issue`. Podle něj
        pak stahujeme data z Redmine.
        """
        self.stdout.write("Updating Redmine issues...")

        # TODO

        # for page in DistrictProgramPage.objects.all():
        #     fill_data_from_redmine_for_page(page)
        #    page.save()

        self.stdout.write("\nUpdating Redmine issues finished")
