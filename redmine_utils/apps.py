from django.apps import AppConfig


class RedmineUtilsConfig(AppConfig):
    name = "redmine_utils"
