from wagtail.blocks import StructValue

from shared.blocks import ProgramItemBlock

from .getters import get_issue_list, get_issue_overall_percentage


def fill_data_from_redmine_for_page(page):
    """
    Naplní hondnotu completion_percentage a program_items pro RedmineProgramBlock
    pro danou Page (musí mít atribut content s blockem redmine_program_block)
    """
    for program_block in page.content:
        if program_block.block_type == "redmine_program_block":
            redmine_issue_id = program_block.value["redmine_issue"]
            fill_overall_percentage_from_redmine(program_block.value, redmine_issue_id)
            fill_program_items_from_redmine(program_block.value, redmine_issue_id)


def fill_overall_percentage_from_redmine(program_block_value: dict, issue_id: int):
    """
    Naplní hondnotu completion_percentage pro RedmineProgramBlock hodnoutou z Redmine.
    """
    program_block_value["completion_percentage"] = get_issue_overall_percentage(
        issue_id
    )


def fill_program_items_from_redmine(program_block_value: dict, issue_id: int):
    """
    Naplní hondnotu program_items pro RedmineProgramBlock. Nejdříve hodnotu (list)
    promaže a pak z getných issues naplní pomocí StructValue
    """
    issue_list = get_issue_list(issue_id)
    program_block_value["program_items"].clear()

    for issue in issue_list:
        sv = StructValue(ProgramItemBlock)
        sv.update(
            {
                "completion_percentage": issue.get("done_ratio", None),
                "title": issue.get("subject", ""),
                "issue_link": "https://redmine.pirati.cz/issues/{}".format(
                    issue.get("id", "")
                ),
            }
        )
        program_block_value["program_items"].append(sv)
