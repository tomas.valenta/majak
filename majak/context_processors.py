from django.conf import settings


def styleguide_url_processor(request):
    return {"styleguide_url": settings.STYLEGUIDE_URL}
