import captcha.urls
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from pirates.urls import urlpatterns as pirates_urlpatterns
from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.documents import urls as wagtaildocs_urls

from elections2021 import views as elections2021_views
from maps_utils import urls as maps_utils_urls
from shared.utils import subscribe_to_newsletter_ajax
from tuning.views import SitesListView

handler404 = "shared.views.page_not_found"

urlpatterns = [
    path("django-admin/", admin.site.urls),
    path("admin/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path(
        "export/elections2021/banner-orders.csv",
        elections2021_views.banner_orders_csv,
        name="elections2021_banner_orders_csv",
    ),
    path("maps/", include(maps_utils_urls)),
    path("captcha/", include(captcha.urls)),
    path("seznam-webu/", SitesListView.as_view()),
    path("newsletter/", subscribe_to_newsletter_ajax),
    path("sitemap.xml", sitemap),
] + pirates_urlpatterns


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns


urlpatterns = urlpatterns + [
    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    path("", include(wagtail_urls)),
    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    url(r"^pages/", include(wagtail_urls)),
]
