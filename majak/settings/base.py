from os.path import join
from pathlib import Path

import environ
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

ROOT_DIR = Path(__file__).parents[2]
PROJECT_DIR = ROOT_DIR / "majak"

env = environ.Env()
environ.Env.read_env(str(ROOT_DIR / ".env"))

# GENERAL
# ------------------------------------------------------------------------------
DEBUG = env.bool("DJANGO_DEBUG", False)
ROOT_URLCONF = "majak.urls"
WSGI_APPLICATION = "majak.wsgi.application"

# I18N and L10N
# ------------------------------------------------------------------------------
TIME_ZONE = "Europe/Prague"
LANGUAGE_CODE = "cs"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# DATABASES
# ------------------------------------------------------------------------------
DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# APPS
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    "article_import_utils",
    "districts",
    "senate",
    "donate",
    "main",
    "elections",
    "senat_campaign",
    "uniweb",
    "district",
    "czech_inspirational",
    "shared",
    "shared_legacy",
    "calendar_utils",
    "maps_utils",
    "redmine_utils",
    "users",
    "pirates",
    "tuning",
    "regulace_konopi",
    "green_deal",
    "elections2021",
    "widget_tweaks",
    "captcha",
    "wagtail.contrib.forms",
    "wagtail.contrib.redirects",
    "wagtail_modeladmin",
    "wagtail.contrib.table_block",
    "wagtail.contrib.routable_page",
    "wagtail.embeds",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail.admin",
    "wagtail",
    "wagtailmetadata",
    "wagtail_trash",
    "modelcluster",
    "taggit",
    "django_extensions",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "django.contrib.postgres",
    "django.contrib.sitemaps",
]

# AUTHENTICATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = ["pirates.auth.PiratesOIDCAuthenticationBackend"]
AUTH_USER_MODEL = "users.User"
LOGIN_REDIRECT_URL = "/admin"
LOGOUT_REDIRECT_URL = "/admin"
LOGIN_URL = "/admin"

OIDC_RP_CLIENT_ID = env.str("OIDC_RP_CLIENT_ID")
OIDC_RP_CLIENT_SECRET = env.str("OIDC_RP_CLIENT_SECRET")
OIDC_RP_REALM_URL = env.str("OIDC_RP_REALM_URL")
OIDC_RP_SIGN_ALGO = "RS256"
OIDC_OP_JWKS_ENDPOINT = join(OIDC_RP_REALM_URL, "protocol/openid-connect/certs")
OIDC_OP_AUTHORIZATION_ENDPOINT = join(OIDC_RP_REALM_URL, "protocol/openid-connect/auth")
OIDC_OP_TOKEN_ENDPOINT = join(OIDC_RP_REALM_URL, "protocol/openid-connect/token")
OIDC_OP_USER_ENDPOINT = join(OIDC_RP_REALM_URL, "protocol/openid-connect/userinfo")

# MIDDLEWARE
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

# STATIC
# ------------------------------------------------------------------------------
STATIC_ROOT = str(ROOT_DIR / "static_files")
STATIC_URL = "/static/"
STATICFILES_DIRS = [str(PROJECT_DIR / "static")]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

# MEDIA
# ------------------------------------------------------------------------------
MEDIA_URL = "/media/"
MEDIA_ROOT = str(ROOT_DIR / "media_files")

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [str(PROJECT_DIR / "templates")],
        "OPTIONS": {
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django_settings_export.settings_export",
                "majak.context_processors.styleguide_url_processor",
            ],
        },
    },
]

# SECURITY
# ------------------------------------------------------------------------------
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_HTTPONLY = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = "DENY"

# needed for editing large map collections
DATA_UPLOAD_MAX_NUMBER_FIELDS = None

# EMAIL
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND", default="django.core.mail.backends.dummy.EmailBackend"
)

# LOGGING
# ------------------------------------------------------------------------------
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}

# CACHES
# ------------------------------------------------------------------------------
CACHES = {
    "default": env.cache("CACHE_URL", default="locmemcache://"),
    "renditions": env.cache("CACHE_URL", default="locmemcache://"),
}
CACHES["default"]["TIMEOUT"] = 60 * 60 * 24
CACHES["renditions"]["TIMEOUT"] = 60 * 60 * 24

# CELERY
# ------------------------------------------------------------------------------
CELERY_BROKER_URL = env.str("CELERY_BROKER_URL", default="")
if not CELERY_BROKER_URL:
    CELERY_TASK_ALWAYS_EAGER = True
CELERY_RESULT_BACKEND = env.str("CELERY_RESULT_BACKEND", default="")
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"

# SENTRY
# ------------------------------------------------------------------------------

SENTRY_DSN = env.str("SENTRY_DSN", default="")
if SENTRY_DSN:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[DjangoIntegration()],
        send_default_pii=True,
    )

# WAGTAIL SETTINGS
# ------------------------------------------------------------------------------
WAGTAIL_SITE_NAME = "Maják"
WAGTAIL_USER_TIME_ZONES = ["Europe/Prague"]
WAGTAIL_ALLOW_UNICODE_SLUGS = False
WAGTAILADMIN_NOTIFICATION_INCLUDE_SUPERUSERS = False
WAGTAILIMAGES_FEATURE_DETECTION_ENABLED = env.bool(
    "WAGTAILIMAGES_FEATURE_DETECTION_ENABLED", True
)
WAGTAILIMAGES_INDEX_PAGE_SIZE = 30
WAGTAILIMAGES_USAGE_PAGE_SIZE = 30
WAGTAILIMAGES_CHOOSER_PAGE_SIZE = 15
WAGTAIL_USAGE_COUNT_ENABLED = True
TAGGIT_CASE_INSENSITIVE = True

# disable editing of user details synced from SSO
WAGTAIL_USER_EDIT_FORM = "users.forms.CustomUserEditForm"
WAGTAIL_USER_CREATION_FORM = "users.forms.CustomUserCreationForm"
WAGTAIL_PASSWORD_MANAGEMENT_ENABLED = False
WAGTAIL_PASSWORD_RESET_ENABLED = False
WAGTAILUSERS_PASSWORD_ENABLED = False
WAGTAILUSERS_PASSWORD_REQUIRED = False
WAGTAIL_EMAIL_MANAGEMENT_ENABLED = False

WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.database",
        "SEARCH_CONFIG": env.str("SEARCH_CONFIG", default="english"),
    }
}

WAGTAILEMBEDS_FINDERS = [
    {
        "class": "shared.finders.CustomPeertubeFinder",
    },
]

WAGTAILEMBEDS_RESPONSIVE_HTML = True

# Base URL to use when referring to full URLs within the Wagtail admin backend -
# e.g. in notification emails. Don't include '/admin' or a trailing slash
BASE_URL = env.str("BASE_URL", default="https://majak.pirati.cz")
WAGTAILADMIN_BASE_URL = BASE_URL

# CUSTOM SETTINGS
# ------------------------------------------------------------------------------
STYLEGUIDE_URL = env.str("STYLEGUIDE_URL", "https://styleguide.pirati.cz/2.20.x/")
OCTOPUS_API_URL = env.str("OCTOPUS_API_URL", "https://chobotnice.pirati.cz/graphql/")

MAJAK_ENV = env.str("MAJAK_ENV", default="prod")

SETTINGS_EXPORT = ["MAJAK_ENV"]

CAPTCHA_BACKGROUND_COLOR = "#fafafa"
CAPTCHA_FOREGROUND_COLOR = "#303132"
CAPTCHA_LETTER_ROTATION = (-20, 20)
CAPTCHA_FONT_SIZE = 30
CAPTCHA_IMAGE_SIZE = (120, 47)

# meta image size
WAGTAILMETADATA_IMAGE_FILTER = "max-1600x1600"

# CUSTOM APPS SETTINGS
# ------------------------------------------------------------------------------
DONATE_PORTAL_REDIRECT_URL = env.str("DONATE_PORTAL_REDIRECT_URL", default="")
DONATE_PORTAL_REDIRECT_SOURCE = env.str(
    "DONATE_PORTAL_REDIRECT_SOURCE", default="dary.pirati.cz"
)
DONATE_PORTAL_API_URL = env.str("DONATE_PORTAL_API_URL", default="")
DONATE_PORTAL_API_TIMEOUT = 5
DONATE_PORTAL_API_CACHE_TIMEOUT = 60 * 5

MAILTRAIN_API_URL = env.str("MAILTRAIN_API_URL", default="")
MAILTRAIN_API_TOKEN = env.str("MAILTRAIN_API_TOKEN", default="")

CZECH_INSPIRATIONAL_NEWSLETTER_CID = env.str(
    "CZECH_INSPIRATIONAL_NEWSLETTER_CID", default=""
)

ELECTIONS2021_COOKIE_NAME = "program"
ELECTIONS2021_NEWSLETTER_CID = env.str("ELECTIONS2021_NEWSLETTER_CID", default="")

PIRATICZ_NEWSLETTER_CID = env.str("PIRATICZ_NEWSLETTER_CID", default="")

# URL pointing to MapProxy instance to use.
# MapProxy is used to serve map tiles to hide client details from the tile provider.
# @see: https://mapproxy.org/
MAPS_UTILS_MAPPROXY_URL = env.str(
    "MAPPROXY_URL", default="https://mapproxy.pir-test.eu"
)

TWITTER_BEARER_TOKEN = env.str("TWITTER_BEARER_TOKEN", default="")
