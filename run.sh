#!/bin/bash

# exit on error
set -e

# migrate database
python manage.py migrate

# start webserver
exec gunicorn -c gunicorn.conf.py majak.wsgi
