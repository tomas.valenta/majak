# Generated by Django 5.0.4 on 2024-05-08 09:07

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0128_alter_districtcalendarpage_calendar_and_more"),
    ]

    operations = [
        migrations.RenameField(
            model_name="districthomepage",
            old_name="content",
            new_name="content_2",
        ),
    ]
