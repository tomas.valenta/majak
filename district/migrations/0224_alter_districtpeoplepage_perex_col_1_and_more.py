# Generated by Django 5.0.6 on 2024-06-12 14:18

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0223_remove_districthomepage_gdpr_and_cookies_page"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtpeoplepage",
            name="perex_col_1",
            field=models.TextField(
                blank=True, null=True, verbose_name="Perex - první sloupec"
            ),
        ),
        migrations.AlterField(
            model_name="districtpeoplepage",
            name="perex_col_2",
            field=models.TextField(
                blank=True, null=True, verbose_name="Perex - druhý sloupec"
            ),
        ),
        migrations.AlterField(
            model_name="districtpersonpage",
            name="perex",
            field=models.TextField(blank=True, null=True),
        ),
    ]
