# Generated by Django 4.1.5 on 2023-01-31 19:42

import django.db.models.deletion
import modelcluster.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0102_alter_districtarticlepage_content_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtarticletag",
            name="content_object",
            field=modelcluster.fields.ParentalKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="tagged_items",
                to="district.districtarticlepage",
            ),
        ),
        migrations.AlterField(
            model_name="districtpersontag",
            name="content_object",
            field=modelcluster.fields.ParentalKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="tagged_items",
                to="district.districtpersonpage",
            ),
        ),
    ]
