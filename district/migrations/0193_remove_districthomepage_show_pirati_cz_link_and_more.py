# Generated by Django 5.0.4 on 2024-05-19 09:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0192_remove_districtarticlepage_thumb_image_and_more"),
        ("main", "0085_alter_mainpersonpage_primary_group"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="districthomepage",
            name="show_pirati_cz_link",
        ),
        migrations.AddField(
            model_name="districthomepage",
            name="gdpr_and_cookies_page",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="main.mainsimplepage",
                verbose_name="Stránka pro GDPR",
            ),
        ),
    ]
