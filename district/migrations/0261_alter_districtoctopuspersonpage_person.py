# Generated by Django 5.0.7 on 2024-07-30 10:46

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0260_districtoctopuspersonpage"),
        ("shared", "0009_octopusperson_username"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtoctopuspersonpage",
            name="person",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="shared.octopusperson",
                verbose_name="Osoba",
            ),
        ),
    ]
