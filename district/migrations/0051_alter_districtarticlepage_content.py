# Generated by Django 4.0.3 on 2022-04-01 14:55

import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0050_alter_districtcrossroadpage_content_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtarticlepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "text",
                        wagtail.blocks.RichTextBlock(
                            features=[
                                "h2",
                                "h3",
                                "h4",
                                "bold",
                                "italic",
                                "ol",
                                "embed",
                                "ul",
                                "link",
                                "document-link",
                                "image",
                            ],
                            label="Textový editor",
                        ),
                    ),
                    (
                        "gallery",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "gallery_items",
                                    wagtail.blocks.ListBlock(
                                        wagtail.images.blocks.ImageChooserBlock(
                                            label="obrázek", required=True
                                        ),
                                        group="ostatní",
                                        icon="image",
                                        label="Galerie",
                                    ),
                                )
                            ],
                            label="Galerie",
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Článek",
            ),
        ),
    ]
