# Generated by Django 3.2.8 on 2021-11-16 14:53

import wagtail.blocks
import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0007_districtpeoplepage_content"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtpeoplepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "group_title",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek", required=True
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.PageChooserBlock(
                                            label="Osoba",
                                            page_type=["district.DistrictPersonPage"],
                                        ),
                                        label="List osob",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                verbose_name="Obsah stránky",
            ),
        ),
        migrations.AlterField(
            model_name="districtprogrampage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "static_program_block",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "headline",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek bloku", required=True
                                    ),
                                ),
                                (
                                    "perex",
                                    wagtail.blocks.TextBlock(
                                        label="Krátký text pod nadpisem", required=True
                                    ),
                                ),
                                (
                                    "completion_percentage",
                                    wagtail.blocks.IntegerBlock(
                                        label="Procento dokončení", required=True
                                    ),
                                ),
                                (
                                    "program_items",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "title",
                                                    wagtail.blocks.CharBlock(
                                                        label="Název", required=True
                                                    ),
                                                ),
                                                (
                                                    "completion_percentage",
                                                    wagtail.blocks.IntegerBlock(
                                                        label="Procento dokončení",
                                                        required=True,
                                                    ),
                                                ),
                                            ]
                                        )
                                    ),
                                ),
                                (
                                    "person",
                                    wagtail.blocks.PageChooserBlock(
                                        label="Garant",
                                        page_type=["district.DistrictPersonPage"],
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "redmine_program_block",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "headline",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek bloku", required=True
                                    ),
                                ),
                                (
                                    "perex",
                                    wagtail.blocks.TextBlock(
                                        label="Krátký text pod nadpisem", required=True
                                    ),
                                ),
                                (
                                    "person",
                                    wagtail.blocks.PageChooserBlock(
                                        label="Garant",
                                        page_type=["district.DistrictPersonPage"],
                                    ),
                                ),
                                (
                                    "redmine_issue",
                                    wagtail.blocks.IntegerBlock(
                                        label="Číslo Redmine issue", required=True
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                verbose_name="obsah stránky",
            ),
        ),
    ]
