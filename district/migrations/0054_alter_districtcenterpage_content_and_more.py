# Generated by Django 4.0.3 on 2022-04-14 14:06

import wagtail.blocks
import wagtail.contrib.table_block.blocks
import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0053_alter_districtcrossroadpage_cards_content"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtcenterpage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    ("text", wagtail.blocks.RichTextBlock()),
                    (
                        "table",
                        wagtail.contrib.table_block.blocks.TableBlock(
                            table_options={"renderer": "html"}
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Obsah",
            ),
        ),
        migrations.AlterField(
            model_name="districtcustompage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    ("text", wagtail.blocks.RichTextBlock()),
                    (
                        "table",
                        wagtail.contrib.table_block.blocks.TableBlock(
                            table_options={"renderer": "html"}
                        ),
                    ),
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "group_title",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek", required=True
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.PageChooserBlock(
                                            label="Osoba",
                                            page_type=[
                                                "district.DistrictPersonPage",
                                            ],
                                        ),
                                        label="List osob",
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Obsah",
            ),
        ),
    ]
