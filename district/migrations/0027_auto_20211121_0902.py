# Generated by Django 3.2.8 on 2021-11-21 08:02

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0026_auto_20211121_0902"),
    ]

    operations = [
        migrations.AddField(
            model_name="districthomepage",
            name="footperson_coord",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="district.districtpersonpage",
                verbose_name="Koordinátor",
            ),
        ),
        migrations.AddField(
            model_name="districthomepage",
            name="footperson_electman",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="district.districtpersonpage",
                verbose_name="Volební manažer",
            ),
        ),
        migrations.AddField(
            model_name="districthomepage",
            name="footperson_media",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="district.districtpersonpage",
                verbose_name="Kontakt pro média",
            ),
        ),
    ]
