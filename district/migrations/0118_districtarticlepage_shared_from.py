# Generated by Django 4.1.8 on 2023-07-23 00:31

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailcore", "0083_workflowcontenttype"),
        ("district", "0117_merge_20230711_1636"),
    ]

    operations = [
        migrations.AddField(
            model_name="districtarticlepage",
            name="shared_from",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailcore.page",
            ),
        ),
    ]
