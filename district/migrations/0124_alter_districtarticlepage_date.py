# Generated by Django 4.1.10 on 2024-01-18 18:54

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0123_alter_districtcalendarpage_calendar_url_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtarticlepage",
            name="date",
            field=models.DateTimeField(
                default=django.utils.timezone.now, verbose_name="Datum a čas"
            ),
        ),
    ]
