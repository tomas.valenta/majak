# Generated by Django 5.0.7 on 2024-10-02 19:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0289_alter_districtpeoplepage_content"),
    ]

    operations = [
        migrations.AddField(
            model_name="districtoctopuspersonpage",
            name="originating_team",
            field=models.CharField(
                blank=True,
                help_text="Tým, ze kterého byla tato osba importována.",
                max_length=128,
                null=True,
                verbose_name="Tým",
            ),
        ),
    ]
