# Generated by Django 4.0.4 on 2022-05-20 14:34

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailimages", "0023_add_choose_permissions"),
        ("district", "0072_alter_districtprogrampage_content"),
    ]

    operations = [
        migrations.AddField(
            model_name="districtgeofeaturecollectionpage",
            name="logo_image",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailimages.image",
                verbose_name="Logo",
            ),
        ),
        migrations.AlterField(
            model_name="districtgeofeaturecollectionpage",
            name="image",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailimages.image",
                verbose_name="Obrázek na pozadí",
            ),
        ),
    ]
