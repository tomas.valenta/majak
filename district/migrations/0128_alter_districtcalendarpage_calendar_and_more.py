# Generated by Django 5.0.4 on 2024-05-06 10:34

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("calendar_utils", "0004_auto_20220505_1228"),
        ("district", "0127_alter_districthomepage_menu"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtcalendarpage",
            name="calendar",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="calendar_utils.calendar",
            ),
        ),
        migrations.AlterField(
            model_name="districtcenterpage",
            name="calendar",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="calendar_utils.calendar",
            ),
        ),
        migrations.AlterField(
            model_name="districthomepage",
            name="calendar",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="calendar_utils.calendar",
            ),
        ),
        migrations.AlterField(
            model_name="districtpersonpage",
            name="calendar",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="calendar_utils.calendar",
            ),
        ),
    ]
