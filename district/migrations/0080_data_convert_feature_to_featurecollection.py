# Generated by Django 4.0.4 on 2022-05-31 12:33

import json

from django.db import migrations


def feature_to_collection(value):
    return {"type": "FeatureCollection", "features": [value]}


def collection_to_feature(value):
    return value["features"][0]


def migrate(apps, mapper):
    DistrictGeoFeatureDetailPage = apps.get_model(
        "district", "DistrictGeoFeatureDetailPage"
    )

    for page in DistrictGeoFeatureDetailPage.objects.all():
        page.geojson = json.dumps(mapper(json.loads(page.geojson)))
        page.save()


def forwards(apps, schema_editor):
    migrate(apps, feature_to_collection)


def backwards(apps, schema_editor):
    migrate(apps, collection_to_feature)


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0079_alter_districtarticlepage_content_and_more"),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
