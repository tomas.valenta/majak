# Generated by Django 5.0.4 on 2024-05-08 09:08

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0129_rename_content_districthomepage_content_2"),
    ]

    operations = [
        migrations.RenameField(
            model_name="districthomepage",
            old_name="subheader",
            new_name="content",
        ),
        migrations.RemoveField(
            model_name="districthomepage",
            name="content_2",
        ),
    ]
