# Generated by Django 4.1.8 on 2023-05-02 14:33

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        (
            "district",
            "0110_districtelectioncampaignpage_campaign_funding_info_and_more",
        ),
        ("district", "0110_districthomepage_footer_extra_content"),
    ]

    operations = []
