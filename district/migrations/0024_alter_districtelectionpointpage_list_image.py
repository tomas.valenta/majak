# Generated by Django 3.2.8 on 2021-11-19 09:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailimages", "0023_add_choose_permissions"),
        ("district", "0023_auto_20211119_1018"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtelectionpointpage",
            name="list_image",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailimages.image",
            ),
        ),
    ]
