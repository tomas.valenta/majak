# Generated by Django 3.2.8 on 2022-01-14 14:27

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailimages", "0023_add_choose_permissions"),
        ("district", "0030_alter_districtelectionpage_content"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="districtpersonpage",
            name="person",
        ),
        migrations.AlterField(
            model_name="districtpersonpage",
            name="background_photo",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailimages.image",
                verbose_name="obrázek do záhlaví",
            ),
        ),
        migrations.AlterField(
            model_name="districtpersonpage",
            name="profile_photo",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="+",
                to="wagtailimages.image",
                verbose_name="profilová fotka",
            ),
        ),
    ]
