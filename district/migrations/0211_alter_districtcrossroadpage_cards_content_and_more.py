# Generated by Django 5.0.4 on 2024-05-28 11:54

import django.db.models.deletion
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtailmetadata.models
from django.db import migrations, models

import shared.models.main


class Migration(migrations.Migration):
    dependencies = [
        ("district", "0210_remove_districtcenterpage_sidebar_content_and_more"),
        ("wagtailcore", "0093_uploadedfile"),
        ("wagtailimages", "0026_delete_uploadedimage"),
    ]

    operations = [
        migrations.AlterField(
            model_name="districtcrossroadpage",
            name="cards_content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "cards",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "headline",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek bloku", required=False
                                    ),
                                ),
                                (
                                    "card_items",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "image",
                                                    wagtail.images.blocks.ImageChooserBlock(
                                                        label="Obrázek"
                                                    ),
                                                ),
                                                (
                                                    "title",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek", required=True
                                                    ),
                                                ),
                                                (
                                                    "text",
                                                    wagtail.blocks.RichTextBlock(
                                                        label="Krátký text pod nadpisem",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "page",
                                                    wagtail.blocks.PageChooserBlock(
                                                        label="Stránka",
                                                        page_type=[
                                                            "district.DistrictArticlesPage",
                                                            "district.DistrictCenterPage",
                                                            "district.DistrictContactPage",
                                                            "district.DistrictCrossroadPage",
                                                            "district.DistrictCustomPage",
                                                            # "district.DistrictElectionRootPage",
                                                            "district.DistrictPeoplePage",
                                                            # "district.DistrictProgramPage",
                                                            # "district.DistrictInteractiveProgramPage",
                                                            "district.DistrictGeoFeatureCollectionPage",
                                                            "district.DistrictCalendarPage",
                                                            "district.DistrictPdfPage",
                                                            "district.DistrictNewProgramPage",
                                                        ],
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "link",
                                                    wagtail.blocks.URLBlock(
                                                        label="Odkaz", required=False
                                                    ),
                                                ),
                                            ],
                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                        ),
                                        label="Karty s odkazy",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                verbose_name="Karty rozcestníku",
            ),
        ),
        migrations.AlterField(
            model_name="districtpeoplepage",
            name="people",
            field=wagtail.fields.StreamField(
                [
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titulek")),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug skupiny",
                                        required=False,
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.PageChooserBlock(
                                            label="Detail osoby",
                                            page_type=["district.DistrictPersonPage"],
                                        ),
                                        label="Skupina osob",
                                    ),
                                ),
                            ],
                            label="Seznam osob",
                        ),
                    ),
                    (
                        "team_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "title",
                                    wagtail.blocks.CharBlock(label="Název sekce týmů"),
                                ),
                                (
                                    "slug",
                                    wagtail.blocks.CharBlock(
                                        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
                                        label="Slug sekce",
                                        required=False,
                                    ),
                                ),
                                (
                                    "team_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "headline",
                                                    wagtail.blocks.CharBlock(
                                                        label="Titulek bloku",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "card_items",
                                                    wagtail.blocks.ListBlock(
                                                        wagtail.blocks.StructBlock(
                                                            [
                                                                (
                                                                    "image",
                                                                    wagtail.images.blocks.ImageChooserBlock(
                                                                        label="Obrázek"
                                                                    ),
                                                                ),
                                                                (
                                                                    "title",
                                                                    wagtail.blocks.CharBlock(
                                                                        label="Titulek",
                                                                        required=True,
                                                                    ),
                                                                ),
                                                                (
                                                                    "text",
                                                                    wagtail.blocks.RichTextBlock(
                                                                        label="Krátký text pod nadpisem",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "page",
                                                                    wagtail.blocks.PageChooserBlock(
                                                                        label="Stránka",
                                                                        page_type=[
                                                                            "district.DistrictArticlesPage",
                                                                            "district.DistrictCenterPage",
                                                                            "district.DistrictContactPage",
                                                                            "district.DistrictCrossroadPage",
                                                                            "district.DistrictCustomPage",
                                                                            # "district.DistrictElectionRootPage",
                                                                            "district.DistrictPeoplePage",
                                                                            # "district.DistrictProgramPage",
                                                                            # "district.DistrictInteractiveProgramPage",
                                                                            "district.DistrictGeoFeatureCollectionPage",
                                                                            "district.DistrictCalendarPage",
                                                                            "district.DistrictPdfPage",
                                                                            "district.DistrictNewProgramPage",
                                                                        ],
                                                                        required=False,
                                                                    ),
                                                                ),
                                                                (
                                                                    "link",
                                                                    wagtail.blocks.URLBlock(
                                                                        label="Odkaz",
                                                                        required=False,
                                                                    ),
                                                                ),
                                                            ],
                                                            template="styleguide2/includes/molecules/boxes/card_box_block.html",
                                                        ),
                                                        label="Karty s odkazy",
                                                    ),
                                                ),
                                            ],
                                            label="Karta týmu",
                                        ),
                                        label="Týmy",
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Lidé a týmy",
            ),
        ),
        migrations.CreateModel(
            name="DistrictNewProgramPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "program",
                    wagtail.fields.StreamField(
                        [
                            (
                                "program_group",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(
                                                label="Titulek části programu"
                                            ),
                                        ),
                                        (
                                            "point_list",
                                            wagtail.blocks.ListBlock(
                                                wagtail.blocks.StructBlock(
                                                    [
                                                        (
                                                            "url",
                                                            wagtail.blocks.URLBlock(
                                                                label="Odkaz pokrývající celou tuto část",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "icon",
                                                            wagtail.images.blocks.ImageChooserBlock(
                                                                label="Ikona",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "title",
                                                            wagtail.blocks.CharBlock(
                                                                label="Titulek článku programu"
                                                            ),
                                                        ),
                                                        (
                                                            "text",
                                                            wagtail.blocks.RichTextBlock(
                                                                features=[
                                                                    "h3",
                                                                    "h4",
                                                                    "h5",
                                                                    "bold",
                                                                    "italic",
                                                                    "ol",
                                                                    "ul",
                                                                    "hr",
                                                                    "link",
                                                                    "document-link",
                                                                    "image",
                                                                    "superscript",
                                                                    "subscript",
                                                                    "strikethrough",
                                                                    "blockquote",
                                                                    "embed",
                                                                ],
                                                                label="Obsah",
                                                            ),
                                                        ),
                                                    ]
                                                ),
                                                label="Jednotlivé články programu",
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                            (
                                "program_group_crossroad",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(
                                                label="Titulek části programu"
                                            ),
                                        ),
                                        (
                                            "point_list",
                                            wagtail.blocks.ListBlock(
                                                wagtail.blocks.StructBlock(
                                                    [
                                                        (
                                                            "image",
                                                            wagtail.images.blocks.ImageChooserBlock(
                                                                label="Obrázek"
                                                            ),
                                                        ),
                                                        (
                                                            "title",
                                                            wagtail.blocks.CharBlock(
                                                                label="Titulek",
                                                                required=True,
                                                            ),
                                                        ),
                                                        (
                                                            "text",
                                                            wagtail.blocks.RichTextBlock(
                                                                label="Krátký text pod nadpisem",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "page",
                                                            wagtail.blocks.PageChooserBlock(
                                                                label="Stránka",
                                                                page_type=[
                                                                    "district.DistrictArticlePage",
                                                                    "district.DistrictArticlesPage",
                                                                    "district.DistrictCenterPage",
                                                                    "district.DistrictContactPage",
                                                                    "district.DistrictCrossroadPage",
                                                                    "district.DistrictCustomPage",
                                                                    # "district.DistrictElectionCampaignPage",
                                                                    # "district.DistrictElectionProgramPage",
                                                                    # "district.DistrictElectionRootPage",
                                                                    "district.DistrictPeoplePage",
                                                                    "district.DistrictPersonPage",
                                                                    # "district.DistrictPostElectionStrategyPage",
                                                                    # "district.DistrictProgramPage",
                                                                ],
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "link",
                                                            wagtail.blocks.URLBlock(
                                                                label="Odkaz",
                                                                required=False,
                                                            ),
                                                        ),
                                                    ]
                                                ),
                                                label="Karty programu",
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                            (
                                "program_group_popout",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(
                                                label="Titulek části programu"
                                            ),
                                        ),
                                        (
                                            "categories",
                                            wagtail.blocks.ListBlock(
                                                wagtail.blocks.StructBlock(
                                                    [
                                                        (
                                                            "name",
                                                            wagtail.blocks.CharBlock(
                                                                label="Název"
                                                            ),
                                                        ),
                                                        (
                                                            "icon",
                                                            wagtail.images.blocks.ImageChooserBlock(
                                                                label="Ikona",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "description",
                                                            wagtail.blocks.RichTextBlock(
                                                                label="Popis",
                                                                required=False,
                                                            ),
                                                        ),
                                                        (
                                                            "point_list",
                                                            wagtail.blocks.ListBlock(
                                                                wagtail.blocks.StructBlock(
                                                                    [
                                                                        (
                                                                            "title",
                                                                            wagtail.blocks.CharBlock(
                                                                                label="Titulek vyskakovacího bloku"
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "content",
                                                                            wagtail.blocks.RichTextBlock(
                                                                                features=[
                                                                                    "h3",
                                                                                    "h4",
                                                                                    "h5",
                                                                                    "bold",
                                                                                    "italic",
                                                                                    "ol",
                                                                                    "ul",
                                                                                    "hr",
                                                                                    "link",
                                                                                    "document-link",
                                                                                    "image",
                                                                                    "superscript",
                                                                                    "subscript",
                                                                                    "strikethrough",
                                                                                    "blockquote",
                                                                                    "embed",
                                                                                ],
                                                                                label="Obsah",
                                                                            ),
                                                                        ),
                                                                    ]
                                                                ),
                                                                label="Jednotlivé bloky programu",
                                                            ),
                                                        ),
                                                    ]
                                                ),
                                                label="Kategorie programu",
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                        ],
                        blank=True,
                        verbose_name="Program",
                    ),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Program",
                "abstract": False,
            },
            bases=(
                shared.models.main.SubpageMixin,
                wagtailmetadata.models.WagtailImageMetadataMixin,
                "wagtailcore.page",
                models.Model,
            ),
        ),
    ]
