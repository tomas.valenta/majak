from wagtail import hooks

from district.models import DistrictCenterPage, DistrictHomePage


@hooks.register("after_copy_page")
def handle_copy_calendar(request, origin_page, copied_page):
    if (
        isinstance(copied_page, DistrictHomePage)
        or isinstance(copied_page, DistrictCenterPage)
    ) and origin_page.specific.calendar_id:
        # get current calendar from origin page
        calendar = origin_page.specific.calendar

        # create copy of calendar
        calendar.pk = None
        calendar.save()

        # set new copy of calendar
        copied_page.calendar_id = calendar.id
        copied_page.save()
