from django.template.defaultfilters import slugify
from django.utils.text import slugify
from wagtail import blocks
from django.utils.safestring import mark_safe
from wagtail.blocks import (
    CharBlock,
    DateTimeBlock,
    ListBlock,
    PageChooserBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from shared.blocks import CandidateBlock as SharedCandidateBlockMixin
from shared.blocks import CandidateListBlock as SharedCandidateListBlockMixin
from shared.blocks import (
    CandidateSecondaryListBlock as SharedCandidateSecondaryListBlockMixin,
)
from shared.blocks import (
    CardLinkBlockMixin,
    CardLinkWithHeadlineBlockMixin,
    CarouselProgramBlock,
    CustomPrimaryCandidateBlock,
    PeopleGroupBlockMixin,
    PersonContactBlockMixin,
    PersonCustomPositionBlockMixin,
    ProgramGroupBlock,
    ProgramGroupBlockMixin,
)
from shared.blocks import ProgramGroupBlockPopout as SharedProgramGroupBlockPopout
from shared.blocks import (
    ProgramGroupWithCandidatesBlock as SharedProgramGroupWithCandidatesBlockMixin,
)
from shared.blocks import SecondaryCandidateBlock as SharedSecondaryCandidateBlockMixin
from shared.blocks import TeamBlockMixin

# --- BEGIN New blocks ---


class FullscreenHeaderBlock(StructBlock):
    desktop_image = ImageChooserBlock(
        label="Obrázek na pozadí (desktop)",
        help_text="Pokud není vybráno video, ukáže se na desktopu.",
        required=False,
    )

    mobile_image = ImageChooserBlock(
        label="Obrázek na pozadí (mobil)",
        help_text="Pokud není vybráno video, ukáže se na mobilu.",
        required=False,
    )

    desktop_video_url = URLBlock(
        label="Video (desktop)",
        help_text="Pokud je vybráno, ukáže se na desktopech s povoleným autoplayem místo obrázku.",
        required=False,
    )

    mobile_video_url = URLBlock(
        label="Video (mobil)",
        help_text="Pokud je vybráno, ukáže se na mobilech s povoleným autoplayem místo obrázku.",
        required=False,
    )

    desktop_line_1 = TextBlock(label="Desktop první řádek", required=False)
    desktop_line_2 = TextBlock(label="Desktop druhý řádek", required=False)

    mobile_line_1 = TextBlock(label="První mobilní řádek", required=False)
    mobile_line_2 = TextBlock(label="Druhý mobilní řádek", required=False)
    mobile_line_3 = TextBlock(label="Třetí mobilní řádek", required=False)

    button_url = URLBlock(
        label="Odkaz tlačítka",
        help_text="Bez odkazu tlačítko nebude viditelné.",
        required=False,
    )
    button_text = CharBlock(
        label="Text tlačítka",
        help_text="Odkaz funguje i bez tlačítka. Pokud chceš tlačítko skrýt, nevyplňuj text.",
        required=False,
    )

    class Meta:
        template = "styleguide2/includes/molecules/menus/district/carousel.html"
        icon = "form"
        label = "Header"


class ButtonBlock(blocks.StructBlock):
    title = blocks.CharBlock(label="Titulek", max_length=128, required=True)

    page = blocks.PageChooserBlock(label="Stránka", required=False)

    link = blocks.URLBlock(label="Odkaz", required=False)

    class Meta:
        label = "Tlačítko"


class ElectionsCountdownBlock(StructBlock):
    title = CharBlock(label="Titulek", required=True)

    text_before_countdown = CharBlock(
        label="Text před odpočtem", default="Aktuálně zbývá", required=True
    )

    countdown_timestamp = DateTimeBlock(label="Datum & čas voleb", required=True)

    buttons = ListBlock(ButtonBlock(), label="Tlačítka", required=False)

    class Meta:
        label = "Odpočet do voleb"
        icon = "time"
        template = "styleguide2/includes/organisms/main_section/district/elections.html"


class NewsletterBlock(StructBlock):
    class Meta:
        label = "Newsletter banner"
        icon = "mail"
        template = "styleguide2/includes/organisms/main_section/newsletter_section.html"


class PersonCustomPositionBlock(PersonCustomPositionBlockMixin):
    page = PageChooserBlock(
        page_type=[
            "district.DistrictOctopusPersonPage",
            "district.DistrictManualOctopusPersonPage",
            "district.DistrictPersonPage",
        ],
        label="Detail osoby",
    )


class OctopusMixin(blocks.StructBlock):
    title = CharBlock(label="Titulek", required=True)

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value

    def get_context(self, value, *args, **kwargs):
        from .models import DistrictOctopusPersonPage

        context = super().get_context(value, *args, **kwargs)

        context["person_list"] = (
            DistrictOctopusPersonPage.objects.filter(
                originating_group=value["group_shortcut"]
            )
            .order_by("title")
            .all()
        )

        return context

    class Meta:
        abstract = True
        label = "Osoby z Chobotnice"
        icon = "group"
        template = "styleguide2/includes/organisms/cards/people_card_list.html"


class OctopusGroupBlock(OctopusMixin):
    slug = blocks.CharBlock(
        label="Slug bloku",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
    )

    group_shortcut = CharBlock(
        label="Zkratka skupiny", help_text="Např. cen_to_ved", required=True
    )

    class Meta:
        label = "Skupina osob z Chobotnice"


class OctopusTeamBlock(OctopusMixin):
    slug = blocks.CharBlock(
        label="Slug bloku",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
    )

    team_shortcut = CharBlock(
        label="Zkratka týmu", help_text="Např. TO-admin", required=True
    )

    roles = CharBlock(
        label="Role",
        help_text=mark_safe(
            "Pokud chceš zobrazit pouze osoby se specifickými rolemi "
            "v tomto týmu, můžeš role zadat zde ve formátu "
            "<em>role1,role2,role3</em>."
        ),
        required=False
    )

    def get_context(self, value, *args, **kwargs):
        from .models import DistrictOctopusPersonPage

        # Skip OctopusMixin
        context = blocks.StructBlock.get_context(self, value, *args, **kwargs)

        filter = Q(
            originating_team=value["team_shortcut"],
        )

        if value["roles"]:
            filter = filter & Q(originating_role=value["roles"].split(","))

        context["person_list"] = (
            DistrictOctopusPersonPage.objects.filter(filter)
            .order_by("title")
            .all()
        )

        return context

    class Meta:
        label = "Tým osob z Chobotnice"


class PeopleGroupBlock(PeopleGroupBlockMixin):
    person_list = blocks.ListBlock(
        blocks.PageChooserBlock(
            page_type=[
                "district.DistrictPersonPage",
                "district.DistrictOctopusPersonPage",
                "district.DistrictManualOctopusPersonPage",
            ],
            label="Detail osoby",
        ),
        default=[],
        label="Osoby",
        help_text="S pozicemi z jejich podstránek",
    )

    person_list_with_custom_positions = ListBlock(
        PersonCustomPositionBlock(),
        default=[],
        label="Osoby",
        help_text="S nastavitelnými pozicemi",
    )


class CardLinkBlock(CardLinkBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "district.DistrictArticlesPage",
            "district.DistrictCenterPage",
            "district.DistrictContactPage",
            "district.DistrictCrossroadPage",
            "district.DistrictCustomPage",
            "district.DistrictPeoplePage",
            "district.DistrictGeoFeatureCollectionPage",
            "district.DistrictCalendarPage",
            "district.DistrictPdfPage",
            "district.DistrictNewProgramPage",
        ],
        required=False,
    )

    class Meta:
        template = "styleguide2/includes/molecules/boxes/card_box_block.html"
        icon = "link"
        label = "Karta s odkazem"


class CardLinkWithHeadlineBlock(CardLinkWithHeadlineBlockMixin):
    card_items = ListBlock(
        CardLinkBlock(
            template="styleguide2/includes/molecules/boxes/card_box_block.html"
        ),
        label="Karty s odkazy",
    )

    class Meta:
        template = (
            "styleguide2/includes/molecules/boxes/card_box_with_headline_block.html"
        )
        icon = "link"
        label = "Karty odkazů s nadpisem"


class TeamBlock(TeamBlockMixin):
    team_list = ListBlock(
        CardLinkWithHeadlineBlock(label="Karta týmu"),
        label="Týmy",
    )


class AddressBlock(StructBlock):
    title = CharBlock(label="Titulek", required=True)
    map_link = URLBlock(label="Odkaz na detail mapy", required=False)
    address = TextBlock(label="Adresa", required=True)
    address_info = TextBlock(label="Info k adrese", required=False)

    class Meta:
        template = "styleguide2/includes/atoms/text/address.html"
        icon = "home"
        label = "Adresa"


class PersonContactBlock(PersonContactBlockMixin):
    person = PageChooserBlock(
        label="Osoba",
        page_type=[
            "district.DistrictPersonPage",
            "district.DistrictOctopusPersonPage",
            "district.DistrictManualOctopusPersonPage",
        ],
    )


class CenterContactBlock(StructBlock):
    title = CharBlock(label="Titulek", required=True)
    contact_list = ListBlock(PersonContactBlock())

    class Meta:
        template = "styleguide2/includes/molecules/contact/center_contacts.html"
        icon = "mail"
        label = "Kontakt"


# --- END New blocks ---


class SimplePersonBlock(StructBlock):
    title = blocks.CharBlock(label="Jméno a příjmení", max_length=128, required=True)
    job = blocks.CharBlock(
        label="Povolání", max_length=128, required=False, help_text="Např. 'Informatik'"
    )
    profile_photo = ImageChooserBlock(label="Profilová fotka", required=False)
    email = blocks.EmailBlock(label="Email", required=False)
    city = blocks.CharBlock(label="Město/obec", max_length=64, required=False)
    age = blocks.IntegerBlock(label="Věk", required=False)
    is_pirate = blocks.BooleanBlock(
        label="Je členem Pirátské strany?", default=True, required=False
    )
    other_party = blocks.CharBlock(label="Strana", max_length=64, required=False)
    other_party_logo = ImageChooserBlock(
        label="Logo strany", required=False, help_text="Vyplňte pokud osoba není Pirát"
    )

    # socials
    facebook_url = blocks.URLBlock(label="Odkaz na Facebook", required=False)
    instagram_url = blocks.URLBlock(label="Odkaz na Instagram", required=False)
    twitter_url = blocks.URLBlock(label="Odkaz na Twitter", required=False)
    youtube_url = blocks.URLBlock(label="Odkaz na Youtube kanál", required=False)
    flickr_url = blocks.URLBlock(label="Odkaz na Flickr", required=False)

    class Meta:
        icon = "user"
        label = "Osoba blok"


class PersonBadgeBlock(StructBlock):
    person = PageChooserBlock(
        label="Osoba",
        page_type=(
            "district.DistrictPersonPage",
            "district.DistrictOctopusPersonPage",
            "district.DistrictManualOctopusPersonPage",
        ),
        required=True,
    )
    caption = CharBlock(
        label="Popisek",
        required=False,
        help_text="Vlastní popisek na vizitce. Pokud není uvedeno, použije se výchozí profese osoby.",
    )

    class Meta:
        template = "styleguide2/includes/molecules/contact/person_badge.html"
        icon = "user"
        label = "Vizitka"


class PersonUrlBlock(StructBlock):
    title = CharBlock(label="Název", required=True)
    url = URLBlock(label="URL", required=True)
    custom_icon = CharBlock(
        label="Vlastní ikonka ze styleguide",
        required=False,
        help_text="Pro vlastní ikonku zadejde název ikonky z https://styleguide.pirati.cz/latest/?p=viewall-atoms-icons (bez tečky), např. 'ico--beer'",
    )


class ArticlesBlock(StructBlock):
    articles = ListBlock(
        PageChooserBlock(
            "district.DistrictArticlePage", required=True, label="Aktualita"
        ),
        label="Seznam aktualit",
        required=True,
    )

    class Meta:
        template = "styleguide2/includes/organisms/articles/article_list_block.html"
        icon = "list-ul"
        label = "Blok aktualit"


class ArticleLinksBlock(StructBlock):
    articles = ListBlock(
        PageChooserBlock(
            "district.DistrictArticlePage", required=True, label="Aktualita"
        ),
        label="Seznam aktualit",
        required=True,
    )

    class Meta:
        template = "styleguide2/includes/organisms/articles/article_links_block.html"
        icon = "list-ul"
        label = "Seznam nadpisů článků"


class ProgramGroupBlockCrossroad(ProgramGroupBlockMixin):
    point_list = ListBlock(CardLinkBlock(), label="Karty programu")

    class Meta:
        icon = "date"
        label = "Rozcestníkový program"


class ProgramGroupBlockPopout(SharedProgramGroupBlockPopout, ProgramGroupBlockMixin):
    pass


class CandidateBlock(SharedCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "district.DistrictPersonPage",
            "district.DistrictOctopusPersonPage",
            "district.DistrictManualOctopusPersonPage",
        ],
    )


class SecondaryCandidateBlock(SharedSecondaryCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "district.DistrictPersonPage",
            "district.DistrictOctopusPersonPage",
            "district.DistrictManualOctopusPersonPage",
        ],
    )


class CandidateListBlock(SharedCandidateListBlockMixin):
    stream_candidates = StreamBlock(
        [
            ("candidate", CandidateBlock()),
            ("custom_candidate", CustomPrimaryCandidateBlock()),
        ],
        required=False,
        label=" ",  # Hacky way to show no label
    )


class CandidateSecondaryListBlock(SharedCandidateSecondaryListBlockMixin):
    candidates = ListBlock(
        SecondaryCandidateBlock(),
        min_num=0,
        default=[],
        label=" ",  # Hacky way to show no label
    )


class ProgramGroupWithCandidatesBlock(SharedProgramGroupWithCandidatesBlockMixin):
    primary_candidates = CandidateListBlock(
        label="Osoby na čele kandidátky",
        help_text="Zobrazí se ve velkých blocích na začátku stránky.",
    )

    secondary_candidates = CandidateSecondaryListBlock(
        label="Ostatní osoby na kandidátce",
        help_text="Zobrazí se v kompaktním seznamu pod čelem kandidátky. Níže můžeš změnit nadpis.",
    )

    program = StreamBlock(
        [
            ("program_group", ProgramGroupBlock()),
            ("program_group_crossroad", ProgramGroupBlockCrossroad()),
            ("program_group_popout", ProgramGroupBlockPopout()),
            (
                "carousel_program",
                CarouselProgramBlock(
                    template="styleguide2/includes/molecules/program/program_block.html"
                ),
            ),
        ],
        required=False,
    )
