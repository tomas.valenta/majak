from django import template
from django.db import models

register = template.Library()


@register.filter
def get_block_octopus_person_list(block):
    from district.models import DistrictOctopusPersonPage

    filter = (
        models.Q(originating_group=block.value["group_shortcut"])
        if "group_shortcut" in block.value
        else (
            models.Q(
                originating_team=block.value["team_shortcut"],
                originating_role__in=block.value["roles"].split(",")
            )
            if block.value["roles"]
            else models.Q(originating_team=block.value["team_shortcut"])
        )
    )

    for page in DistrictOctopusPersonPage.objects.filter(models.Q(originating_team=block.value["team_shortcut"])).order_by("title").distinct("title").all():
        print(page.originating_team)

    return DistrictOctopusPersonPage.objects.filter(filter).order_by("title").distinct("title").all()
