from django.core.management.base import BaseCommand
from wagtail.models.media import Collection

from district.models import DistrictManualOctopusPersonPage, DistrictPeoplePage
from district.tasks import (
    import_manual_person,
    import_people_from_group,
    import_people_from_team,
)


class Command(BaseCommand):
    help = """Importuje Osoby z Chobotnice."""

    def handle(self, *args, **options):
        for people_page in DistrictPeoplePage.objects.all():
            for group in people_page.get_syncable_octopus_groups():
                collection_id = people_page.root_page.image_collection_id

                if collection_id is None:
                    collection_id = Collection.objects.first().id

                import_people_from_group.delay(
                    people_page.id,
                    collection_id,
                    group["shortcut"],
                    group["title"],
                )

        for people_page in DistrictPeoplePage.objects.all():
            for team in people_page.get_syncable_octopus_teams():
                collection_id = people_page.root_page.image_collection_id

                if collection_id is None:
                    collection_id = Collection.objects.first().id

                import_people_from_team.delay(
                    people_page.id,
                    collection_id,
                    team["shortcut"],
                    team["title"],
                    team["roles"]
                )

        for person_page in DistrictManualOctopusPersonPage.objects.all():
            import_manual_person.delay(person_page.id, collection_id)
