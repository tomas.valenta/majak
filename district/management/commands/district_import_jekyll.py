from django.core.management.base import BaseCommand

from ...jekyll_import import JekyllArticleImporter


class Command(BaseCommand):
    help = """Importuje články z pirátského jekyll webu."""

    def add_arguments(self, parser):
        parser.add_argument("path", help="Cesta k jekyll repu")
        parser.add_argument(
            "--hostname",
            default=None,
            help="Hostname webu, defaultně vezme první site (/admin/sites/)",
        )
        parser.add_argument(
            "--clanky-id",
            required=True,
            type=int,
            help="Id nadstránky článků (DistrictArticlesPage), pod kterou se články naimportují",
        )
        parser.add_argument(
            "--kolekce-id",
            required=True,
            type=int,
            help="Id kolekce (Collection) pro import obrázků.",
        )
        parser.add_argument(
            "--dry-run",
            default=False,
            type=bool,
            help="Zda je o testovací běh.",
        )
        parser.add_argument(
            "--use-git",
            default=True,
            type=bool,
            help="Zda je o testovací běh.",
        )

    def handle(self, *args, **options):
        return JekyllArticleImporter(
            article_parent_page_id=options["clanky_id"],
            collection_id=options["kolekce_id"],
            url=options["path"],
            dry_run=options["dry_run"],
            use_git=options["use_git"],
        ).perform_import()
