import logging
import os
import tempfile

from celery import shared_task

from shared.jekyll_import import JekyllArticleImporter
from shared.people_import import PeopleGroupImporter, PeopleTeamImporter, PersonImporter

logger = logging.getLogger(__name__)


@shared_task()
def import_jekyll_articles(
    article_parent_page_id,
    collection_id,
    url,
    dry_run,
    use_git,
):
    from .models import DistrictArticlePage, DistrictArticlesPage

    return JekyllArticleImporter(
        article_parent_page_id=article_parent_page_id,
        article_parent_page_model=DistrictArticlesPage,
        collection_id=collection_id,
        url=url,
        dry_run=dry_run,
        use_git=use_git,
        page_model=DistrictArticlePage,
    ).perform_import()


@shared_task()
def import_people_from_group(
    people_parent_page_id, collection_id, group_shortcut, group_display
):
    from .models import DistrictOctopusPersonPage, DistrictPeoplePage

    lock_file_name = os.path.join(
        tempfile.gettempdir(),
        f"{people_parent_page_id}-{group_shortcut}.people-from-group-import-lock",
    )

    if os.path.isfile(lock_file_name):
        return

    open(lock_file_name, "w").close()

    return PeopleGroupImporter(
        people_parent_page_id=people_parent_page_id,
        people_parent_page_model=DistrictPeoplePage,
        person_page_model=DistrictOctopusPersonPage,
        collection_id=collection_id,
        group_shortcut=group_shortcut,
        group_display=group_display,
        lock_file_name=lock_file_name,
    ).perform_import()


@shared_task()
def import_people_from_team(
    people_parent_page_id,
    collection_id,
    team_shortcut,
    team_display,
    team_roles,
):
    from .models import DistrictOctopusPersonPage, DistrictPeoplePage

    if team_roles is None:
        team_roles = []

    lock_file_name = os.path.join(
        tempfile.gettempdir(),
        f"{people_parent_page_id}-{team_shortcut}-{','.join(team_roles)}.people-from-team-import-lock",
    )

    if os.path.isfile(lock_file_name):
        return

    open(lock_file_name, "w").close()

    return PeopleTeamImporter(
        people_parent_page_id=people_parent_page_id,
        people_parent_page_model=DistrictPeoplePage,
        person_page_model=DistrictOctopusPersonPage,
        collection_id=collection_id,
        team_shortcut=team_shortcut,
        team_display=team_display,
        team_roles=team_roles,
        lock_file_name=lock_file_name,
    ).perform_import()


@shared_task()
def import_manual_person(
    person_page_id,
    collection_id,
):
    from .models import DistrictManualOctopusPersonPage

    lock_file_name = os.path.join(
        tempfile.gettempdir(),
        f"{person_page_id}.person-import-lock",
    )

    if os.path.isfile(lock_file_name):
        return

    open(lock_file_name, "w").close()

    return PersonImporter(
        person_page_model=DistrictManualOctopusPersonPage,
        person_page_id=person_page_id,
        collection_id=collection_id,
        lock_file_name=lock_file_name,
    ).perform_import()
