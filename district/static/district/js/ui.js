$(window).ready(
	function(event) {
		const modalScript = document.createElement("script");
		modalScript.src = "/static/district/js/jquery.modal.js";
		document.head.append(modalScript);

		$(".topic-content-wrapper").css(
			"max-height",
			`${Math.ceil($(".topic").length / 3) * 115.75}px`
		)

		$(".topic").on(
			"click",
			function(event) {
				$(".topic").removeClass("topic-selected");
				$(".topic-content").css("display", "none");

				const element = $(event.currentTarget);

				if (element.hasClass("topic-selected")) {
					element.removeClass("topic-selected");
				} else {
					element.addClass("topic-selected");

					$(".topic-content-wrapper").html(element.children(".block-topic-content").html());

					$(".program-point-share").on(
						"click",
						function(event) {
							$("#share-form").modal();

							const topicParent = event.currentTarget.parentNode.parentNode;

							const shareTextElement = $("#share-text");
							const renderedSharedText = (
								"„"
								// Remove padding tabs and newlines, replace tabs with newlines and only allow one consecutive line break
								+ topicParent.textContent.trim().replace(/\t+/g, "\n").replace(/\n\s*\n/g, "\n")
								+ "“\n\n"
								+ window.location.href
							);

							shareTextElement.html(renderedSharedText);
							shareTextElement.select();

							$("#share-diaspora")[0].onclick = function(event) {
								window.open(
									(
										"https://sharetodiaspora.github.io/?url="
										+ encodeURIComponent(window.location.href)
										+ "&title="
										+ encodeURIComponent(document.title)
									),
									"das",
									"location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550"
								);
							}

							$("#share-twitter").attr(
								"href",
								`https://twitter.com/share?text=${encodeURIComponent(renderedSharedText)}`
							);

							$("#share-facebook").attr(
								"href",
								`https://facebook.com/sharer/sharer.php?u=${encodeURIComponent(window.location.href)}`
							);
						}
					);
				}
			}
		);

		$(".topic")[0].click();

		$("#js-program").css("display", "block");
	}
);
