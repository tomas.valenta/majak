import os
import tempfile

from wagtail.admin.forms import WagtailAdminPageForm
from wagtail.models.media import Collection

from shared.forms import ArticlesPageForm as SharedArticlesPageForm
from shared.forms import JekyllImportForm as SharedJekyllImportForm

from .tasks import (
    import_jekyll_articles,
    import_manual_person,
    import_people_from_group,
)


class JekyllImportForm(SharedJekyllImportForm):
    def handle_import(self):
        lock_file_name = os.path.join(
            tempfile.gettempdir(), f".{self.instance.id}.articles-import-lock"
        )

        if os.path.isfile(lock_file_name):
            return

        open(lock_file_name, "w").close()

        import_jekyll_articles.delay(
            article_parent_page_id=self.instance.id,
            collection_id=self.cleaned_data["collection"].id,
            url=self.cleaned_data["jekyll_repo_url"],
            dry_run=self.cleaned_data["dry_run"],
            use_git=True,
        )


class DistrictPeoplePageForm(WagtailAdminPageForm):
    def save(self, *args, **kwargs):
        # If anything inside the content field has changed,
        # sync Octopus profiles just in case.
        if "content" in self.changed_data:
            collection_id = self.instance.root_page.image_collection_id

            if collection_id is None:
                collection_id = Collection.objects.first().id

            for group in self.instance.get_syncable_octopus_groups():
                import_people_from_group.delay(
                    self.instance.id,
                    collection_id,
                    group["shortcut"],
                    group["title"],
                )

        return super().save(*args, **kwargs)


class DistrictManualOctopusPersonPageForm(WagtailAdminPageForm):
    def save(self, *args, **kwargs):
        # Sync every time this type page is saved, just in case.
        collection_id = self.instance.root_page.image_collection_id

        if collection_id is None:
            collection_id = Collection.objects.first().id

        import_manual_person.delay(
            self.instance.id,
            collection_id,
        )

        return super().save(*args, **kwargs)


class DistrictArticlesPageForm(SharedArticlesPageForm, JekyllImportForm):
    def __init__(self, *args, **kwargs):
        from shared.models import SharedTag

        from .models import DistrictArticleTag

        super().__init__(*args, **kwargs)

        self.fields["shared_tags"].queryset = SharedTag.objects.order_by("name")

        if self.instance.pk:
            valid_tag_ids = list(
                DistrictArticleTag.objects.filter(
                    content_object__in=self.instance.get_children().specific()
                )
                .values_list("tag_id", flat=True)
                .distinct()
                .all()
            )

            valid_shared_tag_ids = (
                self.instance.shared_tags.values_list("id", flat=True).distinct().all()
            )

            self.fields["displayed_tags"].queryset = (
                DistrictArticleTag.objects.filter(id__in=valid_tag_ids)
                .order_by("tag__name")
                .distinct("tag__name")
            )
            self.fields["displayed_shared_tags"].queryset = (
                SharedTag.objects.filter(id__in=valid_shared_tag_ids)
                .order_by("name")
                .distinct("name")
            )
        else:
            self.fields["displayed_tags"].queryset = DistrictArticleTag.objects.filter(
                id=-1
            )
            self.fields["displayed_shared_tags"].queryset = SharedTag.objects.filter(
                id=-1
            )
