import json
from functools import cached_property

from django.conf import settings
from django.contrib import messages
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.http import HttpResponseRedirect
from django.utils.safestring import mark_safe
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from taggit.models import TaggedItemBase
from wagtail.admin.panels import (
    FieldPanel,
    HelpPanel,
    InlinePanel,
    MultiFieldPanel,
    ObjectList,
    PageChooserPanel,
    TabbedInterface,
)
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.fields import RichTextField, StreamField
from wagtail.models import Orderable, Page
from wagtail.models.media import Collection
from wagtailmetadata.models import MetadataPageMixin

from calendar_utils.models import CalendarMixin
from maps_utils.blocks import MapPointBlock
from maps_utils.const import (
    DEFAULT_MAP_STYLE,
    MAP_STYLES,
    SUPPORTED_FEATURE_TYPES,
    TILE_SERVER_CONFIG,
)
from maps_utils.validation import validators as maps_validators
from shared.blocks import (
    DEFAULT_CONTENT_BLOCKS,
    ButtonGroupBlock,
    CalendarBlock,
    CarouselProgramBlock,
    ChartBlock,
    NewsBlock,
    NewsletterSubscriptionBlock,
    PeopleOverviewBlock,
    ProgramGroupBlock,
    SocialLinkBlock,
)
from shared.const import RICH_TEXT_DEFAULT_FEATURES
from shared.forms import SubscribeForm
from shared.models import (
    ExtendedMetadataPageMixin,
    MainArticlePageMixin,
    MainArticlesPageMixin,
    MainContactPageMixin,
    MainHomePageMixin,
    MainPeoplePageMixin,
    MainPersonPageMixin,
    MainProgramPageMixin,
    MainSearchPageMixin,
    MainSimplePageMixin,
    PageInMenuMixin,
    PdfPageMixin,
    SharedTaggedDistrictArticle,
    SubpageMixin,
)
from shared.templatetags.shared_filters import markdown
from shared.utils import (
    make_promote_panels,
    strip_all_html_tags,
    subscribe_to_newsletter,
    trim_to_length,
)

from . import blocks
from .forms import (
    DistrictArticlesPageForm,
    DistrictManualOctopusPersonPageForm,
    DistrictPeoplePageForm,
)

CONTENT_BLOCKS = DEFAULT_CONTENT_BLOCKS + [
    ("chart", ChartBlock()),
    ("related", blocks.ArticlesBlock()),
    ("related_links", blocks.ArticleLinksBlock()),
]


class DistrictHomePage(CalendarMixin, MainHomePageMixin):
    ### FIELDS

    # Main section
    content = StreamField(
        [
            ("fullscreen_header_block", blocks.FullscreenHeaderBlock()),
            (
                "news_block",
                NewsBlock(
                    template="styleguide2/includes/organisms/articles/district/articles_section.html"
                ),
            ),
            ("elections_block", blocks.ElectionsCountdownBlock()),
            (
                "people_block",
                PeopleOverviewBlock(
                    template="styleguide2/includes/organisms/main_section/district/representatives_section.html"
                ),
            ),
            ("calendar_block", CalendarBlock()),
            ("carousel_program", CarouselProgramBlock()),
            ("newsletter_block", blocks.NewsletterBlock()),
        ],
        verbose_name="Obsah",
        blank=True,
        use_json_field=True,
    )

    # Footer
    footer_person_list = StreamField(
        [("person", blocks.PersonContactBlock())],
        verbose_name="Osoby v zápatí webu",
        blank=True,
        max_num=6,
        use_json_field=True,
    )

    # Settings
    newsletter_list_id = models.CharField(
        "ID newsletteru",
        max_length=20,
        blank=True,
        null=True,
        help_text="ID newsletteru z Mailtrainu. Po vyplnění se formulář pro odběr newsletteru zobrazí na úvodní stránce a na stránce s kontakty.",
    )
    newsletter_description = models.CharField(
        "Popis newsletteru",
        max_length=250,
        default="Fake news tam nenajdeš, ale dozvíš se, co chystáme doopravdy!",
    )
    image_collection = models.ForeignKey(
        Collection,
        verbose_name="Default kolekce obrázků",
        help_text=(
            "Do této kolekce se budou importovat např. fotky osob "
            "importovaných z Chobotnice. Pokud je tato hodnota "
            "nevyplněna, použije se běžná 'Root' kolekce."
        ),
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
    )

    calendar_button_text = models.CharField(
        "Text tlačítka kalendáře", max_length=256, default="Kalendář"
    )

    custom_css = models.TextField(
        "Vlastní CSS",
        help_text=(
            "Pokud si rozumíš s CSS a potřebuješ manuálně upravit něco ve vzhledu "
            "webu, můžeš do tohoto pole zadat pravidla, která se aplikují napříč "
            "celým webem. V opačném případě sem prosím nic nezadávej."
        ),
        blank=True,
        null=True,
    )

    # Extra komentar v paticce (TODO)
    footer_extra_content = RichTextField(
        verbose_name="Extra obsah na začátku patičky",
        blank=True,
        features=RICH_TEXT_DEFAULT_FEATURES,
    )

    ### PANELS

    menu_panels = [
        FieldPanel("title_suffix"),
        FieldPanel("meta_title_suffix"),
    ] + MainHomePageMixin.menu_panels

    footer_panels = MainHomePageMixin.footer_panels + [
        FieldPanel("footer_extra_content"),
    ]

    settings_panels = [
        MultiFieldPanel(
            [
                FieldPanel("calendar_button_text"),
                FieldPanel("calendar_url"),
            ],
            "Kalendář",
        ),
        MultiFieldPanel(
            [
                FieldPanel("newsletter_list_id"),
                FieldPanel("newsletter_description"),
            ],
            "Formulář pro odběr newsletteru",
        ),
        FieldPanel("matomo_id"),
        FieldPanel("image_collection"),
        FieldPanel("custom_css"),
        FieldPanel("fallback_image"),
    ]

    ### EDIT HANDLERS

    edit_handler = TabbedInterface(
        [
            ObjectList(MainHomePageMixin.content_panels, heading="Obsah"),
            ObjectList(menu_panels, heading="Hlavička"),
            ObjectList(footer_panels, heading="Patička"),
            ObjectList(settings_panels, heading="Nastavení"),
            ObjectList(MainHomePageMixin.promote_panels, heading="Metadata"),
        ]
    )

    ### RELATIONS

    subpage_types = [
        "district.DistrictArticlesPage",
        "district.DistrictCenterPage",
        "district.DistrictContactPage",
        "district.DistrictCrossroadPage",
        "district.DistrictCustomPage",
        "district.DistrictPeoplePage",
        "district.DistrictGeoFeatureCollectionPage",
        "district.DistrictCalendarPage",
        "district.DistrictPdfPage",
        "district.DistrictNewProgramPage",
        "district.DistrictSearchPage",
    ]

    ### OTHERS

    class Meta:
        verbose_name = "Oblastní sdružení"

    @property
    def gdpr_and_cookies_page(self):
        from main.models import MainHomePage

        return MainHomePage.objects.first().gdpr_and_cookies_page

    @property
    def articles_page_model(self):
        return DistrictArticlesPage

    @property
    def article_page_model(self):
        return DistrictArticlePage

    @property
    def search_page_model(self):
        return DistrictSearchPage

    @property
    def people_page_model(self):
        return DistrictPeoplePage

    @property
    def contact_page_model(self):
        return DistrictContactPage

    @property
    def calendar_page(self):
        return self._first_subpage_of_type(DistrictCalendarPage)

    @property
    def root_page(self):
        return self

    @property
    def has_calendar(self):
        return self.calendar_id is not None


class DistrictArticleTag(TaggedItemBase):
    content_object = ParentalKey(
        "district.DistrictArticlePage",
        on_delete=models.CASCADE,
        related_name="tagged_items",
    )


class DistrictArticlePage(MainArticlePageMixin):
    ### FIELDS

    author_page = models.ForeignKey(
        "district.DistrictPersonPage", on_delete=models.SET_NULL, null=True, blank=True
    )
    tags = ClusterTaggableManager(through=DistrictArticleTag, blank=True)
    shared_tags = ClusterTaggableManager(
        verbose_name="Štítky pro sdílení mezi weby",
        through=SharedTaggedDistrictArticle,
        blank=True,
    )

    ### RELATIONS

    parent_page_types = ["district.DistrictArticlesPage"]
    subpage_types = ["district.DistrictPdfPage"]


class DistrictArticlesPage(MainArticlesPageMixin):
    base_form_class = DistrictArticlesPageForm

    displayed_tags = ParentalManyToManyField(
        "district.DistrictArticleTag",
        verbose_name="Z tohoto oblastního webu",
        related_name="+",
        blank=True,
    )

    displayed_shared_tags = ParentalManyToManyField(
        "shared.SharedTag",
        verbose_name="Sdílecí",
        related_name="+",
        blank=True,
    )

    ### FIELDS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = ["district.DistrictArticlePage"]


class DistrictContactPage(MainContactPageMixin):
    ### FIELDS

    contact_people = StreamField(
        [("item", blocks.PersonContactBlock())],
        verbose_name="Kontaktní osoby",
        blank=True,
        use_json_field=True,
    )

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = []


class PersonPageMixin(Page):
    @property
    def main_image(self) -> None:
        return None

    @property
    def profile_image(self):
        return self.person.photo

    def get_profile_image(self):
        if self.profile_image:
            return self.profile_image

        return self.root_page.fallback_image

    @property
    def before_name(self):
        return self.person.degree_before

    @property
    def after_name(self):
        return self.person.degree_after

    def get_full_name(self) -> str:
        full_name = ""

        if self.person.degree_before:
            full_name += f"{self.person.degree_before} "

        full_name += self.title

        if self.person.degree_after:
            full_name += f", {self.person.degree_after}"

        return full_name

    @property
    def position(self):
        print(self.person.position)

        return self.person.position

    @property
    def perex(self):
        return self.person.short_text

    @property
    def text(self):
        return markdown(self.person.long_text)

    @property
    def social_links(self):
        link_blocks = []

        for attr_name, attr_info in {
            "facebook_url": {"name": "Facebook", "icon": "ico--facebook"},
            "flickr_url": {
                "name": "Flickr",
                "icon": "ico--flickr",
            },
            "instagram_url": {"name": "Instagram", "icon": "ico--instagram"},
            "mastodon_url": {"name": "Mastodon", "icon": "ico--mastodon"},
            "twitter_url": {"name": "X", "icon": "ico--twitter"},
            "tiktok_url": {"name": "TikTok", "icon": "ico--globe"},
            "web_url": {"name": "Webové stránky", "icon": "ico--globe"},
            "youtube_url": {"name": "YouTube", "icon": "ico--youtube"},
        }.items():
            link = getattr(self.person, attr_name)

            if link is None:
                continue

            link_blocks.append(
                SocialLinkBlock().to_python(
                    {"icon": attr_info["icon"], "text": attr_info["name"], "link": link}
                )
            )

        return link_blocks

    @property
    def related_people(self):
        return []

    @property
    def email(self):
        return self.person.email

    @property
    def phone(self):
        return self.person.phone

    @property
    def job(self) -> None:
        return None

    @property
    def city(self) -> None:
        return None

    @property
    def age(self) -> None:
        return None

    @property
    def primary_group(self) -> None:
        return None

    @property
    def is_pirate(self):
        return True

    @property
    def other_party(self) -> None:
        return None

    @property
    def other_party_logo(self) -> None:
        return None

    class Meta:
        abstract = True


class DistrictManualOctopusPersonPage(
    ExtendedMetadataPageMixin, SubpageMixin, MetadataPageMixin, PersonPageMixin, Page
):
    base_form_class = DistrictManualOctopusPersonPageForm

    ### FIELDS

    username = models.CharField(
        verbose_name="Username",
        help_text='Uživatelské jméno uživatele v Chobotnici, např. "igor.hnizdo".',
        max_length=128,
    )

    person = models.ForeignKey(
        "shared.OctopusPerson",
        # Wagtail doesn't support CASCADE, so we'll just deal with that somewhere else.
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="+",
        verbose_name="Osoba",
    )

    ### PANELS

    content_panels = Page.content_panels + [FieldPanel("username")]

    ### RELATIONS

    parent_page_types = ["district.DistrictPeoplePage"]
    subpage_types = []

    ### OTHERS

    class Meta:
        verbose_name = "Osoba z Chobotnice"


class DistrictOctopusPersonPage(
    ExtendedMetadataPageMixin, SubpageMixin, MetadataPageMixin, PersonPageMixin, Page
):
    ### FIELDS

    person = models.ForeignKey(
        "shared.OctopusPerson",
        # Wagtail doesn't support CASCADE, so we'll just deal with that somewhere else.
        on_delete=models.SET_NULL,
        blank=False,
        null=True,
        related_name="+",
        verbose_name="Osoba",
    )

    is_automatically_created = models.BooleanField(
        verbose_name="Profil vytvořen automaticky",
        default=False,
    )

    originating_group = models.CharField(
        verbose_name="Skupina",
        help_text="Skupina, ze které byla tato osba importována.",
        max_length=128,
        blank=True,
        null=True,
    )

    originating_team = models.CharField(
        verbose_name="Tým",
        help_text="Tým, ze kterého byla tato osba importována.",
        max_length=128,
        blank=True,
        null=True,
    )

    originating_role = models.CharField(
        verbose_name="Role",
        help_text="Požadovaná role v týmu, ze kterého byla tato osba importována.",
        max_length=128,
        blank=True,
        null=True,
    )

    originating_display = models.CharField(
        verbose_name="Název týmu/skupiny",
        help_text="Název týmu nebo skupiny, ze kterých byla tato osba importována.",
        max_length=128,
        blank=True,
        null=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("person"),
        FieldPanel("is_automatically_created", read_only=True),
        FieldPanel("originating_group", read_only=True),
        FieldPanel("originating_team", read_only=True),
    ]

    ### RELATIONS

    parent_page_types = ["district.DistrictPeoplePage"]
    subpage_types = []

    ### OTHERS

    @property
    def primary_group(self) -> None:
        return self.originating_display

    class Meta:
        verbose_name = "Osoba z Chobotnice"


class DistrictPersonPage(MainPersonPageMixin):
    ### FIELDS

    job = models.CharField(
        "Povolání",
        max_length=128,
        blank=True,
        null=True,
        help_text="Např. 'Informatik'",
    )

    city = models.CharField("Město/obec", max_length=64, blank=True, null=True)
    age = models.IntegerField("Věk", blank=True, null=True)

    is_pirate = models.BooleanField("Je členem Pirátské strany?", default=True)
    other_party = models.CharField(
        "Strana",
        max_length=64,
        blank=True,
        null=True,
        help_text="Vyplňte pokud osoba není Pirát",
    )
    other_party_logo = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="+",
        verbose_name="Logo strany",
        help_text="Vyplňte pokud osoba není Pirát",
    )

    ### PANELS

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("main_image"),
                FieldPanel("profile_image"),
            ],
            heading="Obrázky",
        ),
        MultiFieldPanel(
            [FieldPanel("before_name"), FieldPanel("after_name")],
            heading="Titul",
        ),
        MultiFieldPanel(
            [
                FieldPanel("is_pirate"),
                FieldPanel("other_party"),
                FieldPanel("other_party_logo"),
            ],
            heading="Politická příslušnost",
        ),
        MultiFieldPanel(
            [
                FieldPanel("position"),
                FieldPanel("job"),
                FieldPanel("city"),
                FieldPanel("age"),
            ],
            heading="Základní informace",
        ),
        MultiFieldPanel([FieldPanel("perex"), FieldPanel("text")], heading="Popis"),
        MultiFieldPanel(
            [
                FieldPanel("email"),
                FieldPanel("phone"),
                FieldPanel("social_links"),
            ],
            heading="Kontakt",
        ),
        FieldPanel("calendar_url"),
        FieldPanel("related_people"),
    ]

    ### RELATIONS

    parent_page_types = ["district.DistrictPeoplePage"]
    subpage_types = []


class DistrictPeoplePage(MainPeoplePageMixin):
    base_form_class = DistrictPeoplePageForm

    ### FIELDS

    content = StreamField(
        [
            ("octopus_group", blocks.OctopusGroupBlock(label="Skupina z Chobotnice")),
            ("octopus_team", blocks.OctopusTeamBlock(label="Tým z Chobotnice")),
            ("people_group", blocks.PeopleGroupBlock(label="Seznam osob", group="")),
            ("team_group", blocks.TeamBlock()),
        ],
        verbose_name="Lidé a týmy",
        blank=True,
        use_json_field=True,
    )

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = [
        "district.DistrictPersonPage",
        "district.DistrictOctopusPersonPage",
        "district.DistrictManualOctopusPersonPage",
    ]

    ### PANELS

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("perex_col_1", heading="První sloupec"),
                FieldPanel("perex_col_2", heading="Druhý sloupec"),
            ],
            "Perex",
        ),
        MultiFieldPanel(
            [
                HelpPanel(
                    content=mark_safe(
                        "Pokud chceš importovat osoby z Chobotnice, můžeš přidat do sekce "
                        "<em>Lidé a týmy</em> blok <em>Skupina z Chobotnice</em> nebo <em>"
                        "Tým z Chobotnice</em>. Jako zkratku skupiny můžeš vyplnit "
                        "například <em>cen_ao_ved</em> pro skupinu "
                        "<a href='https://pi2.cz/chobo-ao'>Administrativní odbor - vedení</a>. "
                        "Jako zkratku týmu můžeš vyplnit např. <em>TO-admin</em> pro tým "
                        "<a href='https://pi2.cz/chobo-to'>Administrátoři TO</a>. Při sync. "
                        "týmů se do pozice osoby propíše role v daném týmu.<br><br>"
                        "Dále je také možné manuálně importovat jednotlivé osoby přidáním "
                        "podstránky typu <em>Osoba z Chobotnice</em>."
                    ),
                ),
                FieldPanel("content"),
            ],
            "Obsah",
        ),
    ]

    edit_handler = TabbedInterface(
        [
            ObjectList(content_panels, heading="Obsah"),
            ObjectList(MainPeoplePageMixin.promote_panels, heading="Metadata"),
        ]
    )

    ### OTHERS

    @classmethod
    def allowed_subpage_models(cls):
        # Get all page types and remove the unwanted child page type
        allowed_pages = super().allowed_subpage_models()
        return [
            page
            for page in allowed_pages
            if page.__name__ != "DistrictOctopusPersonPage"
        ]

    def get_syncable_octopus_groups(self):
        groups = []

        for block in self.content:
            if block.block_type == "octopus_group":
                groups.append(
                    {
                        "shortcut": block.value["group_shortcut"],
                        "title": block.value["title"],
                    }
                )

        # Remove duplicates by converting to a set of tuples and back to a list of dicts
        unique_groups = [dict(g) for g in {tuple(group.items()) for group in groups}]

        return unique_groups

    def get_syncable_octopus_teams(self):
        teams = []

        for block in self.content:
            if block.block_type == "octopus_team":
                teams.append(
                    {
                        "shortcut": block.value["team_shortcut"],
                        "roles": block.value["roles"],
                        "title": block.value["title"],
                    }
                )

        # Remove duplicates by converting to a set of tuples and back to a list of dicts
        unique_teams = [dict(t) for t in {tuple(team.items()) for team in teams}]

        # Since lists can't be hashed and our unique-ification function won't work
        # with them around, convert roles to lists only after we know the teams
        # are unique.
        for position, team in enumerate(unique_teams):
            if team["roles"] is None:
                continue
            
            unique_teams[position]["roles"] = team["roles"].split(",")

        return unique_teams


class DistrictCalendarPage(SubpageMixin, MetadataPageMixin, CalendarMixin, Page):
    ### PANELS

    content_panels = Page.content_panels + [FieldPanel("calendar_url")]

    ### RELATIONS

    parent_page_types = [
        "district.DistrictCenterPage",
        "district.DistrictHomePage",
    ]
    subpage_types = []

    ### OTHERS

    class Meta:
        verbose_name = "Stránka s kalendářem"


class DistrictPdfPage(PdfPageMixin, MetadataPageMixin, SubpageMixin, Page):
    ### RELATIONS

    parent_page_types = [
        "district.DistrictHomePage",
        "district.DistrictArticlePage",
    ]
    subpage_types = []

    ### PANELS

    content_panels = Page.content_panels + PdfPageMixin.content_panels

    ### OTHER

    class Meta:
        verbose_name = "PDF stránka"


class DistrictCenterPage(
    CalendarMixin,
    ExtendedMetadataPageMixin,
    SubpageMixin,
    MetadataPageMixin,
    PageInMenuMixin,
    Page,
):
    ### FIELDS

    calendar_page = models.ForeignKey(
        "DistrictCalendarPage",
        verbose_name="Stránka s kalendářem",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    perex = models.TextField("Perex", blank=True, null=True)
    background_photo = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="+",
    )
    content = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("contact", blocks.CenterContactBlock()),
            ("badge", blocks.PersonBadgeBlock()),
        ],
        verbose_name="Obsah",
        blank=True,
        use_json_field=True,
    )

    map_address_content = StreamField(
        [
            ("address", blocks.AddressBlock()),
        ],
        verbose_name="Adresa u mapy",
        blank=True,
        use_json_field=True,
    )

    map_area_content = StreamField(
        [
            ("map", MapPointBlock()),
        ],
        verbose_name="Mapa",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("perex"),
                PageChooserPanel("background_photo"),
            ],
            "Hlavička",
        ),
        MultiFieldPanel(
            [
                FieldPanel("map_address_content"),
                PageChooserPanel("map_area_content"),
            ],
            "Obsah vyskakovacího bloku s mapou",
        ),
        FieldPanel("content"),
        MultiFieldPanel(
            [
                FieldPanel("calendar_url"),
                PageChooserPanel("calendar_page"),
            ],
            "Kalendář",
        ),
    ]

    promote_panels = make_promote_panels()

    settings_panels = []

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = ["district.DistrictCalendarPage"]

    ### OTHERS

    class Meta:
        verbose_name = "Stránka pirátského centra"

    @property
    def has_calendar(self):
        return self.calendar_id is not None

    def get_meta_image(self):
        return (
            self.search_image
            or self.background_photo
            or self.root_page.get_meta_image()
        )

    def get_meta_description(self):
        if self.search_description:
            return self.search_description

        if hasattr(self, "perex") and self.perex:
            self.perex
        elif hasattr(self, "text") and self.text:
            trim_to_length(strip_all_html_tags(self.text))

        return ""


class DistrictNewProgramPage(MainProgramPageMixin):
    ### FIELDS

    program = StreamField(
        [
            ("program_group", ProgramGroupBlock()),
            ("program_group_crossroad", blocks.ProgramGroupBlockCrossroad()),
            ("program_group_popout", blocks.ProgramGroupBlockPopout()),
            ("program_group_with_candidates", blocks.ProgramGroupWithCandidatesBlock()),
        ],
        verbose_name="Programy",
        blank=True,
        use_json_field=True,
    )

    header_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Obrázek na pozadí hlavičky",
        related_name="+",
    )

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = ["district.DistrictCustomPage"]

    ### PANELS

    settings_panels = [FieldPanel("header_image")]

    edit_handler = TabbedInterface(
        [
            ObjectList(MainProgramPageMixin.content_panels, heading="Obsah"),
            ObjectList(settings_panels, heading="Nastavení"),
            ObjectList(MainProgramPageMixin.promote_panels, heading="Metadata"),
        ]
    )


class DistrictCrossroadPage(
    ExtendedMetadataPageMixin, SubpageMixin, MetadataPageMixin, PageInMenuMixin, Page
):
    ### FIELDS

    cards_content = StreamField(
        [("cards", blocks.CardLinkWithHeadlineBlock())],
        verbose_name="Karty rozcestníku",
        blank=True,
        use_json_field=True,
    )

    content = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
        ],
        verbose_name="Obsah stránky",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("cards_content"),
        FieldPanel("content"),
    ]

    promote_panels = make_promote_panels()

    settings_panels = []

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]
    subpage_types = [
        "district.DistrictArticlePage",
        "district.DistrictArticlesPage",
        "district.DistrictCenterPage",
        "district.DistrictContactPage",
        "district.DistrictCrossroadPage",
        "district.DistrictCustomPage",
        "district.DistrictGeoFeatureCollectionPage",
        "district.DistrictPeoplePage",
        "district.DistrictPersonPage",
    ]
    ### OTHERS

    class Meta:
        verbose_name = "Rozcestník s kartami"


class DistrictSearchPage(MainSearchPageMixin):
    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage"]

    ### OTHERS

    @property
    def searchable_models(self) -> list:
        return [
            DistrictArticlePage,
            DistrictCustomPage,
            DistrictCrossroadPage,
            DistrictNewProgramPage,
            DistrictCenterPage,
            DistrictContactPage,
        ]


class DistrictCustomPage(RoutablePageMixin, MainSimplePageMixin):
    ### FIELDS

    main_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Obrázek na pozadí hlavičky",
        related_name="+",
    )

    content = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
            ("newsletter", NewsletterSubscriptionBlock()),
        ],
        verbose_name="Obsah",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("main_image"),
        FieldPanel("content"),
    ]

    ### RELATIONS

    parent_page_types = ["district.DistrictHomePage", "district.DistrictCrossroadPage"]
    subpage_types = []

    ### OTHERS

    @cached_property
    def newsletter_subscribe_url(self):
        newsletter_subscribe = self.reverse_subpage("newsletter_subscribe")

        return (
            self.url + newsletter_subscribe
            if self.url is not None
            else newsletter_subscribe
        )  # preview fix

    @route(r"^prihlaseni-k-newsletteru/$")
    def newsletter_subscribe(self, request):
        if request.method == "POST":
            form = SubscribeForm(request.POST)

            if form.is_valid():
                subscribe_to_newsletter(
                    form.cleaned_data["email"],
                    (
                        self.newsletter_list_id
                        if hasattr(self, "newsletter_list_id")
                        and self.newsletter_list_id
                        else settings.PIRATICZ_NEWSLETTER_CID
                    ),
                )

                messages.success(
                    request,
                    "Zkontroluj si prosím schránku, poslali jsme ti potvrzovací email.",
                )

                try:
                    page = (
                        Page.objects.filter(id=form.cleaned_data["return_page_id"])
                        .live()
                        .first()
                    )
                    return HttpResponseRedirect(page.full_url)
                except Page.DoesNotExist:
                    return HttpResponseRedirect(self.url)

            messages.error(
                request,
                "Tvůj prohlížeč nám odeslal špatná data. Prosím, zkus to znovu.",
            )

        return HttpResponseRedirect(self.url)

    @property
    def newsletter_list_id(self) -> str:
        for block in self.content:
            if block.block_type == "newsletter":
                return block.value["list_id"]

        return self.root_page.newsletter_list_id


class DistrictGeoFeatureCollectionPage(
    ExtendedMetadataPageMixin, SubpageMixin, MetadataPageMixin, Page
):
    ### FIELDS
    perex = models.TextField("Perex", null=True)
    hero_cta_buttons = StreamField(
        [
            ("button_group", ButtonGroupBlock()),
        ],
        verbose_name="CTAs pro banner",
        blank=True,
        null=True,
        help_text="Použije se v hlavním banneru.",
        use_json_field=True,
    )
    content = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
        ],
        verbose_name="Obsah úvodní",
        blank=True,
        use_json_field=True,
    )
    content_after = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
        ],
        verbose_name="Obsah za mapou",
        blank=True,
        use_json_field=True,
    )
    content_footer = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
        ],
        verbose_name="Obsah v patičkové části",
        blank=True,
        use_json_field=True,
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Obrázek na pozadí",
        related_name="+",
    )
    logo_image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Logo",
        related_name="+",
    )
    style = models.CharField(
        "Styl mapy", choices=MAP_STYLES, max_length=50, default=DEFAULT_MAP_STYLE
    )
    map_title = models.TextField("Titulek mapy", blank=True, null=True)
    category_list_title = models.TextField(
        "Titulek přehledu dle kategorie", blank=True, null=True
    )
    promoted_block_title = models.TextField(
        "Titulek bloku propagovaných položek", blank=True, null=True
    )

    ### PANELS

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel("perex"),
                FieldPanel("hero_cta_buttons"),
                FieldPanel("content"),
                FieldPanel("content_after"),
                FieldPanel("content_footer"),
                FieldPanel("promoted_block_title"),
                FieldPanel("logo_image"),
                FieldPanel("image"),
            ],
            "Obsah hlavní stránky kolekce",
        ),
        MultiFieldPanel(
            [
                InlinePanel("categories"),
                FieldPanel("category_list_title"),
            ],
            "Kategorie",
        ),
        MultiFieldPanel(
            [
                FieldPanel("map_title"),
                FieldPanel("style"),
            ],
            "Nastavení mapy",
        ),
    ]

    settings_panels = []

    ### RELATIONS

    parent_page_types = [
        "district.DistrictHomePage",
    ]
    subpage_types = ["district.DistrictGeoFeatureDetailPage"]

    class Meta:
        verbose_name = "Stránka s mapovou kolekcí"

    def get_features_by_category(self):
        features = (
            self.get_children()
            .live()
            .specific()
            .prefetch_related("category")
            .order_by("districtgeofeaturedetailpage__sort_order")
        )
        categories = sorted(set(f.category for f in features), key=lambda c: c.name)

        return [
            (category, [f for f in features if f.category == category])
            for category in categories
        ]

    def get_promoted_features(self):
        return (
            self.get_children()
            .live()
            .specific()
            .filter(districtgeofeaturedetailpage__promoted=True)
            .order_by("districtgeofeaturedetailpage__sort_order")
        )

    def get_context(self, request):
        context = super().get_context(request)
        features_by_category = self.get_features_by_category()

        context["features_by_category"] = features_by_category
        context["promoted_features"] = self.get_promoted_features()
        context["js_map"] = {
            "tile_server_config": json.dumps(TILE_SERVER_CONFIG),
            "style": self.style,
            "categories": json.dumps(
                [
                    # Gather all categories used in collection
                    {"name": c.name, "color": c.hex_color}
                    for c, f in features_by_category
                ]
            ),
            "geojson": json.dumps(
                [
                    f.as_geojson_object(request)
                    for c, features in features_by_category
                    for f in features
                ]
            ),
        }

        return context

    def get_meta_image(self):
        return (
            self.search_image
            or self.logo_image
            or self.image
            or self.root_page.get_meta_image()
        )

    def get_meta_description(self):
        if self.search_description:
            return self.search_description
        return self.perex


class DistrictGeoFeatureCollectionCategory(Orderable):
    name = models.CharField("Název", max_length=100)
    hex_color = models.CharField(
        "Barva (HEX)",
        max_length=6,
        help_text="Zadejte barvu pomocí HEX notace (bez # na začátku).",
    )
    page = ParentalKey(
        DistrictGeoFeatureCollectionPage,
        on_delete=models.CASCADE,
        related_name="categories",
    )

    ### PANELS

    panels = [
        FieldPanel("name"),
        FieldPanel("hex_color"),
    ]

    class Meta:
        verbose_name = "Kategorie mapové kolekce"

    def __str__(self) -> str:
        return f"{self.page} / {self.name}"

    @property
    def rgb(self):
        return tuple(int(self.hex_color[i : i + 2], 16) for i in (0, 2, 4))


def make_feature_index_cache_key(feature: "DistrictGeoFeatureDetailPage"):
    return f"DistrictGeoFeatureDetailPage::{feature.id}::index"


class DistrictGeoFeatureDetailPage(
    ExtendedMetadataPageMixin, MetadataPageMixin, SubpageMixin, Page, Orderable
):
    perex = models.TextField("Perex", null=False)
    geojson = models.TextField(
        "Geodata",
        help_text="Vložte surový GeoJSON objekt typu 'FeatureCollection'. Vyrobit jej můžete např. pomocí online služby geojson.io. Pokud u Feature objektů v kolekci poskytnete properties 'title' a 'description', zobrazí se jak na mapě, tak i v detailu.",
        null=False,
    )
    category = models.ForeignKey(
        "district.DistrictGeoFeatureCollectionCategory",
        verbose_name="Kategorie",
        blank=False,
        null=False,
        on_delete=models.PROTECT,
    )
    guarantor = models.ForeignKey(
        "district.DistrictPersonPage",
        verbose_name="Garant",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="obrázek",
    )
    content = StreamField(
        CONTENT_BLOCKS
        + [
            ("badge", blocks.PersonBadgeBlock()),
            ("new_people_group", blocks.PeopleGroupBlock()),
        ],
        verbose_name="Obsah",
        blank=True,
        use_json_field=True,
    )
    parts_section_title = models.CharField(
        "Titulek přehledu součástí",
        help_text="Pokud ponecháte prázdné, nebude se titulek zobrazovat. Sekce se vůbec nezobrazí pokud GeoJSON FeatureCollection obsahuje jen jeden Feature.",
        max_length=100,
        null=True,
        blank=True,
    )
    initial_zoom = models.IntegerField(
        "Výchozí zoom",
        default=15,
        null=False,
        blank=False,
    )
    promoted = models.BooleanField(
        "Propagovat",
        default=False,
    )
    sort_order = models.IntegerField(
        "Index řazení",
        null=True,
        blank=True,
        help_text="Čím větší hodnotu zadáte, tím později ve výpise položek bude. Můžete tím tedy ovlivňovat očíslování "
        "položky ve výpisu.",
        default=0,
    )
    sort_order_field = "sort_order"

    ### PANELS

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel("title"),
                FieldPanel("perex"),
                FieldPanel("content"),
                FieldPanel("parts_section_title"),
                FieldPanel("image"),
                FieldPanel("category"),
                FieldPanel("promoted"),
            ],
            "Základní informace",
        ),
        MultiFieldPanel(
            [
                FieldPanel("geojson"),
                FieldPanel("initial_zoom"),
            ],
            "Mapka",
        ),
        PageChooserPanel("guarantor"),
        FieldPanel("sort_order"),
    ]

    settings_panels = []

    ### RELATIONS

    parent_page_types = ["district.DistrictGeoFeatureCollectionPage"]
    subpage_types = []

    class Meta:
        verbose_name = "Položka mapové kolekce"
        ordering = ["sort_order"]

    def save(self, *args, **kwargs):
        # delete all sibling index cache keys to force recompute
        keys = [
            make_feature_index_cache_key(feature)
            for feature in self.get_siblings(inclusive=True).live()
        ]
        cache.delete_many(keys)
        return super().save(*args, **kwargs)

    @property
    def index(self):
        key = make_feature_index_cache_key(self)
        cached_index = cache.get(key)

        if cached_index is None:
            cached_index = (
                list(
                    self.get_siblings(inclusive=True)
                    .live()
                    .order_by("districtgeofeaturedetailpage__sort_order")
                    .values_list("pk", flat=True)
                ).index(self.pk)
                + 1
            )
            cache.set(key, cached_index)

        return cached_index

    def get_meta_image(self):
        return self.search_image or self.image

    def get_meta_description(self):
        if self.search_description:
            return self.search_description
        return self.perex

    def as_geojson_object(self, request):
        collection = json.loads(self.geojson)
        image = (
            self.image.get_rendition("fill-1200x675-c75|jpegquality-80").url
            if self.image
            else None
        )
        url = self.get_url(request) if self.content else None

        for idx, feature in enumerate(collection["features"]):
            feature["properties"].update(
                {
                    "id": self.pk,
                    "index": self.index,
                    "slug": f"{idx}-{self.pk}-{self.slug}",
                    "collectionTitle": self.title,
                    "collectionDescription": self.perex,
                    "image": image,
                    "link": url,
                    "category": self.category.name,
                }
            )

            if not "description" in feature["properties"]:
                feature["properties"]["description"] = None

        # This extends GeoJSON spec to pass down more context to the map.
        collection["properties"] = {}
        collection["properties"]["slug"] = f"{self.pk}-{self.slug}"
        collection["properties"]["collectionTitle"] = self.title
        collection["properties"]["collectionDescription"] = self.perex
        collection["properties"]["category"] = self.category.name
        collection["properties"]["index"] = self.index
        collection["properties"]["image"] = image
        collection["properties"]["link"] = url

        return collection

    def get_context(self, request):
        context = super().get_context(request)
        context[
            "features_by_category"
        ] = self.get_parent().specific.get_features_by_category()
        feature_collection = self.as_geojson_object(request)

        context["js_map"] = {
            "tile_server_config": json.dumps(TILE_SERVER_CONFIG),
            "style": self.get_parent().specific.style,
            "categories": json.dumps(
                [{"name": self.category.name, "color": self.category.hex_color}]
            ),
            "geojson": json.dumps([feature_collection]),
        }
        context["parts"] = [f["properties"] for f in feature_collection["features"]]
        return context

    def clean(self):
        try:
            self.geojson = maps_validators.normalize_geojson_feature_collection(
                self.geojson, allowed_types=SUPPORTED_FEATURE_TYPES
            )

            print(self.geojson)
        except ValueError as exc:
            print(exc)

            raise ValidationError({"geojson": str(exc)}) from exc


# Legacy models required for migrations


class LegacyProgramPageMixin(Page):
    def serve(self, request, *args, **kwargs):
        return HttpResponseRedirect("/programy/")

    class Meta:
        abstract = True
