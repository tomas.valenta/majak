# Generated by Django 3.0.6 on 2020-05-25 20:18

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("senat_campaign", "0004_auto_20200525_1951"),
    ]

    operations = [
        migrations.DeleteModel(
            name="SenatCampaignWebSettings",
        ),
    ]
