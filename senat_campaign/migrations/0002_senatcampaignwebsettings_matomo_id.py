# Generated by Django 3.0.6 on 2020-05-14 18:29

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("senat_campaign", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="senatcampaignwebsettings",
            name="matomo_id",
            field=models.IntegerField(
                blank=True,
                null=True,
                verbose_name="Matomo ID pro sledování návštěvnosti",
            ),
        ),
    ]
