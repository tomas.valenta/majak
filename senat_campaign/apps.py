from django.apps import AppConfig


class SenatCampaignConfig(AppConfig):
    name = "senat_campaign"
