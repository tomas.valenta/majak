(function ($) {

    // Initialization
    "use strict";
    var nav = $('nav');
    var navHeight = nav.outerHeight();

    // Fancybox
    $('[data-fancybox="gallery"]').fancybox({
      buttons: [
        // "zoom",
        //"share",
        //"slideShow",
        //"fullScreen",
        //"download",
        // "thumbs",
        "close"
      ],
    });


    // Scroll function
    $('a.js-scroll-anchor[href*="#"]:not([href="#"])').on("click", function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: (target.offset().top - navHeight + 5)
          }, 1000);
          return false;
        }
      }
    });


    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-anchor').on("click", function () {
      $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
      target: '#mainNavigation',
      offset: navHeight + 200
    });
    /*--/ End Scrolling nav /--*/

    $(window).trigger('scroll');

  })(jQuery);
