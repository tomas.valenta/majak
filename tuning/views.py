import math

from django.views.generic.base import TemplateView
from wagtail.models import Site


class SitesListView(TemplateView):
    template_name = "tuning/sites_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sites = list(
            Site.objects.exclude(hostname__contains=".pir-test.eu")
            .exclude(hostname="default")
            .select_related("root_page")
            .order_by("root_page__title")
        )
        third = math.ceil(len(sites) / 3)
        context["sites_first"] = sites[:third]
        context["sites_second"] = sites[third : third * 2]
        context["sites_third"] = sites[third * 2 :]
        return context
