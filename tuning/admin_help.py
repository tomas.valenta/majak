INFO = "Informace zde uvedené slouží zejména pro sdílení stránky (na sociálních sítích...) a vyhledávače."
TOOLS = (
    "Jak bude sdílení vypadat můžete zkontrolovat na "
    '<a href="https://developers.facebook.com/tools/debug/">https://developers.facebook.com/tools/debug/</a> '
    'a <a href="https://cards-dev.twitter.com/validator/" target="_blank">https://cards-dev.twitter.com/validator/</a>'
)

IMPORTANT_TITLE = (
    "<strong>Název</strong> stránky (tab obsah) slouží k rozlišení stránek v "
    "Majáku. V prohlížeči se zobrazí <strong>Titulek stránky</strong>."
)

NO_SEO_TITLE = (
    "Pokud není zadán <strong>Titulek stránky</strong>, použije se "
    "<strong>Název</strong> (tab obsah)."
)

NO_SEARCH_IMAGE = (
    "Pokud není zadán <strong>Search image</strong>, použije se stejný jako na "
    "úvodní stránce."
)

NO_DESCRIPTION_USE_PEREX = (
    "Pokud není zadán <strong>Popis vyhledávání</strong>, použije se prvních 150 "
    "znaků <strong>Perexu</strong> (tab obsah)."
)


def build(*items):
    list_items = "<br>".join(f"&bull; {item}" for item in items)
    return f"""<div class="help-block help-info">{INFO}<br><br>{list_items}<br><br>{TOOLS}</div>"""
