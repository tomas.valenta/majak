from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from faker import Faker


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("-f", action="store_true")

    def handle(self, *args, **options):
        if settings.MAJAK_ENV != "dev":
            raise CommandError("Possible to run only in dev environment.")

        if not options["f"]:
            self.stdout.write("Use param -f to actually anonymize users.")
            return

        fake = Faker()
        User = get_user_model()

        for u in User.objects.all():
            old = f"{u} | {u.email}"
            u.first_name = fake.first_name()
            u.last_name = fake.last_name()
            u.email = fake.email()
            u.save()
            new = f"{u} | {u.email}"
            self.stdout.write(f"{old} -> {new}")
