from typing import Sequence

import pytest

from maps_utils.validation.validators import (
    normalize_geojson_feature,
    normalize_geojson_feature_collection,
)


@pytest.mark.parametrize(
    "value, allowed_types, valid",
    (
        ("{}", None, False),
        (
            '{"type": "Feature", "geometry": {"type": "MultiPoint", "coordinates": []}}',
            None,
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Point"}, "properties": {}}',
            None,
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Point", "coordinates": []}, "properties": {}}',
            None,
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Point", "coordinates": [15.7420242, 50.0432227]}, "properties": {}}',
            None,
            True,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Polygon", "coordinates": [[1]]}, "properties": {}}',
            None,
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Polygon", "coordinates": [[[15.7625981, 50.0344817, 0], [15.7626987, 50.0343396, 0], [15.762751, 50.0343559, 0], [15.7626531, 50.0344963, 0], [15.7625981, 50.0344817, 0]]]}, "properties": {}}',
            None,
            True,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Point", "coordinates": [15.7420242, 50.0432227]}, "properties": {}}',
            ("Polygon",),
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Point", "coordinates": [15.7420242, 50.0432227]}, "properties": {}}',
            (
                "Polygon",
                "Point",
            ),
            True,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Polygon", "coordinates": [[[15.7625981, 50.0344817, 0], [15.7626987, 50.0343396, 0], [15.762751, 50.0343559, 0], [15.7626531, 50.0344963, 0], [15.7625981, 50.0344817, 0]]]}, "properties": {}}',
            ("Point",),
            False,
        ),
        (
            '{"type": "Feature", "geometry": {"type": "Polygon", "coordinates": [[[15.7625981, 50.0344817, 0], [15.7626987, 50.0343396, 0], [15.762751, 50.0343559, 0], [15.7626531, 50.0344963, 0], [15.7625981, 50.0344817, 0]]]}, "properties": {}}',
            (
                "Point",
                "Polygon",
            ),
            True,
        ),
    ),
)
def test_normalize_feature(value: str, allowed_types: Sequence[str], valid: bool):
    if valid:
        assert normalize_geojson_feature(value)

    else:
        with pytest.raises(ValueError):
            normalize_geojson_feature(value, allowed_types=allowed_types)


@pytest.mark.parametrize(
    "value, allowed_types, valid",
    (
        ("{}", None, False),
        (
            '{"type": "FeatureCollection", "features": [{"type": "Feature", "geometry": {"type": "Point", "coordinates": [15.7420242, 50.0432227]}, "properties": {}}]}',
            (
                "Polygon",
                "Point",
            ),
            True,
        ),
    ),
)
def test_normalize_feature_collection(
    value: str, allowed_types: Sequence[str], valid: bool
):
    if valid:
        assert normalize_geojson_feature_collection(value)
    else:
        with pytest.raises(ValueError):
            normalize_geojson_feature_collection(value, allowed_types=allowed_types)
