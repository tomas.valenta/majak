import json
from datetime import datetime
from pathlib import Path

import pytest

from calendar_utils.icalevents.icalparser import Event


@pytest.fixture(scope="session")
def sample_response():
    event_dict_list = json.loads(
        (Path(__file__).parent / "samples" / "response.json").read_text()
    )
    event_list = []

    for event_dict in event_dict_list:
        event = Event()
        event.start = datetime.fromisoformat(event_dict["start"])
        event.end = datetime.fromisoformat(event_dict["end"])
        event.summary = event_dict["summary"]
        event.all_day = event_dict["all_day"]
        event.description = event_dict["description"]
        event.location = event_dict["location"]
        event.url = event_dict["url"]
        event_list.append(event)

    return event_list


@pytest.fixture(scope="session")
def sample_events():
    event_dict_list = json.loads(
        (Path(__file__).parent / "samples" / "response.json").read_text()
    )
    for event_dict in event_dict_list:
        event_dict["start"] = datetime.fromisoformat(event_dict["start"])
        event_dict["end"] = datetime.fromisoformat(event_dict["end"])
    return event_dict_list


@pytest.fixture(scope="session")
def sample_past_events():
    event_dict_list = json.loads(
        (Path(__file__).parent / "samples" / "past_events.json").read_text()
    )
    for event_dict in event_dict_list:
        event_dict["start"] = datetime.fromisoformat(event_dict["start"])
        event_dict["end"] = datetime.fromisoformat(event_dict["end"])
    return event_dict_list


@pytest.fixture(scope="session")
def sample_future_events():
    event_dict_list = json.loads(
        (Path(__file__).parent / "samples" / "future_events.json").read_text()
    )
    for event_dict in event_dict_list:
        event_dict["start"] = datetime.fromisoformat(event_dict["start"])
        event_dict["end"] = datetime.fromisoformat(event_dict["end"])
    return event_dict_list
