from datetime import datetime

import pytest

from calendar_utils.icalevents.icalparser import Event
from calendar_utils.parser import (
    process_event_list,
    set_event_duration,
    split_event_dict_list,
)


@pytest.mark.freeze_time("2022-05-13")
def test_split_events(sample_response, sample_future_events, sample_past_events):
    past_events, future_events = process_event_list(sample_response)
    assert sample_past_events == past_events
    assert sample_future_events == future_events


@pytest.mark.freeze_time("2022-05-13")
def test_split_dict_list(sample_events, sample_future_events, sample_past_events):
    past_events, future_events = split_event_dict_list(sample_events)
    assert sample_past_events == past_events
    assert sample_future_events == future_events


def test_set_event_duration__all_day():
    event = Event()
    event.start = datetime.fromisoformat("2020-02-03T00:00:00+00:00")
    event.end = datetime.fromisoformat("2020-02-04T00:00:00+00:00")
    event.all_day = True

    out = set_event_duration(event)
    assert out.duration == "celý den"


def test_set_event_duration__hours():
    event = Event()
    event.start = datetime.fromisoformat("2020-02-03T08:00:00+00:00")
    event.end = datetime.fromisoformat("2020-02-03T12:25:00+00:00")
    event.all_day = False

    out = set_event_duration(event)
    assert out.duration == "9:00 - 13:25"


def test_set_event_duration__days():
    event = Event()
    event.start = datetime.fromisoformat("2020-02-03T09:15:00+00:00")
    event.end = datetime.fromisoformat("2020-02-04T21:30:00+00:00")
    event.all_day = False

    out = set_event_duration(event)
    assert out.duration == "10:15 - 22:30 (4.2.)"
