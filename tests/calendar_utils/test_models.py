import arrow
import pytest
import pytz
from faker import Faker

from calendar_utils.models import Calendar
from tests.models import DummyCalendarModel

pytestmark = pytest.mark.django_db

fake = Faker()


@pytest.mark.freeze_time("2022-05-13")
def test_calendar__update_source(
    sample_response, sample_past_events, sample_future_events
):
    """
    Pokud se změní hash, přenačtou se eventy
    """
    url = "http://foo"
    cal = Calendar.objects.create(url=url)

    cal.handle_event_list(sample_response)

    cal.refresh_from_db()

    assert cal.event_hash == str(hash(str(sample_response)))
    assert cal.last_update == arrow.get("2022-05-13").datetime
    assert sample_past_events == cal.past_events
    assert sample_future_events == cal.future_events


@pytest.mark.freeze_time("2022-05-13")
def test_calendar__update_source__unchanged_source(sample_response):
    """
    Pokud se nezmění hash, tak se nezmění ani eventy
    """
    url = "http://foo"
    event_hash = str(hash(str(sample_response)))

    cal = Calendar.objects.create(
        url=url, event_hash=event_hash, past_events=[], future_events=[]
    )
    cal.handle_event_list(sample_response)

    cal.refresh_from_db()

    assert cal.event_hash == event_hash
    assert cal.last_update == arrow.get("2022-05-13").datetime
    assert [] == cal.past_events
    assert [] == cal.future_events


def test_calendar__save_and_load_events():
    past_events = [
        {
            "start": fake.date_time(tzinfo=pytz.UTC),
            "end": fake.date_time(tzinfo=pytz.UTC),
        },
        {
            "start": fake.date_time(tzinfo=pytz.UTC),
            "end": fake.date_time(tzinfo=pytz.UTC),
        },
    ]
    future_events = [
        {
            "start": fake.date_time(tzinfo=pytz.UTC),
            "end": fake.date_time(tzinfo=pytz.UTC),
        },
    ]

    cal = Calendar.objects.create(
        url=fake.url(), past_events=past_events, future_events=future_events
    )

    cal.refresh_from_db()
    assert cal.past_events == past_events
    assert cal.future_events == future_events


def test_calendar__save_and_load_events__no_values():
    cal = Calendar.objects.create(url=fake.url())

    cal.refresh_from_db()
    assert cal.past_events is None
    assert cal.future_events is None


def test_calendar_mixin__no_calendar_url():
    obj = DummyCalendarModel.objects.create()
    assert obj.calendar is None


def test_calendar_mixin__set_calendar_url(mocker):
    m_update = mocker.patch.object(Calendar, "update_source")
    url = fake.url()

    obj = DummyCalendarModel.objects.create(calendar_url=url)

    obj.refresh_from_db()
    assert obj.calendar_url == url
    assert obj.calendar.url == url
    assert m_update.call_count == 1


def test_calendar_mixin__update_calendar_url(mocker):
    m_update = mocker.patch.object(Calendar, "update_source")
    obj = DummyCalendarModel.objects.create(calendar_url=fake.url())
    url = fake.url()
    m_update.reset_mock()

    obj.calendar_url = url
    obj.save()

    obj.refresh_from_db()
    assert obj.calendar_url == url
    assert obj.calendar.url == url
    assert m_update.call_count == 1


def test_calendar_mixin__unchanged_calendar_url(mocker):
    m_update = mocker.patch.object(Calendar, "update_source")
    url = fake.url()
    obj = DummyCalendarModel.objects.create(calendar_url=url)
    m_update.reset_mock()

    obj.save()

    obj.refresh_from_db()
    assert obj.calendar_url == url
    assert obj.calendar.url == url
    assert m_update.call_count == 1


def test_calendar_mixin__clear_calendar_url(mocker):
    m_update = mocker.patch.object(Calendar, "update_source")
    obj = DummyCalendarModel.objects.create(calendar_url=fake.url())
    m_update.reset_mock()

    obj.calendar_url = None
    obj.save()

    obj.refresh_from_db()
    assert obj.calendar_url is None
    assert obj.calendar is None
    assert m_update.call_count == 0
