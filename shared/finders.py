from wagtail.embeds.finders.oembed import OEmbedFinder


class CustomPeertubeFinder(OEmbedFinder):
    def _get_endpoint(self, url):
        return "https://tv.pirati.cz/services/oembed"


embed_finder_class = CustomPeertubeFinder
