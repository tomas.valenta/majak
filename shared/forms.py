from django import forms
from django.forms import CheckboxSelectMultiple, ModelMultipleChoiceField
from modelcluster.forms import ClusterForm
from wagtail.admin.forms import WagtailAdminPageForm
from wagtail.models.media import Collection


class SubscribeForm(forms.Form):
    email = forms.EmailField()
    confirmed = forms.BooleanField()
    return_page_id = forms.IntegerField()


class JekyllImportForm(WagtailAdminPageForm):
    do_import = forms.BooleanField(
        initial=False, required=False, label="Provést import z Jekyllu"
    )
    collection = forms.ModelChoiceField(
        queryset=Collection.objects.all(), required=False, label="Kolekce obrázků"
    )
    dry_run = forms.BooleanField(
        initial=True,
        required=False,
        label="Jenom na zkoušku",
        help_text="Žádné články se neuloží, vypíše případné problémy či "
        "již existující články - 'ostrému' importu existující "
        "články nevadí, přeskočí je",
    )
    jekyll_repo_url = forms.URLField(
        max_length=512,
        required=False,
        help_text="např. https://github.com/pirati-web/pirati.cz",
    )
    readonly_log = forms.CharField(
        disabled=True,
        label="Log z posledního importu",
        required=False,
        widget=forms.Textarea,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["readonly_log"].initial = self.instance.last_import_log

    def clean(self):
        cleaned_data = super().clean()

        if not cleaned_data.get("do_import"):
            return cleaned_data

        if cleaned_data.get("do_import") and not self.instance.id:
            self.add_error(
                "do_import", "Import proveďte prosím až po vytvoření stránky"
            )

        if not cleaned_data.get("collection"):
            self.add_error("collection", "Pro import je toto pole povinné")
        if not cleaned_data.get("jekyll_repo_url"):
            self.add_error("jekyll_repo_url", "Pro import je toto pole povinné")

        if cleaned_data.get("jekyll_repo_url", "").endswith(".zip"):
            self.add_error(
                "jekyll_repo_url", "Vložte odkaz pouze na repozitář, ne na zip"
            )

        return cleaned_data

    def save(self, commit=True):
        if self.cleaned_data.get("do_import"):
            self.handle_import()

        return super().save(commit=commit)


class TagModelChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.tag.name


from taggit.models import Tag


class ArticlesPageForm(ClusterForm):
    displayed_tags = TagModelChoiceField(
        queryset=Tag.objects.filter(id=-1),
        required=False,
        label="Z tohoto webu",
        widget=CheckboxSelectMultiple,
    )
    displayed_shared_tags = ModelMultipleChoiceField(
        queryset=Tag.objects.filter(id=-1),
        required=False,
        label="Sdílecí",
        help_text=(
            "Aby se zde zobrazily štítky, je nutné nejdříve vybrat "
            "odebírané štítky v sekci níže a publikovat stránku."
        ),
        widget=CheckboxSelectMultiple,
    )

    shared_tags = ModelMultipleChoiceField(
        queryset=Tag.objects.filter(id=-1),
        required=False,
        label="Štítky pro sdílení mezi weby",
        help_text="Články z ostatních webů se štítky, které vybereš, se ukážou na tomto webu.",
        widget=CheckboxSelectMultiple,  # Display as checkboxes
    )
