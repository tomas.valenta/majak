from wagtail.blocks import ListBlock, StructBlock, TextBlock
from wagtail.images.blocks import ImageChooserBlock


class GalleryBlock(StructBlock):
    gallery_items = ListBlock(
        ImageChooserBlock(label="obrázek", required=True),
        label="Galerie",
        icon="image",
        group="3. Ostatní",
    )

    class Meta:
        label = "Galerie"
        icon = "image"
        group = "2. Obrázky"
        template = "styleguide2/includes/molecules/gallery/gallery.html"


class FigureBlock(StructBlock):
    img = ImageChooserBlock(label="Obrázek", required=True)
    caption = TextBlock(label="Popisek", required=False)

    class Meta:
        label = "Obrázek"
        icon = "image"
        group = "2. Obrázky"
        template = "styleguide2/includes/atoms/figure/figure.html"
