from wagtail import blocks


class CalendarBlock(blocks.StructBlock):
    title = blocks.CharBlock(label="Titulek", required=False)

    info = blocks.StaticBlock(
        label="Volba kalendáře",
        admin_text="Adresa kalendáře se zadává v nastavení hlavní stránky webu",
    )

    class Meta:
        label = "Kalendář"
        icon = "calendar-alt"
        group = "3. Ostatní"
        template = "styleguide2/includes/organisms/main_section/district/calendar.html"
