from wagtail import blocks
from wagtail.contrib.table_block.blocks import TableBlock

from .text import RICH_TEXT_DEFAULT_FEATURES, HeadlineBlock


class PopoutPoint(blocks.StructBlock):
    name = blocks.CharBlock(
        label="Název",
        required=True,
    )

    content = blocks.StreamBlock(
        [
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                    template="styleguide2/includes/atoms/text/prose_richtext.html",
                ),
            ),
            ("headline", HeadlineBlock()),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
        ],
        label="Obsah",
    )

    class Meta:
        label = "Rozbalovací blok"
        group = "3. Ostatní"
        template = "styleguide2/includes/molecules/popouts/popout_point.html"
