from django.forms.utils import ErrorList
from wagtail import blocks
from wagtail.blocks.struct_block import StructBlockValidationError


class MenuItemBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        label="Titulek",
        help_text="Pokud není odkazovaná stránka na Majáku, použij možnost zadání samotné adresy níže.",
        required=True,
    )
    page = blocks.PageChooserBlock(label="Stránka", required=False)
    link = blocks.URLBlock(label="Odkaz", required=False)

    class Meta:
        label = "Položka v menu"
        template = "styleguide/2.3.x/menu_item.html"

    def clean(self, value):
        errors = {}

        if value["page"] and value["link"]:
            errors["page"] = ErrorList(
                ["Stránka nemůže být vybrána současně s odkazem."]
            )
            errors["link"] = ErrorList(
                ["Odkaz nemůže být vybrán současně se stránkou."]
            )
        if errors:
            raise StructBlockValidationError(errors)
        return super().clean(value)


class MenuParentBlock(blocks.StructBlock):
    title = blocks.CharBlock(label="Titulek", required=True)
    menu_items = blocks.ListBlock(MenuItemBlock(), label="Položky menu")

    class Meta:
        label = "Vyskakovací menu"
        template = "styleguide2/includes/molecules/dropdown/dropdown.html"
