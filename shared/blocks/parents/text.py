from wagtail import blocks
from wagtail.blocks import CharBlock, RichTextBlock, StructBlock
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock

from maps_utils.blocks import MapFeatureCollectionBlock, MapPointBlock
from shared.const import RICH_TEXT_DEFAULT_FEATURES

from ..children import AlignBlock, ColorBlock
from .button import ButtonBlock, ButtonGroupBlock
from .images import FigureBlock
from .struct import CardBlock
from .video import YouTubeVideoBlock


class AdvancedTextBlock(ColorBlock, AlignBlock):
    text = blocks.RichTextBlock(
        label="Textový editor",
        group="1. Text",
        features=RICH_TEXT_DEFAULT_FEATURES,
    )

    class Meta:
        label = "Textový editor (pokročilý)"
        icon = "doc-full"
        group = "1. Text"
        template = "styleguide2/includes/atoms/text/prose_advanced_richtext.html"


class ColumnsTextBlock(blocks.StructBlock):
    left_text = blocks.RichTextBlock(
        label="levý sloupec", features=RICH_TEXT_DEFAULT_FEATURES
    )
    right_text = blocks.RichTextBlock(
        label="pravý sloupec", features=RICH_TEXT_DEFAULT_FEATURES
    )

    class Meta:
        label = "Text dva sloupce"
        icon = "doc-full"
        group = "1. Text"
        template = "styleguide2/includes/atoms/text/two_columns_richtext.html"


# TODO: Merge
class TwoTextColumnBlock(StructBlock):
    text_column_1 = RichTextBlock(label="První sloupec textu")
    text_column_2 = RichTextBlock(label="Druhý sloupec textu")

    class Meta:
        icon = "doc-full"
        group = "1. Text"
        label = "Text ve dvou sloupcích"


class ArticleQuoteBlock(StructBlock):
    quote = CharBlock(label="Citace")
    autor_name = CharBlock(label="Jméno autora")

    class Meta:
        icon = "user"
        group = "1. Text"
        label = "Blok citace"
        template = "styleguide2/includes/legacy/article_quote_block.html"


class ArticleDownloadBlock(StructBlock):
    file = DocumentChooserBlock(label="Stáhnutelný soubor")

    class Meta:
        icon = "user"
        group = "1. Text"
        label = "Blok stáhnutelného dokumentu"
        template = "styleguide2/includes/molecules/blocks/download_block.html"


class HeadlineBlock(blocks.StructBlock):
    headline = blocks.CharBlock(label="Nadpis", max_length=300, required=True)

    tag = blocks.ChoiceBlock(
        choices=(
            ("h1", "H1"),
            ("h2", "H2"),
            ("h3", "H3"),
            ("h4", "H4"),
            ("h5", "H5"),
            ("h6", "H6"),
        ),
        label="Úroveň nadpisu",
        help_text="Čím nižší číslo, tím vyšší úroveň.",
        required=True,
        default="h1",
    )

    style = blocks.ChoiceBlock(
        choices=(
            ("head-alt-xl", "Velký, Bebas Neue - 6XL"),
            ("head-alt-lg", "Střední, Bebas Neue - 4XL"),
            ("head-alt-md", "Základní velikost - Roboto - MD"),
            ("head-alt-sm", "Malý - Roboto - SM"),
            ("head-alt-xs", "Extra malý - Roboto - XS"),
        ),
        label="Velikost",
        help_text="Náhled si prohlédněte na https://styleguide2.pirati.cz/pattern/patterns/atoms/text/headings.html.",
        default="head-alt-xl",
        required=True,
    )

    align = blocks.ChoiceBlock(
        choices=(
            ("auto", "Automaticky"),
            ("center", "Na střed"),
        ),
        label="Zarovnání",
        default="auto",
        required=True,
    )

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context)

        context["responsive_style"] = ""

        context["responsive_style"] = {
            "head-alt-xl": "head-6xl",
            "head-alt-lg": "head-4xl",
            "head-alt-md": "head-base",
            "head-alt-sm": "head-sm",
            "head-alt-xs": "head-xs",
        }.get(value["style"], "head-4xl")

        context["responsive_style"] += {
            "auto": "",
            "center": " text-center",
        }.get(value["align"], "")

        return context

    class Meta:
        label = "Nadpis"
        icon = "bold"
        group = "1. Text"
        template = "styleguide2/includes/atoms/text/heading.html"


class TwoColumnBlock(blocks.StructBlock):
    left_column_content = blocks.StreamBlock(
        label="Obsah levého sloupce",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("card", CardBlock()),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
            ("button", ButtonBlock()),
            ("button_group", ButtonGroupBlock()),
        ],
        required=True,
    )
    right_column_content = blocks.StreamBlock(
        label="Obsah pravého sloupce",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("card", CardBlock()),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
            ("button", ButtonBlock()),
            ("button_group", ButtonGroupBlock()),
        ],
        required=True,
    )

    class Meta:
        label = "Dva sloupce"
        icon = "grip"
        group = "1. Text"
        template = "styleguide2/includes/atoms/grids/two_columns.html"


class ThreeColumnBlock(blocks.StructBlock):
    left_column_content = blocks.StreamBlock(
        label="Obsah levého sloupce",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("card", CardBlock()),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
            ("button", ButtonBlock()),
            ("button_group", ButtonGroupBlock()),
        ],
        required=True,
    )
    middle_column_content = blocks.StreamBlock(
        label="Obsah prostředního sloupce",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("card", CardBlock()),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
            ("button", ButtonBlock()),
            ("button_group", ButtonGroupBlock()),
        ],
        required=True,
    )
    right_column_content = blocks.StreamBlock(
        label="Obsah pravého sloupce",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("card", CardBlock()),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
            ("button", ButtonBlock()),
            ("button_group", ButtonGroupBlock()),
        ],
        required=True,
    )

    class Meta:
        label = "Tři sloupce"
        icon = "grip"
        group = "1. Text"
        template = "styleguide2/includes/atoms/grids/three_columns.html"


class PictureListBlock(ColorBlock):
    items = blocks.ListBlock(
        blocks.RichTextBlock(label="Odstavec", features=RICH_TEXT_DEFAULT_FEATURES),
        label="Odstavce",
    )
    picture = ImageChooserBlock(
        label="Obrázek",
        help_text="Rozměr 30x30px nebo více (obrázek bude zmenšen na 30x30px)",
    )

    class Meta:
        label = "Seznam z obrázkovými odrážkami"
        icon = "list-ul"
        group = "1. Text"
        template = "styleguide2/includes/molecules/lists/image_list.html"


class PictureHeadlineBlock(ColorBlock):
    title = blocks.CharBlock(label="nadpis")
    picture = ImageChooserBlock(
        label="obrázek",
        help_text="rozměr na výšku 75px nebo více (obrázek bude zmenšen na výšku 75px)",
    )

    class Meta:
        label = "Nadpis (s obrázkem)"
        icon = "title"
        group = "1. Text"
        template = "styleguide2/includes/atoms/text/heading_with_image.html"


class AdvancedColumnsTextBlock(ColorBlock, AlignBlock):
    left_text = blocks.RichTextBlock(
        label="levý sloupec", features=RICH_TEXT_DEFAULT_FEATURES
    )
    right_text = blocks.RichTextBlock(
        label="pravý sloupec", features=RICH_TEXT_DEFAULT_FEATURES
    )

    class Meta:
        label = "Text dva sloupce (pokročilý)"
        icon = "doc-full"
        group = "1. Text"
        template = "styleguide2/includes/atoms/text/advanced_two_columns_richtext.html"
