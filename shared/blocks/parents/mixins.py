from django.template.defaultfilters import slugify
from wagtail import blocks

from ..children import CardLinkWithHeadlineBlockMixin, PersonCustomPositionBlockMixin


class PeopleGroupBlockMixin(blocks.StructBlock):
    title = blocks.CharBlock(label="Titulek")
    slug = blocks.CharBlock(
        label="Slug skupiny",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
    )

    # NOTE: Must (or at least should) be changed in mixed-in blocks.
    person_list = blocks.ListBlock(
        blocks.PageChooserBlock(label="Detail osoby"),
        label="Osoby",
        default=[],
        help_text="S pozicemi z jejich podstránek",
    )

    # NOTE: Must (or at least should) be changed in mixed-in blocks.
    person_list_with_custom_positions = blocks.ListBlock(
        PersonCustomPositionBlockMixin(),
        label="Osoby",
        default=[],
        help_text="S nastavitelnými pozicemi",
    )

    class Meta:
        icon = "group"
        group = "3. Ostatní"
        label = "Skupina osob"
        template = "styleguide2/includes/organisms/cards/people_card_list.html"

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value


class TeamBlockMixin(blocks.StructBlock):
    title = blocks.CharBlock(label="Název sekce týmů")
    slug = blocks.CharBlock(
        label="Slug sekce",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky vyplněno",
    )
    # NOTE: Must (or at least should) be overrided in mixed-in classes
    team_list = blocks.ListBlock(
        CardLinkWithHeadlineBlockMixin(label="Karta týmu"),
        label="Týmy",
    )

    class Meta:
        icon = "group"
        label = "Týmy"

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value
