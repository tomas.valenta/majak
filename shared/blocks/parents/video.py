import re
import urllib

from django.core.files.images import ImageFile
from django.forms.utils import ErrorList
from wagtail import blocks
from wagtail.blocks.struct_block import StructBlockValidationError
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.models import Image
from wagtail.models import Collection


class YouTubeVideoBlock(blocks.StructBlock):
    poster_image = ImageChooserBlock(
        label="Náhled videa (automatické pole)",
        required=False,
        help_text="Není třeba vyplňovat, náhled bude " "dohledán automaticky.",
    )
    video_url = blocks.URLBlock(
        label="Odkaz na video",
        required=False,
        help_text="Odkaz na YouTube video bude automaticky "
        "zkonvertován na ID videa a NEBUDE uložen.",
    )
    video_id = blocks.CharBlock(
        label="ID videa (automatické pole)",
        required=False,
        help_text="Není třeba vyplňovat, bude automaticky " "načteno z odkazu.",
    )

    class Meta:
        label = "YouTube video"
        icon = "media"
        group = "2. Obrázky"
        template = "styleguide2/includes/atoms/youtube_video/youtube_video.html"

    def clean(self, value):
        errors = {}

        if not value["video_url"] and not value["video_id"]:
            errors["video_url"] = ErrorList(["Zadejte prosím odkaz na YouTube video."])

        if value["video_url"]:
            if not value["video_url"].startswith("https://youtu.be") and not value[
                "video_url"
            ].startswith("https://www.youtube.com"):
                errors["video_url"] = ErrorList(
                    [
                        'Odkaz na video musí začínat "https://www.youtube.com" '
                        'nebo "https://youtu.be"'
                    ]
                )

        if value["video_id"]:
            if not re.match("^[A-Za-z0-9_-]{11}$", value["video_id"]):
                errors["video_url"] = ErrorList(
                    ["Formát ID YouTube videa není validní"]
                )

        if errors:
            raise StructBlockValidationError(errors)
        return super().clean(value)

    def get_prep_value(self, value):
        value = super().get_prep_value(value)

        if value["video_url"]:
            value["video_id"] = self.convert_youtube_link_to_video_id(
                value["video_url"]
            )
            value["poster_image"] = self.get_wagtail_image_id_for_youtube_poster(
                value["video_id"]
            )

        value["video_url"] = ""

        return value

    @staticmethod
    def convert_youtube_link_to_video_id(url):
        reg_str = (
            "((?<=(v|V)/)|(?<=youtu\.be/)|(?<=youtube\.com/watch(\?|\&)v=)"
            "|(?<=embed/))([\w-]+)"
        )
        search_result = re.search(reg_str, url)

        if search_result:
            return search_result.group(0)

        logger.warning(
            "Nepodařilo se získat video ID z YouTube URL", extra={"url": url}
        )
        return ""

    @classmethod
    def get_wagtail_image_id_for_youtube_poster(cls, video_id) -> int or None:
        image_url = "https://img.youtube.com/vi/{}/hqdefault.jpg".format(video_id)

        img_path = "/tmp/{}.jpg".format(video_id)
        urllib.request.urlretrieve(image_url, img_path)
        file = ImageFile(open(img_path, "rb"), name=img_path)

        return cls.get_image_id(file, "YT_poster_v_{}".format(video_id))

    @classmethod
    def get_image_id(cls, file: ImageFile, image_title: str) -> int:
        try:
            image = Image.objects.get(title=image_title)
        except Image.DoesNotExist:
            image = Image(
                title=image_title, file=file, collection=cls.get_posters_collection()
            )
            image.save()
        return image.id

    @staticmethod
    def get_posters_collection() -> Collection:
        collection_name = "YouTube nahledy"

        try:
            collection = Collection.objects.get(name=collection_name)
        except Collection.DoesNotExist:
            root_collection = Collection.get_first_root_node()
            collection = root_collection.add_child(name=collection_name)

        return collection
