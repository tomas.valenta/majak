import logging

from django.forms.utils import ErrorList
from wagtail import blocks
from wagtail.blocks import CharBlock, ListBlock, StructBlock, TextBlock
from wagtail.blocks.struct_block import StructBlockValidationError
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.images.blocks import ImageChooserBlock

from maps_utils.blocks import MapFeatureCollectionBlock, MapPointBlock
from shared.const import RICH_TEXT_DEFAULT_FEATURES

from ..children import FlipCardBlock, LinkBlock, PersonBoxBlock
from .images import FigureBlock
from .video import YouTubeVideoBlock

logger = logging.getLogger(__name__)


class SocialLinkBlock(LinkBlock):
    icon = CharBlock(
        label="Ikona",
        help_text="Seznam ikon - https://styleguide.pirati.cz/latest/?p=viewall-atoms-icons <br/>"
        "Název ikony zadejte bez tečky na začátku",
    )  # TODO CSS class name or somthing better?

    class Meta:
        icon = "link"
        label = "Odkaz"


class NewsBlock(StructBlock):
    title = CharBlock(
        label="Titulek",
        help_text="Nejnovější články se načtou automaticky",
    )
    description = TextBlock(label="Popis", required=False)

    class Meta:
        icon = "doc-full-inverse"
        label = "Novinky"


class PersonContactBoxBlock(StructBlock):
    title = CharBlock(label="Titulek")
    image = ImageChooserBlock(label="Ikona")
    subtitle = CharBlock(label="Podtitulek")

    class Meta:
        icon = "mail"
        label = "Kontakty"


class OtherLinksBlock(StructBlock):
    title = CharBlock(label="Titulek")
    list = ListBlock(LinkBlock, label="Seznam odkazů")

    class Meta:
        icon = "link"
        label = "Odkazy"


class FlipCardsBlock(StructBlock):
    cards = ListBlock(
        FlipCardBlock(label="Karta"),
        label="Karty",
    )

    class Meta:
        icon = "group"
        label = "Seznam obracecích karet"
        group = "3. Ostatní"
        template = "styleguide2/includes/organisms/cards/flip_card_list.html"


class PeopleOverviewBlock(StructBlock):
    title_line_1 = CharBlock(label="První řádek titulku")
    title_line_2 = CharBlock(label="Druhý řádek titulku")

    description = TextBlock(label="Popis")

    list = ListBlock(PersonBoxBlock, label="Boxíky")

    class Meta:
        template = (
            "styleguide2/includes/organisms/main_section/representatives_section.html"
        )
        icon = "group"
        label = "Skupina osob"


class CardBlock(blocks.StructBlock):
    img = ImageChooserBlock(label="Obrázek", required=False)

    headline = blocks.TextBlock(label="Titulek", required=False)

    content = blocks.StreamBlock(
        label="Obsah",
        local_blocks=[
            (
                "text",
                blocks.RichTextBlock(
                    label="Textový editor",
                    group="1. Text",
                    features=RICH_TEXT_DEFAULT_FEATURES,
                ),
            ),
            (
                "table",
                TableBlock(
                    template="styleguide2/includes/atoms/table/table.html",
                    group="3. Ostatní",
                    label="Tabulka",
                ),
            ),
            ("figure", FigureBlock()),
            ("youtube", YouTubeVideoBlock()),
            ("map_point", MapPointBlock(label="Špendlík na mapě")),
            ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
        ],
        required=False,
    )

    page = blocks.PageChooserBlock(label="Stránka", required=False)

    link = blocks.URLBlock(label="Odkaz", required=False)

    class Meta:
        label = "Karta"
        group = "3. Ostatní"
        icon = "form"
        template = "styleguide2/includes/molecules/boxes/card_box_block.html"

    def clean(self, value):
        errors = {}

        if value["page"] and value["link"]:
            errors["page"] = ErrorList(
                ["Stránka nemůže být vybrána současně s odkazem."]
            )
            errors["link"] = ErrorList(
                ["Odkaz nemůže být vybrán současně se stránkou."]
            )

        if errors:
            raise StructBlockValidationError(errors)

        return super().clean(value)


class NewsletterSubscriptionBlock(blocks.StructBlock):
    list_id = blocks.CharBlock(label="ID newsletteru", required=True)

    title_line_1 = blocks.CharBlock(
        label="Nadpis bloku (1. řádek)", required=True, default="Odebírej náš"
    )

    title_line_2 = blocks.CharBlock(
        label="Nadpis bloku (2. řádek)", required=True, default="newsletter"
    )

    description = blocks.CharBlock(
        label="Popis newsletteru",
        required=True,
        default="Fake news tam nenajdeš, ale dozvíš se, co chystáme doopravdy!",
    )

    class Meta:
        label = "Formulář pro odebírání newsletteru"
        icon = "form"
        group = "3. Ostatní"
        template = "styleguide2/includes/organisms/main_section/newsletter_section.html"
