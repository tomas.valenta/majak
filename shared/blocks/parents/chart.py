import json

from django.core.exceptions import ValidationError
from wagtail import blocks

from ..children import ChartDataset, ChartRedmineIssueDataset


class ChartBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        label="Název",
        max_length=120,
    )
    chart_type = blocks.ChoiceBlock(
        label="Typ",
        choices=[
            ("bar", "Graf se sloupci"),
            ("horizontalBar", "Graf s vodorovnými sloupci"),
            ("pie", "Koláčový graf"),
            ("doughnut", "Donutový graf"),
            ("polarArea", "Graf polární oblasti"),
            ("radar", "Radarový graf"),
            ("line", "Graf s liniemi"),
        ],
        default="bar",
    )

    hide_points = blocks.BooleanBlock(
        label="Schovat body",
        required=False,
        help_text="Mění vzhled pouze u linových grafů.",
    )

    local_labels = blocks.ListBlock(
        blocks.CharBlock(
            max_length=40,
            label="Skupina",
        ),
        default=[],
        blank=True,
        required=False,
        collapsed=True,
        label="Místně definované skupiny",
    )
    local_datasets = blocks.ListBlock(
        ChartDataset(),
        default=[],
        blank=True,
        required=False,
        collapsed=True,
        label="Místní zdroje dat",
    )

    redmine_issue_datasets = blocks.ListBlock(
        ChartRedmineIssueDataset(label="Redmine úkoly"),
        default=[],
        blank=True,
        required=False,
        label="Zdroje dat z Redmine (úkoly)",
        help_text=(
            "Úkoly, podle doby vytvoření. Pokud definuješ "
            "více zdrojů, datumy v nich musí být stejné."
        ),
    )

    def clean(self, value):
        result = super().clean(value)

        redmine_issues_exist = len(value.get("redmine_issue_datasets", [])) != 0

        if len(value.get("local_datasets", [])) != 0 and redmine_issues_exist:
            raise ValidationError(
                "Definuj pouze jeden typ zdroje dat - místní, nebo z Redmine."
            )

        if redmine_issues_exist:
            min_date = value["redmine_issue_datasets"][0]["created_on_min_date"]
            max_date = value["redmine_issue_datasets"][0]["created_on_max_date"]

            if len(value["redmine_issue_datasets"]) > 1:
                for dataset in value["redmine_issue_datasets"]:
                    if (
                        dataset["created_on_min_date"] != min_date
                        or dataset["created_on_max_date"] != max_date
                    ):
                        raise ValidationError(
                            "Maximální a minimální data všech zdrojů z Redmine musí být stejné"
                        )

        return result

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)

        datasets = []
        labels = []

        if len(value["local_datasets"]) != 0:
            labels = value["local_labels"]

            for dataset in value["local_datasets"]:
                datasets.append(
                    {
                        "label": dataset["label"],
                        "data": [item for item in dataset["data"]],
                    }
                )
        elif len(value["redmine_issue_datasets"]) != 0:
            for dataset_wrapper in value["redmine_issue_datasets"]:
                redmine_context = ChartRedmineIssueDataset().get_context(
                    dataset_wrapper
                )

                labels = redmine_context["parsed_issue_labels"]
                datasets += redmine_context["parsed_issues"]

        value["datasets"] = json.dumps(datasets)
        value["labels"] = json.dumps([label for label in labels])

        return context

    class Meta:
        template = "styleguide2/includes/molecules/blocks/chart.html"
        label = "Graf"
        group = "2. Obrázky"
        icon = "form"
        help_text = """Všechny položky zdrojů dat se chovají jako sloupce.
Zobrazí se tolik definovaných sloupců, kolik existuje skupin."""
