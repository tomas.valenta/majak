from wagtail import blocks
from wagtail.blocks import (
    CharBlock,
    ListBlock,
    RichTextBlock,
    StreamBlock,
    StructBlock,
    URLBlock,
)

from ..children import (
    CandidateBlock,
    CarouselProgramCategoryBlock,
    CustomPrimaryCandidateBlock,
    ProgramGroupBlockMixin,
    ProgramPointBlock,
    ProgramPopoutCategory,
    SecondaryCandidateBlock,
)


class ProgramGroupBlockPopout(StructBlock):
    preamble = RichTextBlock(
        label="Preambule",
        help_text="Zobrazí se nad přepínačem mezi kandidáty a programem.",
        required=False,
    )

    categories = ListBlock(ProgramPopoutCategory(), label="Kategorie programu")

    class Meta:
        icon = "date"
        label = "Vyskakovací program"


class ProgramGroupBlock(ProgramGroupBlockMixin):
    point_list = ListBlock(ProgramPointBlock(), label="Jednotlivé články programu")

    class Meta:
        icon = "date"
        label = "Běžný program"


class CandidateListBlock(StructBlock):
    # NOTE: should be changed in mixed-in blocks.
    stream_candidates = StreamBlock(
        [
            ("candidate", CandidateBlock()),
            ("custom_candidate", CustomPrimaryCandidateBlock()),
        ],
        required=False,
        label=" ",  # Hacky way to show no label
    )

    class Meta:
        template = (
            "styleguide2/includes/organisms/candidates/candidate_primary_list.html"
        )
        icon = "form"
        label = "Seznam kandidátů"


class CandidateSecondaryListBlock(StructBlock):
    heading = CharBlock(
        label=" ",  # Hacky way to show no label
        required=False,
        default="Ostatní kandidáti",
    )

    # NOTE: should be changed in mixed-in blocks.
    candidates = ListBlock(
        SecondaryCandidateBlock(),
        min_num=0,
        default=[],
        label=" ",  # Hacky way to show no label
    )

    class Meta:
        template = (
            "styleguide2/includes/organisms/candidates/candidate_secondary_list.html"
        )
        icon = "form"
        label = "Sekundární seznam kandidátů"


class CarouselProgramBlock(StructBlock):
    label = CharBlock(label="Nadpis", help_text="Např. 'Program'", default="Program")

    categories = ListBlock(CarouselProgramCategoryBlock(), label="Kategorie")

    long_version_url = URLBlock(
        label="Odkaz na celou verzi programu",
        help_text="Pro zobrazení odkazu na celou verzi programu musí být obě následující pole vyplněná.",
        required=False,
    )
    long_version_text = CharBlock(
        label="Nadpis odkazu na celou verzi programu", required=False
    )

    class Meta:
        icon = "form"
        group = "Program"
        label = "Priority programu, carousel"
        template = "styleguide2/includes/molecules/program/card_program.html"


class ProgramItemBlock(blocks.StructBlock):
    title = blocks.CharBlock(label="Název", required=True)
    completion_percentage = blocks.IntegerBlock(
        label="Procento dokončení", required=True
    )
    issue_link = blocks.URLBlock(label="Odkaz na Redmine issue", required=False)
