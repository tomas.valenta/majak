from django.forms.utils import ErrorList
from wagtail import blocks
from wagtail.blocks.struct_block import StructBlockValidationError


class ButtonBlock(blocks.StructBlock):
    title = blocks.CharBlock(label="Titulek", max_length=128, required=True)

    color = blocks.ChoiceBlock(
        choices=(
            ("black", "Černá"),
            ("white", "Bílá"),
            ("pirati-yellow", "Žlutá"),
            ("grey-125", "Světle šedá"),
            ("blue-300", "Modrá"),
            ("cyan-200", "Tyrkysová"),
            ("green-400", "Zelená"),
            ("violet-400", "Vínová"),
            ("red-600", "Červená"),
        ),
        label="Barva",
        default="black",
    )

    hoveractive = blocks.BooleanBlock(
        label="Animovat na hover",
        default=True,
        help_text="Pokud je zapnuto, tlačítko při najetí kurzorem ukáže žlutou šipku.",
        required=False,
    )

    page = blocks.PageChooserBlock(label="Stránka", required=False)

    link = blocks.URLBlock(label="Odkaz", required=False)

    align = blocks.ChoiceBlock(
        choices=(
            ("auto", "Automaticky"),
            ("center", "Na střed"),
        ),
        label="Zarovnání",
        default="auto",
    )

    class Meta:
        label = "Tlačítko"
        icon = "code"
        group = "1. Text"
        template = "styleguide2/includes/atoms/buttons/round_button_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context)

        context["background_color"] = f"bg-{value['color']}"

        context["text_color"] = (
            "text-white"
            if value["color"]
            in (
                "black",
                "red-600",
                "blue-300",
                "cyan-200",
                "green-400",
                "violet-400",
                "red-600",
            )
            else "text-black"
        )

        context[
            "color_classes"
        ] = f"{context['background_color']} {context['text_color']}"

        return context

    def clean(self, value):
        errors = {}

        if value["page"] and value["link"]:
            errors["page"] = ErrorList(
                ["Stránka nemůže být vybrána současně s odkazem."]
            )
            errors["link"] = ErrorList(
                ["Odkaz nemůže být vybrán současně se stránkou."]
            )

        if not value["page"] and not value["link"]:
            errors["page"] = ErrorList(["Stránka nebo odkaz musí být vyplněna."])
            errors["link"] = ErrorList(["Stránka nebo odkaz musí být vyplněna."])

        if value["hoveractive"] and value["color"] not in (
            "black",
            "white",
            "grey-125",
        ):
            errors["hoveractive"] = ErrorList(
                ["Šipku lze ukazovat pouze s černým, bílým nebo šedým pozadím."]
            )

        if errors:
            raise StructBlockValidationError(errors)

        return super().clean(value)


class ButtonGroupBlock(blocks.StructBlock):
    buttons = blocks.ListBlock(ButtonBlock(), label="Tlačítka")

    class Meta:
        label = "Skupina tlačítek"
        icon = "list-ul"
        group = "1. Text"
        template = "styleguide2/includes/atoms/buttons/round_button_group_block.html"
