from django.template.defaultfilters import slugify
from wagtail.blocks import CharBlock, ListBlock, RichTextBlock, StreamBlock, StructBlock
from wagtail.contrib.table_block.blocks import TableBlock

from .children import *  # noqa
from .parents import *  # noqa

DEFAULT_CONTENT_BLOCKS = [
    (
        "text",
        blocks.RichTextBlock(
            label="Textový editor",
            group="1. Text",
            features=RICH_TEXT_DEFAULT_FEATURES,
            template="styleguide2/includes/atoms/text/prose_richtext.html",
        ),
    ),
    ("advanced_text", AdvancedTextBlock()),
    ("two_columns_text", ColumnsTextBlock()),
    ("headline", HeadlineBlock()),
    ("headline_with_picture", PictureHeadlineBlock()),
    ("picture_list", PictureListBlock()),
    ("flip_cards", FlipCardsBlock()),
    (
        "table",
        TableBlock(
            template="styleguide2/includes/atoms/table/table.html",
            group="3. Ostatní",
            label="Tabulka",
        ),
    ),
    (
        "popout_table",
        TableBlock(
            template="styleguide2/includes/atoms/table/popout_table.html",
            group="3. Ostatní",
            label="Rozbalovací tabulka",
        ),
    ),
    ("gallery", GalleryBlock(label="Galerie")),
    ("figure", FigureBlock()),
    ("card", CardBlock()),
    ("two_columns", TwoColumnBlock()),
    ("three_columns", ThreeColumnBlock()),
    ("youtube", YouTubeVideoBlock(label="YouTube video")),
    ("map_point", MapPointBlock(label="Špendlík na mapě")),
    ("map_collection", MapFeatureCollectionBlock(label="Mapová kolekce")),
    ("button", ButtonBlock()),
    ("button_group", ButtonGroupBlock()),
    ("popout_point", PopoutPoint()),
]


# Create these blocks here since it requires DEFAULT_CONTENT_BLOCKS.
class ProgramGroupWithCandidatesOtherViewBlock(StructBlock):
    title = CharBlock(
        label="Název sekce",
        help_text="Např. 'Volební noviny'",
    )

    child_blocks = StreamBlock(DEFAULT_CONTENT_BLOCKS, label="Obsah")

    class Meta:
        label = "Blok obsahu"
        icon = "list-ul"


class ProgramGroupWithCandidatesBlock(StructBlock):
    title = CharBlock(
        label="Název programu",
        help_text="Např. 'Krajské volby 2024', 'Evropské volby 2024', ...",
    )

    preamble_content = RichTextBlock(
        label="Preambule",
        help_text="Text, který se zobrazí před přepínačem mezi kandidáty a programem.",
        required=False,
    )

    candidates_title = CharBlock(
        label="Nadpis záložky pro kandidáty", default="Kandidáti"
    )

    program_title = CharBlock(label="Nadpis záložky pro program", default="Program")

    primary_candidates = CandidateListBlock(
        label="Osoby na čele kandidátky",
        help_text="Zobrazí se ve velkých blocích na začátku stránky.",
    )

    secondary_candidates = CandidateSecondaryListBlock(
        label="Ostatní osoby na kandidátce",
        help_text="Zobrazí se v kompaktním seznamu pod čelem kandidátky. Níže můžeš změnit nadpis jejich sekce.",
    )

    other_views = ListBlock(
        ProgramGroupWithCandidatesOtherViewBlock(),
        label="Ostatní záložky",
        default=[],
        min_num=0,
        help_text=(
            "Použitelné např. pro zobrazení volebních novin na separátní "
            "záložce stránky s programem."
        ),
    )

    tab_links = ListBlock(
        LinkBlock(label="Odkaz"),
        label="Odkazy zobrazené jako záložky",
        default=[],
        min_num=0,
        help_text=(
            "Odkazy na jiné části stránky, které se zobrazí jako záložka nahoře."
        ),
    )

    # NOTE: Must be changed in mixed-in classes
    program = StreamBlock(
        [],
        label="Program",
        required=False,
    )

    class Meta:
        icon = "list-ul"
        label = "Program s kandidáty"
        template = "styleguide2/includes/molecules/program/program_block.html"

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value
