from django.forms.utils import ErrorList
from wagtail import blocks
from wagtail.blocks.struct_block import StructBlockValidationError
from wagtail.images.blocks import ImageChooserBlock


class CardLinkBlockMixin(blocks.StructBlock):
    """
    Shared mixin for cards, which vary in templates and the types of pages
    they're allowed to link to.
    """

    image = ImageChooserBlock(label="Obrázek")
    title = blocks.CharBlock(label="Titulek", required=True)
    text = blocks.RichTextBlock(label="Krátký text pod nadpisem", required=False)

    page = blocks.PageChooserBlock(
        label="Stránka",
        page_type=[],
        required=False,
    )
    link = blocks.URLBlock(label="Odkaz", required=False)

    class Meta:
        # template = ""
        icon = "link"
        label = "Karta s odkazem"

    def clean(self, value):
        errors = {}

        if value["page"] and value["link"]:
            errors["page"] = ErrorList(
                ["Stránka nemůže být vybrána současně s odkazem."]
            )
            errors["link"] = ErrorList(
                ["Odkaz nemůže být vybrán současně se stránkou."]
            )
        elif not value["page"] and not value["link"]:
            errors["page"] = ErrorList(["Zvolte stránku nebo vyplňte odkaz."])
            errors["link"] = ErrorList(["Vyplňte odkaz nebo zvolte stránku."])
        if errors:
            raise StructBlockValidationError(errors)
        return super().clean(value)


class CardLinkWithHeadlineBlockMixin(blocks.StructBlock):
    headline = blocks.CharBlock(label="Titulek bloku", required=False)
    card_items = blocks.ListBlock(CardLinkBlockMixin(), label="Karty")

    class Meta:
        # template = ""
        icon = "link"
        label = "Karty"
