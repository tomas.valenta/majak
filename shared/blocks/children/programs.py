from django.utils.text import slugify
from wagtail.blocks import (
    CharBlock,
    IntegerBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock

PROGRAM_RICH_TEXT_FEATURES = [
    "h3",
    "h4",
    "h5",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
    "superscript",
    "subscript",
    "strikethrough",
    "blockquote",
    "embed",
]


class CTAMixin(StructBlock):
    button_link = URLBlock(label="Odkaz tlačítka")
    button_text = CharBlock(label="Text tlačítka")

    class Meta:
        icon = "doc-empty"
        label = "Tlačítko s odkazem"


class ProgramBlockPopout(StructBlock):
    title = CharBlock(label="Titulek vyskakovacího bloku")
    content = RichTextBlock(
        label="Obsah",
        features=PROGRAM_RICH_TEXT_FEATURES,
    )

    # TODO: Change in mixed-in blocks
    guarantor = PageChooserBlock(
        label="Garant", page_type=["district.DistrictPersonPage"], required=False
    )

    class Meta:
        icon = "date"
        label = "Blok programu"


class ProgramPopoutCategory(StructBlock):
    name = CharBlock(label="Název")
    icon = ImageChooserBlock(label="Ikona", required=False)

    description = RichTextBlock(label="Popis", required=False)

    point_list = ListBlock(ProgramBlockPopout(), label="Jednotlivé bloky programu")

    class Meta:
        icon = "date"
        label = "Kategorie programu"


class ProgramGroupBlockMixin(StructBlock):
    title = CharBlock(
        label="Název programu",
        help_text="Např. 'Krajské volby 2024', 'Evropské volby 2024', ...",
    )
    # point_list = ListBlock(ProgramBlock(), label="Jednotlivé články programu")

    class Meta:
        icon = "date"
        template = "styleguide2/includes/molecules/program/program_block.html"
        # label = "Skupina programů"

    def get_prep_value(self, value):
        value = super().get_prep_value(value)
        value["slug"] = slugify(value["title"])
        return value


class ProgramPointBlock(StructBlock):
    url = URLBlock(
        label="Odkaz pokrývající celou tuto část",
        required=False,
    )
    icon = ImageChooserBlock(
        label="Ikona",
        required=False,
    )
    title = CharBlock(label="Titulek článku programu")
    text = RichTextBlock(
        label="Obsah",
        features=PROGRAM_RICH_TEXT_FEATURES,
    )

    class Meta:
        icon = "date"
        label = "Článek programu"


class CarouselProgramCategoryItemBlock(StructBlock):
    content = TextBlock(label="Obsah")

    class Meta:
        icon = "form"
        label = "Bod"


class CarouselProgramCategoryBlock(StructBlock):
    number = IntegerBlock(label="Číslo")

    name = CharBlock(label="Název")

    points = ListBlock(CarouselProgramCategoryItemBlock(), label="Body")

    class Meta:
        icon = "form"
        label = "Kategorie"
