from wagtail.blocks import (
    CharBlock,
    PageChooserBlock,
    RichTextBlock,
    StructBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock


class CandidateBlock(StructBlock):
    # NOTE: Page type should be restricted in mixed-in classes
    page = PageChooserBlock(label="Stránka")

    image = ImageChooserBlock(
        label="Obrázek",
        help_text="Pokud není vybrán, použije se obrázek ze stránky kandidáta",
        required=False,
    )

    description = RichTextBlock(label="Popis", required=False)

    class Meta:
        template = (
            "styleguide2/includes/molecules/candidates/candidate_primary_box.html"
        )
        icon = "form"
        group = "Kandidáti"
        label = "Kandidát"


class CustomPrimaryCandidateBlock(StructBlock):
    title = CharBlock(label="Nadpis")
    subtitle = CharBlock(label="Podnadpis", required=False)
    job = CharBlock(label="Povolání", required=False)

    image = ImageChooserBlock(
        label="Obrázek",
        help_text="Pokud není vybrán, použije se obrázek ze stránky kandidáta",
        required=False,
    )

    description = RichTextBlock(label="Popis", required=False)

    button_url = URLBlock(label="Odkaz tlačítka", required=False)
    button_text = CharBlock(
        label="Text tlačítka",
        help_text="Pokud není vyplněno, odkaz tlačítka se nezobrazí",
        default="Zjisti více",
        required=False,
    )

    class Meta:
        template = "styleguide2/includes/molecules/candidates/custom_candidate_primary_box.html"
        icon = "form"
        group = "Kandidáti"
        label = "Custom blok kandidáta"


class SecondaryCandidateBlock(StructBlock):
    number = CharBlock(label="Číslo")

    # NOTE: Page type should be restricted in mixed-in classes
    page = PageChooserBlock(label="Stránka")

    image = ImageChooserBlock(
        label="Obrázek",
        help_text="Pokud není vybrán, použije se obrázek ze stránky kandidáta",
        required=False,
    )

    class Meta:
        template = (
            "styleguide2/includes/molecules/candidates/candidate_secondary_box.html"
        )
        icon = "form"
        group = "Kandidáti"
        label = "Kandidát"
