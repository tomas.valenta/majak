from django.forms.utils import ErrorList
from django.utils.safestring import mark_safe
from wagtail.blocks import CharBlock, RichTextBlock, StructBlock, TextBlock, URLBlock
from wagtail.blocks.struct_block import StructBlockValidationError
from wagtail.images.blocks import ImageChooserBlock

from .mixins import CTAMixin


class LinkBlock(StructBlock):
    text = CharBlock(label="Název")
    link = URLBlock(label="Odkaz")

    class Meta:
        icon = "link"
        group = "1. Text"
        label = "Odkaz"


class NavbarMenuItemBlock(CTAMixin):
    class Meta:
        label = "Tlačítko"
        template = "styleguide2/includes/molecules/navbar/additional_button.html"


class PersonContactBlockMixin(StructBlock):
    position = CharBlock(label="Název pozice", required=False)

    @property
    def person(self):
        # NOTE: Needs to be implemented

        raise NotImplementedError

    # email, phone?

    class Meta:
        abstract = True
        icon = "user"
        label = "Osoba s volitelnou pozicí"


class FlipCardBlock(StructBlock):
    bg_color = CharBlock(
        label="Barva pozadí",
        default="FEC900",
        help_text=mark_safe(
            "Kód barvy lze vytvořit např. <a href='https://mdn.github.io/css-examples/tools/color-picker/'"
            " target='_blank'>zde</a>."
        ),
    )

    image = ImageChooserBlock(
        label="Obrázek",
        help_text="Nahrazuje pozadí. Nelze vybrat obě najednou.",
        required=False,
    )
    background = ImageChooserBlock(
        label="Pozadí",
        help_text="Nahrazuje obrázek. Nelze vybrat obě najednou.",
        required=False,
    )

    title = TextBlock(label="Nadpis", help_text="Řádkování je manuální.")
    title_color = CharBlock(
        label="Barva textu",
        default="000000",
        help_text=mark_safe(
            "Kód barvy lze vytvořit např. <a href='https://mdn.github.io/css-examples/tools/color-picker/'"
            " target='_blank'>zde</a>."
        ),
    )

    content = RichTextBlock(label="Obsah")

    button_text = CharBlock(
        label="Nadpis tlačítka",
        help_text="Pokud není vyplněn, tlačítko se neukáže.",
        required=False,
    )
    button_url = CharBlock(label="Odkaz tlačítka", required=False)

    def clean(self, value):
        errors = {}

        if value["image"] and value["background"]:
            errors["image"] = ErrorList(
                ["Obrázek nemůže být vybrán současně s pozadím."]
            )
            errors["background"] = ErrorList(
                ["Pozadí nemůže být vybráno současně s obrázkem."]
            )

        if errors:
            raise StructBlockValidationError(errors)

        return super().clean(value)

    class Meta:
        icon = "view"
        group = "3. Ostatní"
        label = "Obracecí karta"
        template = "styleguide2/includes/molecules/boxes/flip_card_box.html"


class PersonBoxBlock(CTAMixin, StructBlock):
    title = CharBlock(label="Nadpis")
    image = ImageChooserBlock(label="Logo/obrázek")

    class Meta:
        icon = "form"
        group = "3. Ostatní"
        label = "Box"
