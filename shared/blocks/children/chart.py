import datetime
import typing
import urllib

import requests
from django.core.cache import cache
from wagtail import blocks


class ChartDataset(blocks.StructBlock):
    label = blocks.CharBlock(
        label="Označení zdroje dat",
        max_length=120,
    )

    data = blocks.ListBlock(
        blocks.IntegerBlock(),
        label="Data",
        default=[0],
    )

    class Meta:
        group = "Data"
        label = "Zdroj dat"


def get_redmine_projects():
    projects = requests.get("https://redmine.pirati.cz/projects.json?limit=10000")

    if projects.ok:
        projects = projects.json()["projects"]
    else:
        projects = []

    return [(project["id"], project["name"]) for project in projects]


class ChartRedmineIssueDataset(blocks.StructBlock):
    projects = blocks.MultipleChoiceBlock(
        label="Projekty", choices=get_redmine_projects
    )

    is_open = blocks.BooleanBlock(
        label="Jen otevřené",
        required=False,
    )
    is_closed = blocks.BooleanBlock(
        label="Jen uzavřené",
        required=False,
    )

    created_on_min_date = blocks.DateBlock(label="Min. datum vytvoření", required=True)
    created_on_max_date = blocks.DateBlock(label="Max. datum vytvoření", required=True)
    updated_on = blocks.CharBlock(
        label="Filtr pro datum aktualizace",
        max_length=128,
        help_text="Např. <=2023-01-01. Více informací na pi2.cz/redmine-api",
        required=False,
    )

    issue_label = blocks.CharBlock(
        label="Označení úkolů uvnitř grafu",
        max_length=128,
        required=True,
    )

    split_per_project = blocks.BooleanBlock(
        label="Rozdělit podle projektu",
        required=False,
    )

    only_grow = blocks.BooleanBlock(
        label="Pouze růst nahoru",
        required=False,
    )

    def _get_issues_url(
        self, value, project_id: typing.Union[None, str, list[str]] = None
    ):
        url = "https://redmine.pirati.cz/issues.json"
        params = [
            ("sort", "created_on"),
            ("limit", "100"),
            (
                "created_on",
                f"><{value['created_on_min_date']}|{value['created_on_max_date']}",
            ),
        ]

        if isinstance(project_id, str):
            params.append(("project_id", project_id))
        elif isinstance(project_id, list):
            params.append(("project_id", ",".join(project_id)))

        is_open = value.get("is_open", False)
        is_closed = value.get("is_closed", False)

        if is_open and is_closed:
            params.append(("status_id", "*"))
        elif is_open:
            params.append(("status_id", "open"))
        elif is_closed:
            params.append(("status_id", "closed"))

        if value.get("updated_on", "") != "":
            params.append(("updated_on", value["updated_on"]))

        is_first = True

        for param_set in params:
            param, param_value = param_set

            url += "?" if is_first else "&"
            url += f"{param}={urllib.parse.quote(param_value)}"

            is_first = False

        return url

    def _get_parsed_issues(self, value, labels, issues_url) -> tuple:
        issues_response = cache.get(f"redmine_{issues_url}")

        if issues_response is None:
            issues_response = requests.get(issues_url)
            issues_response.raise_for_status()
            issues_response = issues_response.json()

            cache.set(
                f"redmine_{issues_url}",
                issues_response,
                timeout=604800,  # 1 week
            )

        only_grow = value.get("only_grow", False)

        collected_issues = issues_response["issues"]
        offset = 0

        while issues_response["total_count"] - offset > len(issues_response["issues"]):
            offset += 100
            url_with_offset = f"{issues_url}&offset={offset}"

            issues_response = cache.get(f"redmine_{url_with_offset}")

            if issues_response is None:
                issues_response = requests.get(url_with_offset)
                issues_response.raise_for_status()
                issues_response = issues_response.json()

                cache.set(
                    f"redmine_{url_with_offset}",
                    issues_response,
                    timeout=604800,  # 1 week
                )

            collected_issues += issues_response["issues"]

        ending_position = len(collected_issues) - 1

        data = None

        current_issue_count = 0
        current_label = datetime.date.fromisoformat(
            collected_issues[0]["created_on"].split("T")[0]
        )

        if not only_grow:
            data = [0] * len(labels)

            for position, issue in enumerate(
                collected_issues
            ):  # Assume correct sorting order
                created_on_date = datetime.date.fromisoformat(
                    issue["created_on"].split("T")[0]
                )

                if current_label != created_on_date or position == ending_position:
                    data[
                        labels.index(current_label)
                    ] = current_issue_count  # Assume labels are unique
                    current_label = created_on_date

                    if position != ending_position:
                        current_issue_count = 0
                    else:
                        data[labels.index(current_label)] = 1
                        break

                current_issue_count += 1
        else:
            data = []
            issue_count_by_date = {}

            for position, issue in enumerate(
                collected_issues
            ):  # Assume correct sorting order
                created_on_date = datetime.date.fromisoformat(
                    issue["created_on"].split("T")[0]
                )

                if current_label not in issue_count_by_date:
                    issue_count_by_date[current_label] = 0

                if current_label != created_on_date or position == ending_position:
                    issue_count_by_date[
                        current_label
                    ] = current_issue_count  # Assume labels are unique
                    current_label = created_on_date

                    if position == ending_position:
                        issue_count_by_date[current_label] = current_issue_count + 1
                        break

                current_issue_count += 1

            previous_date = None

            for date in labels:
                if date not in issue_count_by_date:
                    if previous_date is None:
                        data.append(0)
                        continue

                    data.append(issue_count_by_date[previous_date])
                    continue

                data.append(issue_count_by_date[date])
                previous_date = date

        return data

    def get_context(self, value) -> list:
        context = super().get_context(value)

        labels = []
        datasets = []

        for day_count in range(
            (value["created_on_max_date"] - value["created_on_min_date"]).days + 1
        ):
            day = value["created_on_min_date"] + datetime.timedelta(days=day_count)
            labels.append(day)

        if value.get("split_per_project", False):
            project_choices_lookup = dict(get_redmine_projects())

            for project_id in value["projects"]:
                issues_url = self._get_issues_url(value, project_id)

                datasets.append(
                    {
                        "label": f"{value['issue_label']} - {project_choices_lookup[int(project_id)]}",
                        "data": self._get_parsed_issues(value, labels, issues_url),
                    }
                )
        else:
            issues_url = self._get_issues_url(value, value["projects"])

            datasets.append(
                {
                    "label": value["issue_label"],
                    "data": self._get_parsed_issues(value, labels, issues_url),
                }
            )

        labels = [date.strftime("%d. %m. %Y") for date in labels]

        context["parsed_issue_labels"] = labels
        context["parsed_issues"] = datasets

        return context

    class Meta:
        label = "Zdroj dat z Redmine (úkoly vytvořené za den)"
        group = "Data"
        help_text = (
            "Po prvním otevření se bude stránka otevírat delší dobu, "
            "zatímco se na pozadí načítají data do grafu. Poté bude "
            "fungovat běžně."
        )
