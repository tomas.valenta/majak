from wagtail import blocks

from shared.const import (
    ALIGN_CHOICES,
    ALIGN_CSS,
    BLACK_ON_WHITE,
    COLOR_CHOICES,
    COLOR_CSS,
    LEFT,
)


class ColorBlock(blocks.StructBlock):
    """
    Intended as parent class for blocks with color option.
    """

    color = blocks.ChoiceBlock(COLOR_CHOICES, label="barva", default=BLACK_ON_WHITE)

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        if "css_class" not in context:
            context["css_class"] = []

        # If the CSS class has been removed at some point, just add no classes.
        context["css_class"] += COLOR_CSS.get(value["color"], "")
        return context


class AlignBlock(blocks.StructBlock):
    """
    Intended as parent class for blocks with align option.
    """

    align = blocks.ChoiceBlock(ALIGN_CHOICES, label="zarovnání", default=LEFT)

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        if "css_class" not in context:
            context["css_class"] = []
        context["css_class"] += ALIGN_CSS[value["align"]]
        return context


class CTAMixin(blocks.StructBlock):
    button_link = blocks.URLBlock(label="Odkaz tlačítka")
    button_text = blocks.CharBlock(label="Text tlačítka")

    class Meta:
        icon = "doc-empty"
        label = "Tlačítko s odkazem"


class PersonCustomPositionBlockMixin(blocks.StructBlock):
    page = blocks.PageChooserBlock(label="Detail osoby")
    position = blocks.CharBlock(
        label="Pozice",
        help_text="Pokud není pozice vyplněná, použije se pozice ze stránky osoby.",
        required=False,
    )
