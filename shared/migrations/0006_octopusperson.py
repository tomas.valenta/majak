# Generated by Django 5.0.7 on 2024-07-29 20:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("shared", "0005_auto_20240513_0955"),
        ("wagtailimages", "0026_delete_uploadedimage"),
    ]

    operations = [
        migrations.CreateModel(
            name="OctopusPerson",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "email",
                    models.EmailField(max_length=254, verbose_name="Veřejný email"),
                ),
                ("phone", models.CharField(verbose_name="Telefonní číslo")),
                (
                    "facebook_url",
                    models.CharField(
                        blank=True, null=True, verbose_name="Facebook URL"
                    ),
                ),
                (
                    "flickr_url",
                    models.CharField(blank=True, null=True, verbose_name="Flickr URL"),
                ),
                (
                    "instagram_url",
                    models.CharField(
                        blank=True, null=True, verbose_name="Instagram URL"
                    ),
                ),
                (
                    "mastodon_url",
                    models.CharField(
                        blank=True, null=True, verbose_name="Mastodon URL"
                    ),
                ),
                (
                    "twitter_url",
                    models.CharField(blank=True, null=True, verbose_name="Twitter URL"),
                ),
                (
                    "tiktok_url",
                    models.CharField(blank=True, null=True, verbose_name="TikTok URL"),
                ),
                (
                    "web_url",
                    models.CharField(blank=True, null=True, verbose_name="Web URL"),
                ),
                (
                    "youtube_url",
                    models.CharField(blank=True, null=True, verbose_name="YouTube URL"),
                ),
                (
                    "more_info_url",
                    models.URLField(
                        blank=True, null=True, verbose_name="URL profilu v Chobotnici"
                    ),
                ),
                (
                    "profile_type",
                    models.CharField(
                        choices=[("political", "Politický"), ("pirate", "Pirátský")],
                        max_length=9,
                        verbose_name="Typ medailonku",
                    ),
                ),
                (
                    "short_text",
                    models.TextField(
                        blank=True, null=True, verbose_name="Krátký popis"
                    ),
                ),
                (
                    "long_text",
                    models.TextField(
                        blank=True, null=True, verbose_name="Dlouhý popis"
                    ),
                ),
                (
                    "degree_before",
                    models.CharField(max_length=64, verbose_name="Titul (před)"),
                ),
                (
                    "degree_after",
                    models.CharField(max_length=64, verbose_name="Titul (za)"),
                ),
                (
                    "display_name",
                    models.CharField(max_length=128, verbose_name="Zobrazované jméno"),
                ),
                (
                    "photo",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="wagtailimages.image",
                        verbose_name="Profilový obrázek",
                    ),
                ),
            ],
        ),
    ]
