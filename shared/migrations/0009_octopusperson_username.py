# Generated by Django 5.0.7 on 2024-07-30 10:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("shared", "0008_alter_octopusperson_degree_after_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="octopusperson",
            name="username",
            field=models.CharField(default="", max_length=128, verbose_name="Username"),
            preserve_default=False,
        ),
    ]
