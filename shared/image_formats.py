from wagtail.images.formats import Format, register_image_format

register_image_format(
    Format(
        "tailwind_center_800",
        "šablona: Uprostřed (max 800px)",
        "w-full mx-auto my-6 object-contain h-max lg:h-max lg:w-auto",
        "max-800x800",
    )
)

register_image_format(
    Format(
        "tailwind_center_400",
        "šablona: Uprostřed (max 400px)",
        "w-full mx-auto my-6 object-contain h-max lg:h-max lg:w-auto",
        "max-400x400",
    )
)

register_image_format(
    Format(
        "tailwind_center_200",
        "šablona: Uprostřed (max 200px)",
        "w-full mx-auto my-6 object-contain h-max lg:h-max lg:w-auto",
        "max-200x200",
    )
)

register_image_format(
    Format(
        "tailwind_left_400",
        "šablona: Vlevo (max 400px)",
        "float-left mr-4 mb-6 max-w-[400px] mt-0 sm:max-w-full object-contain",
        "max-400x400",
    )
)

register_image_format(
    Format(
        "tailwind_left_200",
        "šablona: Vlevo (max 200px)",
        "float-left mr-4 mb-6 mt-0 object-contain",
        "max-200x200",
    )
)

register_image_format(
    Format(
        "tailwind_right_400",
        "šablona: Vpravo (max 400px)",
        "float-right ml-4 mb-6 max-w-[400px] mt-0 sm:max-w-full object-contain",
        "max-400x400",
    )
)

register_image_format(
    Format(
        "tailwind_right_200",
        "šablona: Vpravo (max 200px)",
        "float-right ml-4 mb-6 mt-0 object-contain",
        "max-200x200",
    )
)
