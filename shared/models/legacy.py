import logging
from urllib.parse import quote

from django.db import models
from django.utils import timezone
from modelcluster.fields import ParentalManyToManyField
from wagtail.admin.panels import FieldPanel, PublishingPanel
from wagtail.fields import StreamField
from wagtail.models import Page
from wagtail.search import index

from shared.blocks import DEFAULT_CONTENT_BLOCKS

from .main import ArticlesMixin

logger = logging.getLogger(__name__)


# --- BEGIN Migrated models ---


class ArticleMixin(models.Model):
    """
    Common fields for articles.

    Must be used in class definition before MetadataPageMixin!

    If you want to tag articles, add tags as `tags` field in article page model.
    """

    ### FIELDS

    content = StreamField(
        DEFAULT_CONTENT_BLOCKS,
        verbose_name="Článek",
        blank=True,
        use_json_field=True,
    )
    timestamp = models.DateTimeField("Datum a čas", default=timezone.now)
    perex = models.TextField("perex")
    author = models.CharField("autor", max_length=250, blank=True, null=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="obrázek",
    )
    """
    Hidden field describing the source of shared articles, can be of values "district", "uniweb", "elections"
    or "main", depending on from which type of site this article was shared from
    """
    shared_type = models.TextField(null=True, blank=True)
    """
    Hidden field which links to a Page model of ArticlesMixin page to which this article was shared.
    Example: DistrictArticlesPage has shared tag "main", which this article shares as well -> shared_from will contain a reference to DistrictArticlesPage
    """
    shared_from = models.ForeignKey(
        Page,
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.PROTECT,
    )

    search_fields = Page.search_fields + [
        index.SearchField("title"),
        index.SearchField("author"),
        index.SearchField("perex"),
        index.SearchField("content"),
    ]

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("timestamp"),
        FieldPanel("perex"),
        FieldPanel("content"),
        FieldPanel("author"),
        FieldPanel("image"),
    ]

    settings_panels = [PublishingPanel()]

    class Meta:
        abstract = True

    @property
    def get_original_url(self):
        return self.full_url

    @property
    def get_no_index(self):
        """
        Indicates that a link to self should contain rel="noindex"
        """
        return self.shared_from is not None

    @property
    def get_rel(self):
        """
        Returns "rel" property for a link to this article
        """
        return "rel=noindex" if self.get_no_index else ""

    @property
    def date(self):
        """
        Returns the date of this article's timestamp.
        """

        return self.timestamp.date()

    def get_url(self, request=None):
        # 'request' kwarg for Wagtail compatibility

        if self.shared_from is not None:
            return f"{self.shared_from.url}sdilene?sdilene={quote(self.slug)}"
        return self.url

    def get_full_url(self, request=None):
        # 'request' kwarg for Wagtail compatibility

        if self.shared_from is not None:
            return f"{self.shared_from.full_url}sdilene?sdilene={quote(self.slug)}"
        return self.full_url

    @property
    def articles_page(self):
        """
        Returns articles page on which this article is displayed
        """
        return (
            self.shared_from.get_specific()
            if self.shared_from
            else self.get_parent().get_specific()
        )

    @property
    def root_page(self):
        """
        Returns root page of article, or a root page of Articles page to which this article was shared
        """
        if self.shared_from is None:
            return self.get_parent().get_ancestors().specific().live().last()

        return self.shared_from.get_specific().root_page

    @property
    def get_tags(self):
        """
        Returns all tags, including tags of shared articles from another site
        """
        if self.shared_from is not None:
            return self.articles_page.search_tags_by_unioned_id_query([self])
        return self.tags.all()

    @classmethod
    def has_tags(cls):
        try:
            cls._meta.get_field("tags")
        except models.FieldDoesNotExist:
            return False
        return True

    def get_meta_image(self):
        if hasattr(self, "search_image") and self.search_image:
            return self.search_image
        return self.image

    def get_meta_description(self):
        if hasattr(self, "search_description") and self.search_description:
            return self.search_description
        return self.perex


class ArticlesPageMixin(ArticlesMixin, models.Model):
    def get_shared_tags(self):
        """
        Overrides get_shared_tags from ArticlesMixin, returns shared tags
        """
        return self.shared_tags

    shared_tags = ParentalManyToManyField(
        "shared.SharedTag",
        verbose_name="Výběr tagů pro články sdílené mezi sítěmi",
        help_text="Pro výběr jednoho tagu klikněte na tag a uložte nebo publikujte stránku. Pro výběr více tagů využijte podržte Ctrl a vyberte příslušné tagy.",
        blank=True,
    )

    content_panels = Page.content_panels + [FieldPanel("shared_tags")]

    class Meta:
        abstract = True


# --- END Migrated models ---
