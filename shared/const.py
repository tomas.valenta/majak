BLACK_ON_WHITE = "black_on_white"
WHITE_ON_BLACK = "white_on_black"
WHITE_ON_BLUE = "white_on_blue"
WHITE_ON_CYAN = "white_on_cyan"
WHITE_ON_VIOLET = "white_on_violet"
BLACK_ON_YELLOW = "black_on_yellow"

LEFT = "left"
CENTER = "center"
RIGHT = "right"

RICH_TEXT_DEFAULT_FEATURES = [
    "h2",
    "h3",
    "h4",
    "h5",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
    "superscript",
    "subscript",
    "strikethrough",
    "blockquote",
    "embed",
]

MONTH_NAMES = [
    "Leden",
    "Únor",
    "Březen",
    "Duben",
    "Květen",
    "Červen",
    "Červenec",
    "Srpen",
    "Září",
    "Říjen",
    "Listopad",
    "Prosinec",
]

COLOR_CHOICES = (
    (BLACK_ON_WHITE, "černá na bílé"),
    (BLACK_ON_YELLOW, "černá na žluté"),
    (WHITE_ON_BLACK, "bílá na černé"),
    (WHITE_ON_BLUE, "bílá na modré"),
    (WHITE_ON_CYAN, "bílá na tyrkysové"),
    (WHITE_ON_VIOLET, "bílá na fialové"),
)

# TODO
COLOR_CSS = {
    BLACK_ON_WHITE: [
        "text-black",
        "[&_p]:!text-black",
        "[&_h1]:!text-black",
        "[&_h2]:!text-black",
        "[&_h3]:!text-black",
        "[&_h4]:!text-black",
        "[&_h5]:!text-black",
        "[&_h6]:!text-black",
        "bg-white",
    ],
    BLACK_ON_YELLOW: [
        "text-black",
        "[&_p]:!text-black",
        "[&_h1]:!text-black",
        "[&_h2]:!text-black",
        "[&_h3]:!text-black",
        "[&_h4]:!text-black",
        "[&_h5]:!text-black",
        "[&_h6]:!text-black",
        "bg-pirati-yellow",
    ],
    WHITE_ON_BLACK: [
        "text-white",
        "[&_p]:!text-white",
        "[&_h1]:!text-white",
        "[&_h2]:!text-white",
        "[&_h3]:!text-white",
        "[&_h4]:!text-white",
        "[&_h5]:!text-white",
        "[&_h6]:!text-white",
        "bg-black",
    ],
}

ALIGN_CHOICES = (
    (LEFT, "vlevo"),
    (CENTER, "uprostřed"),
    (RIGHT, "vpravo"),
)

ALIGN_CSS = {
    LEFT: ["text-left", "w-full", "[&_*]:mr-auto"],
    CENTER: ["text-center", "w-full", "[&_*]:mx-auto"],
    RIGHT: ["text-right", "w-full", "[&_*]:ml-auto"],
}
