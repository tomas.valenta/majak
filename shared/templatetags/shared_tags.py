from django import template

register = template.Library()


@register.simple_tag
def get_pagination_range(current_page: int, all_page_range: range):
    surrounding_count = 2
    if current_page > surrounding_count:
        if current_page + surrounding_count <= all_page_range[-1]:
            return range(
                current_page - surrounding_count, current_page + surrounding_count + 1
            )
        else:
            return range(current_page - surrounding_count, all_page_range[-1] + 1)
    else:
        if current_page + surrounding_count <= all_page_range[-1]:
            return range(all_page_range[0], current_page + surrounding_count + 1)
        else:
            return range(all_page_range[0], all_page_range[-1] + 1)
