from django import template

register = template.Library()


@register.filter
def is_first_people_type(content, forloop):
    first_people_type_position = None

    for position, item in enumerate(content):
        if item.block_type in (
            "octopus_group",
            "octopus_team",
            "people_group",
            "team_group",
        ):
            if first_people_type_position is None:
                first_people_type_position = position
                break

    if (
        first_people_type_position is not None
        and forloop["counter0"] != first_people_type_position
    ):
        return True

    return False


@register.filter
def has_one_people_type(content):
    people_type_count = 0

    for item in content:
        if item.block_type in (
            "octopus_group",
            "octopus_team",
            "people_group",
            "team_group",
        ):
            people_type_count += 1

            if people_type_count > 1:
                return False

    # Make sure there is a least one item
    return people_type_count == 1
