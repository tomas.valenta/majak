import markdown as md
from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def markdown(value):
    """Prekonvertuje vstupni text na markdown, necekane"""
    return mark_safe(md.markdown(value))
