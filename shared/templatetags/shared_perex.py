from django.template import Library

register = Library()


@register.filter
def shorten_perex(perex) -> str:
    if len(perex) > 245:
        return perex[:245] + "..."

    return perex
