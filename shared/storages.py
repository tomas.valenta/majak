import os

from django.conf import settings
from django.core.files.storage import get_storage_class


class OverwriteStorage(get_storage_class()):
    def get_available_name(self, name, max_length):
        """
        Returns a filename that's free on the target storage system, and
        available for new content to be written to. This file storage solves overwrite
        on upload problem.

        Found at https://djangosnippets.org/snippets/976/
        """

        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))

        return name
