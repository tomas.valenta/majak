from django.db import models
from django.utils.translation import gettext_lazy
from wagtail import blocks
from wagtail.admin.panels import CommentPanel, FieldPanel, HelpPanel, MultiFieldPanel
from wagtail.fields import StreamField
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Page
from wagtailmetadata.models import MetadataPageMixin

from tuning import admin_help


class PersonBlock(blocks.StructBlock):
    name = blocks.CharBlock(label="jméno")
    district = blocks.CharBlock(label="obvod")
    info = blocks.CharBlock(label="info o nominaci")
    phone = blocks.CharBlock(label="telefon", required=False)
    email = blocks.EmailBlock(label="email", required=False)
    web = blocks.URLBlock(label="web", required=False)
    photo = ImageChooserBlock(label="fotka")

    class Meta:
        icon = "person"
        label = "osoba"


class SenateHomePage(MetadataPageMixin, Page):
    ### FIELDS

    senators = StreamField(
        [("item", PersonBlock())],
        verbose_name="naši senátoři",
        blank=True,
        use_json_field=True,
    )
    candidates = StreamField(
        [("item", PersonBlock())],
        verbose_name="kandidáti",
        blank=True,
        use_json_field=True,
    )
    candidates_second_round = StreamField(
        [("item", PersonBlock())],
        verbose_name="kandidáti do 2. kola",
        blank=True,
        use_json_field=True,
    )
    matomo_id = models.IntegerField(
        "Matomo ID pro sledování návštěvnosti", blank=True, null=True
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("senators"),
        FieldPanel("candidates"),
        FieldPanel("candidates_second_round"),
    ]

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(admin_help.build(admin_help.IMPORTANT_TITLE)),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    settings_panels = [FieldPanel("matomo_id"), CommentPanel()]

    ### RELATIONS

    subpage_types = []

    ### OTHERS

    class Meta:
        verbose_name = "Senátní rozcestník"
