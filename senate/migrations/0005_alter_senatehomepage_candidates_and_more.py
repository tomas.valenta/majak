# Generated by Django 4.1.5 on 2023-01-30 23:27

import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("senate", "0004_senatehomepage_candidates_second_round"),
    ]

    operations = [
        migrations.AlterField(
            model_name="senatehomepage",
            name="candidates",
            field=wagtail.fields.StreamField(
                [
                    (
                        "item",
                        wagtail.blocks.StructBlock(
                            [
                                ("name", wagtail.blocks.CharBlock(label="jméno")),
                                ("district", wagtail.blocks.CharBlock(label="obvod")),
                                (
                                    "info",
                                    wagtail.blocks.CharBlock(label="info o nominaci"),
                                ),
                                (
                                    "phone",
                                    wagtail.blocks.CharBlock(
                                        label="telefon", required=False
                                    ),
                                ),
                                (
                                    "email",
                                    wagtail.blocks.EmailBlock(
                                        label="email", required=False
                                    ),
                                ),
                                (
                                    "web",
                                    wagtail.blocks.URLBlock(
                                        label="web", required=False
                                    ),
                                ),
                                (
                                    "photo",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="fotka"
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="kandidáti",
            ),
        ),
        migrations.AlterField(
            model_name="senatehomepage",
            name="candidates_second_round",
            field=wagtail.fields.StreamField(
                [
                    (
                        "item",
                        wagtail.blocks.StructBlock(
                            [
                                ("name", wagtail.blocks.CharBlock(label="jméno")),
                                ("district", wagtail.blocks.CharBlock(label="obvod")),
                                (
                                    "info",
                                    wagtail.blocks.CharBlock(label="info o nominaci"),
                                ),
                                (
                                    "phone",
                                    wagtail.blocks.CharBlock(
                                        label="telefon", required=False
                                    ),
                                ),
                                (
                                    "email",
                                    wagtail.blocks.EmailBlock(
                                        label="email", required=False
                                    ),
                                ),
                                (
                                    "web",
                                    wagtail.blocks.URLBlock(
                                        label="web", required=False
                                    ),
                                ),
                                (
                                    "photo",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="fotka"
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="kandidáti do 2. kola",
            ),
        ),
        migrations.AlterField(
            model_name="senatehomepage",
            name="senators",
            field=wagtail.fields.StreamField(
                [
                    (
                        "item",
                        wagtail.blocks.StructBlock(
                            [
                                ("name", wagtail.blocks.CharBlock(label="jméno")),
                                ("district", wagtail.blocks.CharBlock(label="obvod")),
                                (
                                    "info",
                                    wagtail.blocks.CharBlock(label="info o nominaci"),
                                ),
                                (
                                    "phone",
                                    wagtail.blocks.CharBlock(
                                        label="telefon", required=False
                                    ),
                                ),
                                (
                                    "email",
                                    wagtail.blocks.EmailBlock(
                                        label="email", required=False
                                    ),
                                ),
                                (
                                    "web",
                                    wagtail.blocks.URLBlock(
                                        label="web", required=False
                                    ),
                                ),
                                (
                                    "photo",
                                    wagtail.images.blocks.ImageChooserBlock(
                                        label="fotka"
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="naši senátoři",
            ),
        ),
    ]
