from shared_legacy.const import RICH_TEXT_DEFAULT_FEATURES

# Select colors for rich text editors font color from style guide
font_colors = [
    ("Green", "#4ca971"),
    ("Yellow", "#fde119"),
    ("Blue", "#027da8"),
    ("Cyan", "#004958"),
    ("Violet", "#670047"),
    ("Red", "#d60d53"),
    ("Grey", "#262626"),
]

RICH_TEXT_FEATURES = RICH_TEXT_DEFAULT_FEATURES + list(
    map(lambda color: "font_color_" + color[1], font_colors)
)
