from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def get_section_kind(context):
    if "current_section" not in context or context["current_section"] == "primary":
        context["current_section"] = "alternate"
    else:
        context["current_section"] = "primary"

    return context["current_section"]
