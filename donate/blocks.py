from wagtail.blocks import (
    CharBlock,
    DateBlock,
    IntegerBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StructBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from donate.constants import RICH_TEXT_FEATURES
from shared_legacy.blocks import MenuItemBlock as MenuItemBlockBase


class MenuItemBlock(MenuItemBlockBase):
    class Meta:
        label = "Položka v menu"
        template = "donate/blocks/menu_item.html"


class MenuParentBlock(StructBlock):
    title = CharBlock(label="Titulek", required=True)
    menu_items = ListBlock(MenuItemBlock(), label="Položky menu")

    class Meta:
        label = "Podmenu"
        template = "donate/blocks/navbar_submenu.html"


class CustomLinkBlock(StructBlock):
    text = CharBlock(label="Nadpis")
    url = URLBlock(label="Odkaz")

    class Meta:
        label = "Extra odkaz"


class ProjectIndexBlock(StructBlock):
    page = PageChooserBlock(
        label="Stránka se seznamem",
        page_type="donate.DonateProjectIndexPage",
    )

    class Meta:
        template = "donate/blocks/project_index_block.html"
        label = "Seznam projektů"


class DistrictDonationBlock(StructBlock):
    heading = CharBlock(label="Nadpis")
    description = RichTextBlock(label="Obsah", features=RICH_TEXT_FEATURES)

    class Meta:
        template = "donate/blocks/district_donation_block.html"
        label = "Podpora krajů"


class PartySupportFormBlock(StructBlock):
    heading = CharBlock(label="Nadpis")
    description = RichTextBlock(label="Obsah", features=RICH_TEXT_FEATURES)
    image = ImageChooserBlock(label="Obrázek")

    class Meta:
        template = "donate/blocks/party_support_form_block.html"
        label = "Podpoř stranu"


class CrowdfundingRewardBlock(StructBlock):
    title = CharBlock(label="Název odměny")
    description = CharBlock(label="Popis", max_length=255, required=False)
    amount = IntegerBlock(label="Částka")
    image = ImageChooserBlock(label="Obrázek")
    delivery_date = DateBlock(label="Datum dodání", required=False)
    reward_id = IntegerBlock(
        label="ID odměny",
        required=True,
    )
    variant_list = ListBlock(
        CharBlock(label="Varianta", max_length=12, help_text="Například velikost: S"),
        label="Varianty",
    )

    class Meta:
        template = "donate/blocks/crowdfunding_reward_block.html"
        icon = "pick"
        label = "Odměna"


class CustomContentBlock(StructBlock):
    title = CharBlock(label="Nadpis")
    content = RichTextBlock(label="Obsah", features=RICH_TEXT_FEATURES)
    link = URLBlock(label="Odkaz")

    class Meta:
        template = "donate/blocks/custom_content_block.html"
        icon = "doc-full"
        label = "Obecný blok"
