(function ($) {

  // Initialization
  "use strict";
  var nav = $('nav');
  var navHeight = nav.outerHeight();

  // Fancybox
  $('[data-fancybox="gallery"]').fancybox({
    buttons: [
      // "zoom",
      //"share",
      //"slideShow",
      //"fullScreen",
      //"download",
      // "thumbs",
      "close"
    ],
  });


  // Scroll function
  $('a.js-scroll-anchor[href*="#"]:not([href="#"])').on("click", function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - navHeight + 5)
        }, 1000);
        return false;
      }
    }
  });


  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-anchor').on("click", function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNavigation',
    offset: navHeight + 200
  });
  /*--/ End Scrolling nav /--*/

  $(window).trigger('scroll');

  /* DONATE FORM */

  // On amount radio change
  $("input[name='amount']").change(function() {
    const $customAmount = $('#customamount')
    $customAmount.attr('required', false);
    $customAmount.val("");
  });

  $("input[name='custom_amount']").focus(function() {
    $('#customamount').attr('required', true);
    $('input[name=amount]:checked', '#js-donate-form').prop('checked', false);
  })

  // Function for handling switching between monthly and one-time prices
  $("input[name='periodicity']").change(function () {

    // periodicity 730 === monthly
    if ($(this).attr('value') === '730') {
      const $monthlyAmounts = $('#monthlyAmounts')

      const selectRadio = $monthlyAmounts.data('default-selected')
      $('input[name=amount]:checked', '#js-donate-form').prop('checked', false);
      $(`#monthlyAmounts input[name=amount]:eq("${selectRadio - 1}")`).prop('checked', true);

      $monthlyAmounts.show();
      $('#oneTimeAmounts').hide();
    } else {
      const $oneTimeAmounts = $('#oneTimeAmounts')

      const selectRadio = $oneTimeAmounts.data('default-selected')
      $('input[name=amount]:checked', '#js-donate-form').prop('checked', false);
      $(`#oneTimeAmounts input[name=amount]:eq("${selectRadio - 1}")`).prop('checked', true);

      $oneTimeAmounts.show();
      $('#monthlyAmounts').hide();
    }
  });

  // Trigger correct state onload
  $("input[name='amount']:checked").change();

  // Update sharing link on form change for target donations

  function updateLink(projectId) {
    var url = new URL(location.href);
    url.searchParams.set("p", projectId);
    console.log(url.href);
    $("#target_sharing_link").attr("href", url.href);
  }

  $("input:radio.target-radio").change(function () {
    if ($(this).is(":checked")) {
      if ($(this).val() == -1) {
        updateLink($("#other_target_select").val());
      } else {
        updateLink($(this).val());
      }
    }
  });

  $("#other_target_select").change(function () {
    if ($(this).val() !== "") {
      $("#other_target").prop("checked", true);
      updateLink($(this).val());
    }
  });

})(jQuery);
