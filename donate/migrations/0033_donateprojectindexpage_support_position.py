# Generated by Django 4.1.10 on 2023-09-13 20:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("donate", "0032_remove_donateprojectindexpage_support_position"),
    ]

    operations = [
        migrations.AddField(
            model_name="donateprojectindexpage",
            name="support_position",
            field=models.IntegerField(
                default=0,
                help_text="Čím nižší číslo, tím výš se seznam projektů zobrazí.",
                verbose_name="Podpoř projekt pozice",
            ),
        ),
    ]
