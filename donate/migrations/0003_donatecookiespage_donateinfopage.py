# Generated by Django 3.0.6 on 2020-06-02 17:04

import django.db.models.deletion
import wagtail.fields
import wagtailmetadata.models
from django.db import migrations, models

import donate.models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailcore", "0045_assign_unlock_grouppagepermission"),
        ("wagtailimages", "0022_uploadedimage"),
        ("donate", "0002_auto_20200601_2324"),
    ]

    operations = [
        migrations.CreateModel(
            name="DonateInfoPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                (
                    "body",
                    wagtail.fields.RichTextField(blank=True, verbose_name="obsah"),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.Image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Info",
            },
            bases=(
                "wagtailcore.page",
                donate.models.SubpageMixin,
                wagtailmetadata.models.MetadataMixin,
                models.Model,
            ),
        ),
        migrations.CreateModel(
            name="DonateCookiesPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                (
                    "body",
                    wagtail.fields.RichTextField(blank=True, verbose_name="obsah"),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.Image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Cookies",
            },
            bases=(
                "wagtailcore.page",
                donate.models.SubpageMixin,
                wagtailmetadata.models.MetadataMixin,
                models.Model,
            ),
        ),
    ]
