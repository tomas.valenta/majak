# Generated by Django 4.1.8 on 2023-06-13 22:19

import wagtail.blocks
import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("donate", "0026_donateprojectpage_until"),
    ]

    operations = [
        migrations.AddField(
            model_name="donatehomepage",
            name="custom_blocks",
            field=wagtail.fields.StreamField(
                [
                    (
                        "content",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Nadpis")),
                                (
                                    "content",
                                    wagtail.blocks.RichTextBlock(label="Obsah"),
                                ),
                                ("link", wagtail.blocks.URLBlock(label="Odkaz")),
                            ]
                        ),
                    )
                ],
                blank=True,
                use_json_field=True,
                verbose_name="Obecné bloky",
            ),
        ),
    ]
