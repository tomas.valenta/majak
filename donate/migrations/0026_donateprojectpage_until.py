# Generated by Django 4.1.8 on 2023-05-30 20:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("donate", "0025_alter_donateprojectpage_crowdfunding_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="donateprojectpage",
            name="until",
            field=models.DateField(blank=True, null=True, verbose_name="běží do"),
        ),
    ]
