# Generated by Django 3.0.6 on 2020-05-27 18:04

import django.db.models.deletion
import wagtail.fields
import wagtail.images.blocks
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("wagtailcore", "0045_assign_unlock_grouppagepermission"),
        ("wagtailimages", "0022_uploadedimage"),
    ]

    operations = [
        migrations.CreateModel(
            name="DonateProjectIndexPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
            ],
            options={
                "verbose_name": "Přehled projektů",
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="DonateRegionIndexPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
            ],
            options={
                "verbose_name": "Přehled krajů",
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="DonateRegionPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                (
                    "perex",
                    models.TextField(verbose_name="krátký popis do přehledu krajů"),
                ),
                (
                    "main_title",
                    models.CharField(
                        max_length=250, verbose_name="hlavní nadpis na stránce"
                    ),
                ),
                ("body", wagtail.fields.RichTextField(verbose_name="obsah")),
            ],
            options={
                "verbose_name": "Kraj",
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="DonateProjectPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                ("date", models.DateField(verbose_name="běží od")),
                ("perex", models.TextField(verbose_name="krátký popis")),
                ("body", wagtail.fields.RichTextField(verbose_name="obsah")),
                (
                    "is_new",
                    models.BooleanField(
                        default=False, verbose_name='označení "nový projekt"'
                    ),
                ),
                (
                    "gallery",
                    wagtail.fields.StreamField(
                        [
                            (
                                "photo",
                                wagtail.images.blocks.ImageChooserBlock(label="fotka"),
                            )
                        ],
                        blank=True,
                        verbose_name="galerie fotek",
                    ),
                ),
                (
                    "photo",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="wagtailimages.Image",
                        verbose_name="fotka",
                    ),
                ),
            ],
            options={
                "verbose_name": "Projekt",
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="DonateHomePage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                (
                    "lead_title",
                    models.CharField(
                        blank=True, max_length=250, verbose_name="hlavní nadpis"
                    ),
                ),
                (
                    "lead_body",
                    models.TextField(blank=True, verbose_name="hlavní popis"),
                ),
                (
                    "lead_video",
                    models.URLField(
                        blank=True, null=True, verbose_name="video na youtube"
                    ),
                ),
                (
                    "support_title",
                    models.CharField(
                        blank=True, max_length=250, verbose_name="podpoř stranu nadpis"
                    ),
                ),
                (
                    "support_body",
                    models.TextField(blank=True, verbose_name="podpoř stranu popis"),
                ),
                (
                    "project_title",
                    models.CharField(
                        blank=True, max_length=250, verbose_name="podpoř projekt nadpis"
                    ),
                ),
                (
                    "project_body",
                    models.TextField(blank=True, verbose_name="podpoř projekt popis"),
                ),
                (
                    "region_title",
                    models.CharField(
                        blank=True, max_length=250, verbose_name="podpoř kraj nadpis"
                    ),
                ),
                (
                    "region_body",
                    models.TextField(blank=True, verbose_name="podpoř kraj popis"),
                ),
                (
                    "facebook",
                    models.URLField(blank=True, null=True, verbose_name="Facebook URL"),
                ),
                (
                    "instagram",
                    models.URLField(
                        blank=True, null=True, verbose_name="Instagram URL"
                    ),
                ),
                (
                    "twitter",
                    models.URLField(blank=True, null=True, verbose_name="Twitter URL"),
                ),
                (
                    "flickr",
                    models.URLField(blank=True, null=True, verbose_name="Flickr URL"),
                ),
                (
                    "matomo_id",
                    models.IntegerField(
                        blank=True,
                        null=True,
                        verbose_name="Matomo ID pro sledování návštěvnosti",
                    ),
                ),
                (
                    "lead_preview",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="wagtailimages.Image",
                        verbose_name="náhled videa",
                    ),
                ),
            ],
            options={
                "verbose_name": "Dary",
            },
            bases=("wagtailcore.page",),
        ),
    ]
