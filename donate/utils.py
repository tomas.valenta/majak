import logging
import os.path

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


def get_donated_amount_from_api(portal_project_id):
    url = os.path.join(
        settings.DONATE_PORTAL_API_URL, "donate/bar", str(portal_project_id)
    )

    try:
        response = requests.get(url, timeout=settings.DONATE_PORTAL_API_TIMEOUT)
        response.raise_for_status()
        _, amount = response.json()
        return amount

    except requests.exceptions.Timeout:
        logger.warning("Donate portal API call timed out for %s", url)

    except requests.exceptions.RequestException:
        logger.warning("Donate portal API call failed for %s", url)

    return None
