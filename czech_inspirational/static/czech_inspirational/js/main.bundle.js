(function (Vue) {
  'use strict';

  function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

  var Vue__default = /*#__PURE__*/_interopDefaultLegacy(Vue);

  var screens = {
    'sm': '576px',
    'md': '768px',
    'lg': '992px',
    'xl': '1200px',
    '2xl': '1366px',
  };

  const lgScreenSize = parseInt(screens.lg.replace("px", ""), 10);

  const forEachNode = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
      callback.call(scope, array[i]); // passes back stuff we need
    }
  };

  function getWindowWidth() {
    return Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  }

  function isLgScreenSize() {
    return getWindowWidth() >= lgScreenSize;
  }

  //

  var script = {
    data() {
      return {
        show: false
      };
    },
    props: {
      href: {
        type: String,
      },
      label: {
        type: String,
      }
    },
    methods: {
      onMouseEnter() {
        if (isLgScreenSize()) {
          this.$data.show = true;
        }
      },
      onMouseLeave() {
        if (isLgScreenSize()) {
          this.$data.show = false;
        }
      },
      handleClick(evt) {
        // On mobile screens, first click should just toggle and redir on second one
        if (isLgScreenSize() || this.$data.show) {
          if (this.$props.href) {
            window.location = this.$props.href;
          }
        }

        this.$data.show = !this.$data.show;
      }
    }
  };

  function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
          createInjectorSSR = createInjector;
          createInjector = shadowMode;
          shadowMode = false;
      }
      // Vue.extend constructor export interop.
      const options = typeof script === 'function' ? script.options : script;
      // render functions
      if (template && template.render) {
          options.render = template.render;
          options.staticRenderFns = template.staticRenderFns;
          options._compiled = true;
          // functional template
          if (isFunctionalTemplate) {
              options.functional = true;
          }
      }
      // scopedId
      if (scopeId) {
          options._scopeId = scopeId;
      }
      let hook;
      if (moduleIdentifier) {
          // server build
          hook = function (context) {
              // 2.3 injection
              context =
                  context || // cached call
                      (this.$vnode && this.$vnode.ssrContext) || // stateful
                      (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
              // 2.2 with runInNewContext: true
              if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                  context = __VUE_SSR_CONTEXT__;
              }
              // inject component styles
              if (style) {
                  style.call(this, createInjectorSSR(context));
              }
              // register component module identifier for async chunk inference
              if (context && context._registeredComponents) {
                  context._registeredComponents.add(moduleIdentifier);
              }
          };
          // used by ssr in case component is cached and beforeCreate
          // never gets called
          options._ssrRegister = hook;
      }
      else if (style) {
          hook = shadowMode
              ? function (context) {
                  style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
              }
              : function (context) {
                  style.call(this, createInjector(context));
              };
      }
      if (hook) {
          if (options.functional) {
              // register for functional component in vue file
              const originalRender = options.render;
              options.render = function renderWithStyleInjection(h, context) {
                  hook.call(context);
                  return originalRender(h, context);
              };
          }
          else {
              // inject component registration as beforeCreate hook
              const existing = options.beforeCreate;
              options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
          }
      }
      return script;
  }

  /* script */
  const __vue_script__ = script;

  /* template */
  var __vue_render__ = function() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c(
      "div",
      { on: { mouseenter: _vm.onMouseEnter, mouseleave: _vm.onMouseLeave } },
      [
        !_vm.href
          ? _c(
              "span",
              {
                staticClass: "navbar-menu__link navbar-menu__submenu-toggle",
                class: { "navbar-menu__submenu-toggle--open": _vm.show },
                on: { click: _vm.handleClick }
              },
              [_vm._v(_vm._s(_vm.label))]
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.href
          ? _c(
              "a",
              {
                staticClass: "navbar-menu__link navbar-menu__submenu-toggle",
                class: { "navbar-menu__submenu-toggle--open": _vm.show },
                attrs: { href: _vm.href },
                on: {
                  click: function($event) {
                    $event.preventDefault();
                    return _vm.handleClick($event)
                  }
                }
              },
              [_vm._v(_vm._s(_vm.label))]
            )
          : _vm._e(),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "navbar-menu__submenu-wrap",
            class: { "navbar-menu__submenu-wrap--show": _vm.show }
          },
          [_vm._t("default")],
          2
        )
      ]
    )
  };
  var __vue_staticRenderFns__ = [];
  __vue_render__._withStripped = true;

    /* style */
    const __vue_inject_styles__ = undefined;
    /* scoped */
    const __vue_scope_id__ = undefined;
    /* module identifier */
    const __vue_module_identifier__ = undefined;
    /* functional template */
    const __vue_is_functional_template__ = false;
    /* style inject */

    /* style inject SSR */

    /* style inject shadow dom */



    const __vue_component__ = /*#__PURE__*/normalizeComponent(
      { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
      __vue_inject_styles__,
      __vue_script__,
      __vue_scope_id__,
      __vue_is_functional_template__,
      __vue_module_identifier__,
      false,
      undefined,
      undefined,
      undefined
    );

  var script$1 = {
    components: {
      UiNavbarSubitem: __vue_component__
    },
    data() {
      return {
        isLgScreenSize: isLgScreenSize(),
        show: false,
        resizeHandler: () => {
          this.$data.isLgScreenSize = isLgScreenSize();
        },
      };
    },
    mounted() {
      this.$nextTick(() => {
        window.addEventListener("resize", this.$data.resizeHandler);
      });
    },
    beforeDestroy() {
      window.removeEventListener("resize", this.$data.resizeHandler);
    }
  };

  /* script */
  const __vue_script__$1 = script$1;

  /* template */

    /* style */
    const __vue_inject_styles__$1 = undefined;
    /* scoped */
    const __vue_scope_id__$1 = undefined;
    /* module identifier */
    const __vue_module_identifier__$1 = undefined;
    /* functional template */
    const __vue_is_functional_template__$1 = undefined;
    /* style inject */

    /* style inject SSR */

    /* style inject shadow dom */



    const __vue_component__$1 = /*#__PURE__*/normalizeComponent(
      {},
      __vue_inject_styles__$1,
      __vue_script__$1,
      __vue_scope_id__$1,
      __vue_is_functional_template__$1,
      __vue_module_identifier__$1,
      false,
      undefined,
      undefined,
      undefined
    );

  //

  var script$2 = {
    data() {
      return {
        isLgScreenSize: isLgScreenSize(),
        show: false,
        resizeHandler: () => {
          this.$data.isLgScreenSize = isLgScreenSize();
        },
      };
    },
    props: {
      href: {
        type: String,
      },
      label: {
        type: String,
      },
      labelclass: {
        type: String,
      },
      wrapperclass: {
        type: String,
        default: "",
      },
      slotwrapperclass: {
        type: String,
        default: "",
      }
    },
    methods: {
      handleClick() {
        if (this.$props.href) {
          window.location = this.$props.href;
        }

        this.$data.show = !this.$data.show;
      }
    },
    mounted() {
      this.$nextTick(() => {
        window.addEventListener("resize", this.$data.resizeHandler);
      });
    },
    beforeDestroy() {
      window.removeEventListener("resize", this.$data.resizeHandler);
    }
  };

  /* script */
  const __vue_script__$2 = script$2;

  /* template */
  var __vue_render__$1 = function() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { class: [_vm.wrapperclass, "footer-collapsible"] }, [
      _c(
        "span",
        {
          staticClass: "text-xl uppercase text-white footer-collapsible__toggle",
          class: [
            _vm.labelclass,
            _vm.show ? "footer-collapsible__toggle--open" : ""
          ],
          on: { click: _vm.handleClick }
        },
        [_vm._v(_vm._s(_vm.label))]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.show || _vm.isLgScreenSize,
              expression: "show || isLgScreenSize"
            }
          ],
          class: [_vm.slotwrapperclass]
        },
        [_vm._t("default")],
        2
      )
    ])
  };
  var __vue_staticRenderFns__$1 = [];
  __vue_render__$1._withStripped = true;

    /* style */
    const __vue_inject_styles__$2 = undefined;
    /* scoped */
    const __vue_scope_id__$2 = undefined;
    /* module identifier */
    const __vue_module_identifier__$2 = undefined;
    /* functional template */
    const __vue_is_functional_template__$2 = false;
    /* style inject */

    /* style inject SSR */

    /* style inject shadow dom */



    const __vue_component__$2 = /*#__PURE__*/normalizeComponent(
      { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
      __vue_inject_styles__$2,
      __vue_script__$2,
      __vue_scope_id__$2,
      __vue_is_functional_template__$2,
      __vue_module_identifier__$2,
      false,
      undefined,
      undefined,
      undefined
    );

  var script$3 = {
    mounted() {
      console.log(`Mounted generic Vue app in ` , this.$el);
    }
  };

  /* script */
  const __vue_script__$3 = script$3;

  /* template */

    /* style */
    const __vue_inject_styles__$3 = undefined;
    /* scoped */
    const __vue_scope_id__$3 = undefined;
    /* module identifier */
    const __vue_module_identifier__$3 = undefined;
    /* functional template */
    const __vue_is_functional_template__$3 = undefined;
    /* style inject */

    /* style inject SSR */

    /* style inject shadow dom */



    const __vue_component__$3 = /*#__PURE__*/normalizeComponent(
      {},
      __vue_inject_styles__$3,
      __vue_script__$3,
      __vue_scope_id__$3,
      __vue_is_functional_template__$3,
      __vue_module_identifier__$3,
      false,
      undefined,
      undefined,
      undefined
    );

  // import FlipClock from "./components/FlipClock";


  // Vue.component("ui-calendar-renderer", Renderer);
  // Vue.component("ui-calendar-dummy-provider", DummyProvider);
  // Vue.component("ui-calendar-google-provider", GoogleProvider);
  // Vue.component("ui-region-map", RegionMap);
  // Vue.component("ui-view-provider", ViewProvider);
  Vue__default['default'].component("ui-navbar", __vue_component__$1);
  Vue__default['default'].component("ui-footer-collapsible", __vue_component__$2);


  const appFactory = (el, attrs) => {
    // Bootstrap Vue.js.
    new Vue__default['default']({
      el,
      components: {
        UiApp: __vue_component__$3
      }
    });
  };




  /**
   * Bootstrap Vue.js application at given Element instance.
   *
   * App properties are passed via data attributes, like:
   *
   * <div class="__vue-root" data-message="Hello" data-app="SomeApp"></div>
   *
   * @param {Element} el DOM Element
   */
  function renderVueAppElement(el) {
    const attrs = Object.assign({}, el.dataset);
    return appFactory(el);
  }


  function init(event) {
    // Initialize Vue.js apps.
    forEachNode(document.querySelectorAll('.__js-root'), renderVueAppElement);

  }

  document.addEventListener('DOMContentLoaded', init);

  var anchorPrehrat = document.getElementById('prehrat-video');

  if(anchorPrehrat !== null) {
   anchorPrehrat.addEventListener('click', function(e){
     e.preventDefault();
     var originalheight=this.offsetHeight;
     // console.log(this.parentNode);
     // this.children[0].style.display = 'none';
     // this.children[1].style.display = 'block';
     this.parentNode.classList.add("tha_videocont_activated");
     this.outerHTML=`
          <iframe style="width:100%; height:` + originalheight + `px;" src="https://www.youtube.com/embed/hhs-cWWrKoY?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          `;
   });
  }

  //uncomment for collapsible chapter list instead of link to separate page

  // var anchorShowFull = document.getElementById('showfulltable');

  // if(anchorShowFull !== null) {
   // anchorShowFull.addEventListener('click', function(e){
    // e.preventDefault();
    // var chaptertable = document.getElementById("chaptertable");
    // chaptertable.classList.remove("candidate-table--fadeout");
    // var secondchapterlist = document.getElementById("secondchapterlist");
    // secondchapterlist.classList.remove("hidden");
    // this.remove();
   // });
  // }




  function forceDownload(href) {
  	var anchor = document.createElement('a');
  	anchor.href = href;
  	anchor.download = href;
  	document.body.appendChild(anchor);
  	anchor.click();
  }
  function change_download_HTML(element,heading){
    var downloadheading = document.getElementById("downloadheading");
    downloadheading.innerHTML=heading;
    element.outerHTML=`
    <div class="text-center pb-2 md:pb-0 mb-28">
     <a class="dlhref md:mx-4" href="/downloads/Ebook_cesko_inspirativni.pdf"><button class="w-80 text-lg btn btn--hoveractive btn--to-grey-700 inline-block align-top mb-3">
      <div class="btn__body py-10 h-12">Stáhnout ebook</div>
     </button></a><!--
     --><a class="dlhref md:mx-4" href="/downloads/Ebook_cesko_inspirativni.epub"><button class="w-80 btn text-lg btn btn--cyan-200 btn--hoveractive inline-block align-top mb-3">
      <div class="btn__body py-10 h-12">Stáhnout ebook pro čtečky</div>
     </button></a>
    </div>
  `;
  }

  var savemailform = document.getElementById('savemailform');

  if(savemailform !== null) {
   savemailform.addEventListener('submit', function(e){
    e.preventDefault();
    change_download_HTML(this,"Email uložen,<br>Děkujeme");
   });
  }

  //opt to download without giving e-mail
  var NoMailLink = document.getElementById('nomail');

  if(NoMailLink !== null) {
   NoMailLink.addEventListener('click', function(e){
    e.preventDefault();
    change_download_HTML(savemailform,"Příjemné<br>čtení!");
   });
  }

  var hrefs = document.getElementsByClassName("dlhref");
  for (var i = 0; i < hrefs.length; i++) {
   hrefs.item(i).addEventListener('click', function(e){
    e.preventDefault();
    console.log(this.href);
    forceDownload(this.href);
    setTimeout(function(){
     window.location.href = "?diky";
    }, 1000);
   });
  }

}(Vue));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXMiOlsic2NyZWVucy5qcyIsInNvdXJjZS9qcy91dGlscy5qcyIsInNvdXJjZS9qcy9jb21wb25lbnRzL25hdmJhci9OYXZiYXJTdWJpdGVtLnZ1ZSIsIm5vZGVfbW9kdWxlcy92dWUtcnVudGltZS1oZWxwZXJzL2Rpc3Qvbm9ybWFsaXplLWNvbXBvbmVudC5tanMiLCJzb3VyY2UvanMvY29tcG9uZW50cy9uYXZiYXIvTmF2YmFyLnZ1ZSIsInNvdXJjZS9qcy9jb21wb25lbnRzL2Zvb3Rlci9Gb290ZXJDb2xsYXBzaWJsZS52dWUiLCJzb3VyY2UvanMvY29tcG9uZW50cy9VaUFwcC52dWUiLCJzb3VyY2UvanMvbWFpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcclxuICAnc20nOiAnNTc2cHgnLFxyXG4gICdtZCc6ICc3NjhweCcsXHJcbiAgJ2xnJzogJzk5MnB4JyxcclxuICAneGwnOiAnMTIwMHB4JyxcclxuICAnMnhsJzogJzEzNjZweCcsXHJcbn07XHJcbiIsImltcG9ydCBzY3JlZW5zIGZyb20gXCIuLi8uLi9zY3JlZW5zXCI7XHJcblxyXG5jb25zdCBsZ1NjcmVlblNpemUgPSBwYXJzZUludChzY3JlZW5zLmxnLnJlcGxhY2UoXCJweFwiLCBcIlwiKSwgMTApO1xyXG5cclxuZXhwb3J0IGNvbnN0IGZvckVhY2hOb2RlID0gZnVuY3Rpb24gKGFycmF5LCBjYWxsYmFjaywgc2NvcGUpIHtcclxuICBmb3IgKHZhciBpID0gMDsgaSA8IGFycmF5Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICBjYWxsYmFjay5jYWxsKHNjb3BlLCBhcnJheVtpXSk7IC8vIHBhc3NlcyBiYWNrIHN0dWZmIHdlIG5lZWRcclxuICB9XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0V2luZG93V2lkdGgoKSB7XHJcbiAgcmV0dXJuIE1hdGgubWF4KGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aCB8fCAwLCB3aW5kb3cuaW5uZXJXaWR0aCB8fCAwKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGlzTGdTY3JlZW5TaXplKCkge1xyXG4gIHJldHVybiBnZXRXaW5kb3dXaWR0aCgpID49IGxnU2NyZWVuU2l6ZTtcclxufVxyXG4iLCI8dGVtcGxhdGU+XHJcbiAgPGRpdiBAbW91c2VlbnRlcj1cIm9uTW91c2VFbnRlclwiIEBtb3VzZWxlYXZlPVwib25Nb3VzZUxlYXZlXCI+XHJcbiAgICA8c3BhbiB2LWlmPVwiIWhyZWZcIiBjbGFzcz1cIm5hdmJhci1tZW51X19saW5rIG5hdmJhci1tZW51X19zdWJtZW51LXRvZ2dsZVwiIDpjbGFzcz1cInsnbmF2YmFyLW1lbnVfX3N1Ym1lbnUtdG9nZ2xlLS1vcGVuJzogc2hvd31cIiBAY2xpY2s9XCJoYW5kbGVDbGlja1wiPnt7IGxhYmVsIH19PC9zcGFuPlxyXG4gICAgPGEgdi1pZj1cImhyZWZcIiA6aHJlZj1cImhyZWZcIiBjbGFzcz1cIm5hdmJhci1tZW51X19saW5rIG5hdmJhci1tZW51X19zdWJtZW51LXRvZ2dsZVwiIDpjbGFzcz1cInsnbmF2YmFyLW1lbnVfX3N1Ym1lbnUtdG9nZ2xlLS1vcGVuJzogc2hvd31cIiBAY2xpY2sucHJldmVudD1cImhhbmRsZUNsaWNrXCI+e3sgbGFiZWwgfX08L2E+XHJcbiAgICA8ZGl2IDpjbGFzcz1cInsnbmF2YmFyLW1lbnVfX3N1Ym1lbnUtd3JhcC0tc2hvdyc6IHNob3d9XCIgY2xhc3M9XCJuYXZiYXItbWVudV9fc3VibWVudS13cmFwXCI+XHJcbiAgICAgIDxzbG90PlxyXG4gICAgICA8L3Nsb3Q+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmltcG9ydCB7IGlzTGdTY3JlZW5TaXplIH0gZnJvbSBcIi4uLy4uL3V0aWxzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgZGF0YSgpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHNob3c6IGZhbHNlXHJcbiAgICB9O1xyXG4gIH0sXHJcbiAgcHJvcHM6IHtcclxuICAgIGhyZWY6IHtcclxuICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgfSxcclxuICAgIGxhYmVsOiB7XHJcbiAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgIH1cclxuICB9LFxyXG4gIG1ldGhvZHM6IHtcclxuICAgIG9uTW91c2VFbnRlcigpIHtcclxuICAgICAgaWYgKGlzTGdTY3JlZW5TaXplKCkpIHtcclxuICAgICAgICB0aGlzLiRkYXRhLnNob3cgPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgb25Nb3VzZUxlYXZlKCkge1xyXG4gICAgICBpZiAoaXNMZ1NjcmVlblNpemUoKSkge1xyXG4gICAgICAgIHRoaXMuJGRhdGEuc2hvdyA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgaGFuZGxlQ2xpY2soZXZ0KSB7XHJcbiAgICAgIC8vIE9uIG1vYmlsZSBzY3JlZW5zLCBmaXJzdCBjbGljayBzaG91bGQganVzdCB0b2dnbGUgYW5kIHJlZGlyIG9uIHNlY29uZCBvbmVcclxuICAgICAgaWYgKGlzTGdTY3JlZW5TaXplKCkgfHwgdGhpcy4kZGF0YS5zaG93KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuJHByb3BzLmhyZWYpIHtcclxuICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbiA9IHRoaXMuJHByb3BzLmhyZWY7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLiRkYXRhLnNob3cgPSAhdGhpcy4kZGF0YS5zaG93O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG48L3NjcmlwdD5cclxuIiwiZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50KHRlbXBsYXRlLCBzdHlsZSwgc2NyaXB0LCBzY29wZUlkLCBpc0Z1bmN0aW9uYWxUZW1wbGF0ZSwgbW9kdWxlSWRlbnRpZmllciAvKiBzZXJ2ZXIgb25seSAqLywgc2hhZG93TW9kZSwgY3JlYXRlSW5qZWN0b3IsIGNyZWF0ZUluamVjdG9yU1NSLCBjcmVhdGVJbmplY3RvclNoYWRvdykge1xyXG4gICAgaWYgKHR5cGVvZiBzaGFkb3dNb2RlICE9PSAnYm9vbGVhbicpIHtcclxuICAgICAgICBjcmVhdGVJbmplY3RvclNTUiA9IGNyZWF0ZUluamVjdG9yO1xyXG4gICAgICAgIGNyZWF0ZUluamVjdG9yID0gc2hhZG93TW9kZTtcclxuICAgICAgICBzaGFkb3dNb2RlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICAvLyBWdWUuZXh0ZW5kIGNvbnN0cnVjdG9yIGV4cG9ydCBpbnRlcm9wLlxyXG4gICAgY29uc3Qgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHQgPT09ICdmdW5jdGlvbicgPyBzY3JpcHQub3B0aW9ucyA6IHNjcmlwdDtcclxuICAgIC8vIHJlbmRlciBmdW5jdGlvbnNcclxuICAgIGlmICh0ZW1wbGF0ZSAmJiB0ZW1wbGF0ZS5yZW5kZXIpIHtcclxuICAgICAgICBvcHRpb25zLnJlbmRlciA9IHRlbXBsYXRlLnJlbmRlcjtcclxuICAgICAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IHRlbXBsYXRlLnN0YXRpY1JlbmRlckZucztcclxuICAgICAgICBvcHRpb25zLl9jb21waWxlZCA9IHRydWU7XHJcbiAgICAgICAgLy8gZnVuY3Rpb25hbCB0ZW1wbGF0ZVxyXG4gICAgICAgIGlmIChpc0Z1bmN0aW9uYWxUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICBvcHRpb25zLmZ1bmN0aW9uYWwgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHNjb3BlZElkXHJcbiAgICBpZiAoc2NvcGVJZCkge1xyXG4gICAgICAgIG9wdGlvbnMuX3Njb3BlSWQgPSBzY29wZUlkO1xyXG4gICAgfVxyXG4gICAgbGV0IGhvb2s7XHJcbiAgICBpZiAobW9kdWxlSWRlbnRpZmllcikge1xyXG4gICAgICAgIC8vIHNlcnZlciBidWlsZFxyXG4gICAgICAgIGhvb2sgPSBmdW5jdGlvbiAoY29udGV4dCkge1xyXG4gICAgICAgICAgICAvLyAyLjMgaW5qZWN0aW9uXHJcbiAgICAgICAgICAgIGNvbnRleHQgPVxyXG4gICAgICAgICAgICAgICAgY29udGV4dCB8fCAvLyBjYWNoZWQgY2FsbFxyXG4gICAgICAgICAgICAgICAgICAgICh0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0KSB8fCAvLyBzdGF0ZWZ1bFxyXG4gICAgICAgICAgICAgICAgICAgICh0aGlzLnBhcmVudCAmJiB0aGlzLnBhcmVudC4kdm5vZGUgJiYgdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQpOyAvLyBmdW5jdGlvbmFsXHJcbiAgICAgICAgICAgIC8vIDIuMiB3aXRoIHJ1bkluTmV3Q29udGV4dDogdHJ1ZVxyXG4gICAgICAgICAgICBpZiAoIWNvbnRleHQgJiYgdHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX18gIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0ID0gX19WVUVfU1NSX0NPTlRFWFRfXztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBpbmplY3QgY29tcG9uZW50IHN0eWxlc1xyXG4gICAgICAgICAgICBpZiAoc3R5bGUpIHtcclxuICAgICAgICAgICAgICAgIHN0eWxlLmNhbGwodGhpcywgY3JlYXRlSW5qZWN0b3JTU1IoY29udGV4dCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHJlZ2lzdGVyIGNvbXBvbmVudCBtb2R1bGUgaWRlbnRpZmllciBmb3IgYXN5bmMgY2h1bmsgaW5mZXJlbmNlXHJcbiAgICAgICAgICAgIGlmIChjb250ZXh0ICYmIGNvbnRleHQuX3JlZ2lzdGVyZWRDb21wb25lbnRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQobW9kdWxlSWRlbnRpZmllcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIHVzZWQgYnkgc3NyIGluIGNhc2UgY29tcG9uZW50IGlzIGNhY2hlZCBhbmQgYmVmb3JlQ3JlYXRlXHJcbiAgICAgICAgLy8gbmV2ZXIgZ2V0cyBjYWxsZWRcclxuICAgICAgICBvcHRpb25zLl9zc3JSZWdpc3RlciA9IGhvb2s7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChzdHlsZSkge1xyXG4gICAgICAgIGhvb2sgPSBzaGFkb3dNb2RlXHJcbiAgICAgICAgICAgID8gZnVuY3Rpb24gKGNvbnRleHQpIHtcclxuICAgICAgICAgICAgICAgIHN0eWxlLmNhbGwodGhpcywgY3JlYXRlSW5qZWN0b3JTaGFkb3coY29udGV4dCwgdGhpcy4kcm9vdC4kb3B0aW9ucy5zaGFkb3dSb290KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgOiBmdW5jdGlvbiAoY29udGV4dCkge1xyXG4gICAgICAgICAgICAgICAgc3R5bGUuY2FsbCh0aGlzLCBjcmVhdGVJbmplY3Rvcihjb250ZXh0KSk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICB9XHJcbiAgICBpZiAoaG9vaykge1xyXG4gICAgICAgIGlmIChvcHRpb25zLmZ1bmN0aW9uYWwpIHtcclxuICAgICAgICAgICAgLy8gcmVnaXN0ZXIgZm9yIGZ1bmN0aW9uYWwgY29tcG9uZW50IGluIHZ1ZSBmaWxlXHJcbiAgICAgICAgICAgIGNvbnN0IG9yaWdpbmFsUmVuZGVyID0gb3B0aW9ucy5yZW5kZXI7XHJcbiAgICAgICAgICAgIG9wdGlvbnMucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyV2l0aFN0eWxlSW5qZWN0aW9uKGgsIGNvbnRleHQpIHtcclxuICAgICAgICAgICAgICAgIGhvb2suY2FsbChjb250ZXh0KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBvcmlnaW5hbFJlbmRlcihoLCBjb250ZXh0KTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIGluamVjdCBjb21wb25lbnQgcmVnaXN0cmF0aW9uIGFzIGJlZm9yZUNyZWF0ZSBob29rXHJcbiAgICAgICAgICAgIGNvbnN0IGV4aXN0aW5nID0gb3B0aW9ucy5iZWZvcmVDcmVhdGU7XHJcbiAgICAgICAgICAgIG9wdGlvbnMuYmVmb3JlQ3JlYXRlID0gZXhpc3RpbmcgPyBbXS5jb25jYXQoZXhpc3RpbmcsIGhvb2spIDogW2hvb2tdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBzY3JpcHQ7XHJcbn1cblxuZXhwb3J0IGRlZmF1bHQgbm9ybWFsaXplQ29tcG9uZW50O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9bm9ybWFsaXplLWNvbXBvbmVudC5tanMubWFwXG4iLCI8c2NyaXB0PlxyXG4gIGltcG9ydCBVaU5hdmJhclN1Yml0ZW0gZnJvbSBcIi4vTmF2YmFyU3ViaXRlbVwiO1xyXG4gIGltcG9ydCB7IGlzTGdTY3JlZW5TaXplIH0gZnJvbSBcIi4uLy4uL3V0aWxzXCI7XHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgVWlOYXZiYXJTdWJpdGVtXHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICBpc0xnU2NyZWVuU2l6ZTogaXNMZ1NjcmVlblNpemUoKSxcclxuICAgICAgICBzaG93OiBmYWxzZSxcclxuICAgICAgICByZXNpemVIYW5kbGVyOiAoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLiRkYXRhLmlzTGdTY3JlZW5TaXplID0gaXNMZ1NjcmVlblNpemUoKTtcclxuICAgICAgICB9LFxyXG4gICAgICB9O1xyXG4gICAgfSxcclxuICAgIG1vdW50ZWQoKSB7XHJcbiAgICAgIHRoaXMuJG5leHRUaWNrKCgpID0+IHtcclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLiRkYXRhLnJlc2l6ZUhhbmRsZXIpO1xyXG4gICAgICB9KTtcclxuICAgIH0sXHJcbiAgICBiZWZvcmVEZXN0cm95KCkge1xyXG4gICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCB0aGlzLiRkYXRhLnJlc2l6ZUhhbmRsZXIpO1xyXG4gICAgfVxyXG4gIH1cclxuPC9zY3JpcHQ+XHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IDpjbGFzcz1cIlt3cmFwcGVyY2xhc3MsICdmb290ZXItY29sbGFwc2libGUnXVwiPlxyXG4gICAgPHNwYW4gY2xhc3M9XCJ0ZXh0LXhsIHVwcGVyY2FzZSB0ZXh0LXdoaXRlIGZvb3Rlci1jb2xsYXBzaWJsZV9fdG9nZ2xlXCIgOmNsYXNzPVwiW2xhYmVsY2xhc3MsIHNob3cgPyAnZm9vdGVyLWNvbGxhcHNpYmxlX190b2dnbGUtLW9wZW4nIDogJyddXCIgQGNsaWNrPVwiaGFuZGxlQ2xpY2tcIj57eyBsYWJlbCB9fTwvc3Bhbj5cclxuICAgIDxkaXYgdi1zaG93PVwic2hvdyB8fCBpc0xnU2NyZWVuU2l6ZVwiIDpjbGFzcz1cIltzbG90d3JhcHBlcmNsYXNzXVwiPlxyXG4gICAgICA8c2xvdD5cclxuICAgICAgPC9zbG90PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG5pbXBvcnQgeyBpc0xnU2NyZWVuU2l6ZSB9IGZyb20gXCIuLi8uLi91dGlsc1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gIGRhdGEoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBpc0xnU2NyZWVuU2l6ZTogaXNMZ1NjcmVlblNpemUoKSxcclxuICAgICAgc2hvdzogZmFsc2UsXHJcbiAgICAgIHJlc2l6ZUhhbmRsZXI6ICgpID0+IHtcclxuICAgICAgICB0aGlzLiRkYXRhLmlzTGdTY3JlZW5TaXplID0gaXNMZ1NjcmVlblNpemUoKTtcclxuICAgICAgfSxcclxuICAgIH07XHJcbiAgfSxcclxuICBwcm9wczoge1xyXG4gICAgaHJlZjoge1xyXG4gICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICB9LFxyXG4gICAgbGFiZWw6IHtcclxuICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgfSxcclxuICAgIGxhYmVsY2xhc3M6IHtcclxuICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgfSxcclxuICAgIHdyYXBwZXJjbGFzczoge1xyXG4gICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgIGRlZmF1bHQ6IFwiXCIsXHJcbiAgICB9LFxyXG4gICAgc2xvdHdyYXBwZXJjbGFzczoge1xyXG4gICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgIGRlZmF1bHQ6IFwiXCIsXHJcbiAgICB9XHJcbiAgfSxcclxuICBtZXRob2RzOiB7XHJcbiAgICBoYW5kbGVDbGljaygpIHtcclxuICAgICAgaWYgKHRoaXMuJHByb3BzLmhyZWYpIHtcclxuICAgICAgICB3aW5kb3cubG9jYXRpb24gPSB0aGlzLiRwcm9wcy5ocmVmO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLiRkYXRhLnNob3cgPSAhdGhpcy4kZGF0YS5zaG93O1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbW91bnRlZCgpIHtcclxuICAgIHRoaXMuJG5leHRUaWNrKCgpID0+IHtcclxuICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgdGhpcy4kZGF0YS5yZXNpemVIYW5kbGVyKTtcclxuICAgIH0pO1xyXG4gIH0sXHJcbiAgYmVmb3JlRGVzdHJveSgpIHtcclxuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIHRoaXMuJGRhdGEucmVzaXplSGFuZGxlcik7XHJcbiAgfVxyXG59XHJcbjwvc2NyaXB0PlxyXG4iLCI8c2NyaXB0PlxyXG5leHBvcnQgZGVmYXVsdCB7XHJcbiAgbW91bnRlZCgpIHtcclxuICAgIGNvbnNvbGUubG9nKGBNb3VudGVkIGdlbmVyaWMgVnVlIGFwcCBpbiBgICwgdGhpcy4kZWwpO1xyXG4gIH1cclxufVxyXG48L3NjcmlwdD5cclxuIiwiaW1wb3J0IFZ1ZSBmcm9tIFwidnVlXCI7XHJcblxyXG5pbXBvcnQgeyBmb3JFYWNoTm9kZSB9IGZyb20gXCIuL3V0aWxzXCI7XHJcblxyXG4vLyBpbXBvcnQgUmVuZGVyZXIgZnJvbSBcIi4vY29tcG9uZW50cy9jYWxlbmRhci9SZW5kZXJlclwiO1xyXG4vLyBpbXBvcnQgRHVtbXlQcm92aWRlciBmcm9tIFwiLi9jb21wb25lbnRzL2NhbGVuZGFyL0R1bW15UHJvdmlkZXJcIjtcclxuLy8gaW1wb3J0IEdvb2dsZVByb3ZpZGVyIGZyb20gXCIuL2NvbXBvbmVudHMvY2FsZW5kYXIvR29vZ2xlUHJvdmlkZXJcIjtcclxuLy8gaW1wb3J0IFJlZ2lvbk1hcCBmcm9tIFwiLi9jb21wb25lbnRzL1JlZ2lvbk1hcFwiO1xyXG4vLyBpbXBvcnQgVmlld1Byb3ZpZGVyIGZyb20gXCIuL2NvbXBvbmVudHMvVmlld1Byb3ZpZGVyXCI7XHJcbmltcG9ydCBOYXZiYXIgZnJvbSBcIi4vY29tcG9uZW50cy9uYXZiYXIvTmF2YmFyXCI7XHJcbmltcG9ydCBGb290ZXJDb2xsYXBzaWJsZSBmcm9tIFwiLi9jb21wb25lbnRzL2Zvb3Rlci9Gb290ZXJDb2xsYXBzaWJsZVwiO1xyXG4vLyBpbXBvcnQgRmxpcENsb2NrIGZyb20gXCIuL2NvbXBvbmVudHMvRmxpcENsb2NrXCI7XHJcblxyXG5cclxuLy8gVnVlLmNvbXBvbmVudChcInVpLWNhbGVuZGFyLXJlbmRlcmVyXCIsIFJlbmRlcmVyKTtcclxuLy8gVnVlLmNvbXBvbmVudChcInVpLWNhbGVuZGFyLWR1bW15LXByb3ZpZGVyXCIsIER1bW15UHJvdmlkZXIpO1xyXG4vLyBWdWUuY29tcG9uZW50KFwidWktY2FsZW5kYXItZ29vZ2xlLXByb3ZpZGVyXCIsIEdvb2dsZVByb3ZpZGVyKTtcclxuLy8gVnVlLmNvbXBvbmVudChcInVpLXJlZ2lvbi1tYXBcIiwgUmVnaW9uTWFwKTtcclxuLy8gVnVlLmNvbXBvbmVudChcInVpLXZpZXctcHJvdmlkZXJcIiwgVmlld1Byb3ZpZGVyKTtcclxuVnVlLmNvbXBvbmVudChcInVpLW5hdmJhclwiLCBOYXZiYXIpO1xyXG5WdWUuY29tcG9uZW50KFwidWktZm9vdGVyLWNvbGxhcHNpYmxlXCIsIEZvb3RlckNvbGxhcHNpYmxlKTtcclxuLy8gVnVlLmNvbXBvbmVudChcInVpLWZsaXAtY2xvY2tcIiwgRmxpcENsb2NrKTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5pbXBvcnQgVWlBcHAgZnJvbSBcIi4vY29tcG9uZW50cy9VaUFwcC52dWVcIjtcclxuXHJcblxyXG5jb25zdCBhcHBGYWN0b3J5ID0gKGVsLCBhdHRycykgPT4ge1xyXG4gIC8vIEJvb3RzdHJhcCBWdWUuanMuXHJcbiAgbmV3IFZ1ZSh7XHJcbiAgICBlbCxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgVWlBcHBcclxuICAgIH1cclxuICB9KTtcclxufTtcclxuXHJcblxyXG5cclxuXHJcbi8qKlxyXG4gKiBCb290c3RyYXAgVnVlLmpzIGFwcGxpY2F0aW9uIGF0IGdpdmVuIEVsZW1lbnQgaW5zdGFuY2UuXHJcbiAqXHJcbiAqIEFwcCBwcm9wZXJ0aWVzIGFyZSBwYXNzZWQgdmlhIGRhdGEgYXR0cmlidXRlcywgbGlrZTpcclxuICpcclxuICogPGRpdiBjbGFzcz1cIl9fdnVlLXJvb3RcIiBkYXRhLW1lc3NhZ2U9XCJIZWxsb1wiIGRhdGEtYXBwPVwiU29tZUFwcFwiPjwvZGl2PlxyXG4gKlxyXG4gKiBAcGFyYW0ge0VsZW1lbnR9IGVsIERPTSBFbGVtZW50XHJcbiAqL1xyXG5mdW5jdGlvbiByZW5kZXJWdWVBcHBFbGVtZW50KGVsKSB7XHJcbiAgY29uc3QgYXR0cnMgPSBPYmplY3QuYXNzaWduKHt9LCBlbC5kYXRhc2V0KTtcclxuICByZXR1cm4gYXBwRmFjdG9yeShlbCwgYXR0cnMpO1xyXG59XHJcblxyXG5cclxuZnVuY3Rpb24gaW5pdChldmVudCkge1xyXG4gIC8vIEluaXRpYWxpemUgVnVlLmpzIGFwcHMuXHJcbiAgZm9yRWFjaE5vZGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLl9fanMtcm9vdCcpLCByZW5kZXJWdWVBcHBFbGVtZW50KTtcclxuICBcclxufVxyXG5cclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGluaXQpO1xyXG5cclxudmFyIGFuY2hvclByZWhyYXQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncHJlaHJhdC12aWRlbycpO1xyXG5cclxuaWYoYW5jaG9yUHJlaHJhdCAhPT0gbnVsbCkge1xyXG4gYW5jaG9yUHJlaHJhdC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKGUpe1xyXG4gICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgIHZhciBvcmlnaW5hbGhlaWdodD10aGlzLm9mZnNldEhlaWdodDtcclxuICAgLy8gY29uc29sZS5sb2codGhpcy5wYXJlbnROb2RlKTtcclxuICAgLy8gdGhpcy5jaGlsZHJlblswXS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAvLyB0aGlzLmNoaWxkcmVuWzFdLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xyXG4gICB0aGlzLnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZChcInRoYV92aWRlb2NvbnRfYWN0aXZhdGVkXCIpO1xyXG4gICB0aGlzLm91dGVySFRNTD1gXHJcbiAgICAgICAgICA8aWZyYW1lIHN0eWxlPVwid2lkdGg6MTAwJTsgaGVpZ2h0OmAgKyBvcmlnaW5hbGhlaWdodCArIGBweDtcIiBzcmM9XCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC9oaHMtY1dXcktvWT9hdXRvcGxheT0xXCIgZnJhbWVib3JkZXI9XCIwXCIgYWxsb3c9XCJhY2NlbGVyb21ldGVyOyBhdXRvcGxheTsgY2xpcGJvYXJkLXdyaXRlOyBlbmNyeXB0ZWQtbWVkaWE7IGd5cm9zY29wZTsgcGljdHVyZS1pbi1waWN0dXJlXCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPlxyXG4gICAgICAgICAgYDtcclxuIH0pO1xyXG59XHJcblxyXG4vL3VuY29tbWVudCBmb3IgY29sbGFwc2libGUgY2hhcHRlciBsaXN0IGluc3RlYWQgb2YgbGluayB0byBzZXBhcmF0ZSBwYWdlXHJcblxyXG4vLyB2YXIgYW5jaG9yU2hvd0Z1bGwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2hvd2Z1bGx0YWJsZScpO1xyXG5cclxuLy8gaWYoYW5jaG9yU2hvd0Z1bGwgIT09IG51bGwpIHtcclxuIC8vIGFuY2hvclNob3dGdWxsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XHJcbiAgLy8gZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gIC8vIHZhciBjaGFwdGVydGFibGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNoYXB0ZXJ0YWJsZVwiKTtcclxuICAvLyBjaGFwdGVydGFibGUuY2xhc3NMaXN0LnJlbW92ZShcImNhbmRpZGF0ZS10YWJsZS0tZmFkZW91dFwiKTtcclxuICAvLyB2YXIgc2Vjb25kY2hhcHRlcmxpc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNlY29uZGNoYXB0ZXJsaXN0XCIpO1xyXG4gIC8vIHNlY29uZGNoYXB0ZXJsaXN0LmNsYXNzTGlzdC5yZW1vdmUoXCJoaWRkZW5cIik7XHJcbiAgLy8gdGhpcy5yZW1vdmUoKTtcclxuIC8vIH0pO1xyXG4vLyB9XHJcblxyXG5cclxuXHJcblxyXG5mdW5jdGlvbiBmb3JjZURvd25sb2FkKGhyZWYpIHtcclxuXHR2YXIgYW5jaG9yID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG5cdGFuY2hvci5ocmVmID0gaHJlZjtcclxuXHRhbmNob3IuZG93bmxvYWQgPSBocmVmO1xyXG5cdGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoYW5jaG9yKTtcclxuXHRhbmNob3IuY2xpY2soKTtcclxufVxyXG5mdW5jdGlvbiBjaGFuZ2VfZG93bmxvYWRfSFRNTChlbGVtZW50LGhlYWRpbmcpe1xyXG4gIHZhciBkb3dubG9hZGhlYWRpbmcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRvd25sb2FkaGVhZGluZ1wiKTtcclxuICBkb3dubG9hZGhlYWRpbmcuaW5uZXJIVE1MPWhlYWRpbmc7XHJcbiAgZWxlbWVudC5vdXRlckhUTUw9YFxyXG4gICAgPGRpdiBjbGFzcz1cInRleHQtY2VudGVyIHBiLTIgbWQ6cGItMCBtYi0yOFwiPlxyXG4gICAgIDxhIGNsYXNzPVwiZGxocmVmIG1kOm14LTRcIiBocmVmPVwiL2Rvd25sb2Fkcy9FYm9va19jZXNrb19pbnNwaXJhdGl2bmkucGRmXCI+PGJ1dHRvbiBjbGFzcz1cInctODAgdGV4dC1sZyBidG4gYnRuLS1ob3ZlcmFjdGl2ZSBidG4tLXRvLWdyZXktNzAwIGlubGluZS1ibG9jayBhbGlnbi10b3AgbWItM1wiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiYnRuX19ib2R5IHB5LTEwIGgtMTJcIj5TdMOhaG5vdXQgZWJvb2s8L2Rpdj5cclxuICAgICA8L2J1dHRvbj48L2E+PCEtLVxyXG4gICAgIC0tPjxhIGNsYXNzPVwiZGxocmVmIG1kOm14LTRcIiBocmVmPVwiL2Rvd25sb2Fkcy9FYm9va19jZXNrb19pbnNwaXJhdGl2bmkuZXB1YlwiPjxidXR0b24gY2xhc3M9XCJ3LTgwIGJ0biB0ZXh0LWxnIGJ0biBidG4tLWN5YW4tMjAwIGJ0bi0taG92ZXJhY3RpdmUgaW5saW5lLWJsb2NrIGFsaWduLXRvcCBtYi0zXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJidG5fX2JvZHkgcHktMTAgaC0xMlwiPlN0w6Fobm91dCBlYm9vayBwcm8gxI10ZcSNa3k8L2Rpdj5cclxuICAgICA8L2J1dHRvbj48L2E+XHJcbiAgICA8L2Rpdj5cclxuICBgO1xyXG4gIHZhciBocmVmcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJkbGhyZWZcIik7XHJcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBocmVmcy5sZW5ndGg7IGkrKykge1xyXG4gICBocmVmcy5pdGVtKGkpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmhyZWYpO1xyXG4gICAgZm9yY2VEb3dubG9hZCh0aGlzLmhyZWYpO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gXCJ0aGFua3lvdS5odG1sXCI7XHJcbiAgICB9LCAxMDAwKTtcclxuICAgfSk7XHJcbiAgfSAgXHJcbn1cclxuXHJcbnZhciBzYXZlbWFpbGZvcm0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2F2ZW1haWxmb3JtJyk7XHJcblxyXG5pZihzYXZlbWFpbGZvcm0gIT09IG51bGwpIHtcclxuIHNhdmVtYWlsZm9ybS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCBmdW5jdGlvbihlKXtcclxuICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgY2hhbmdlX2Rvd25sb2FkX0hUTUwodGhpcyxcIkVtYWlsIHVsb8W+ZW4sPGJyPkTEm2t1amVtZVwiKTtcclxuIH0pO1xyXG59XHJcblxyXG4vL29wdCB0byBkb3dubG9hZCB3aXRob3V0IGdpdmluZyBlLW1haWxcclxudmFyIE5vTWFpbExpbmsgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbm9tYWlsJyk7XHJcblxyXG5pZihOb01haWxMaW5rICE9PSBudWxsKSB7XHJcbiBOb01haWxMaW5rLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XHJcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gIGNoYW5nZV9kb3dubG9hZF9IVE1MKHNhdmVtYWlsZm9ybSxcIlDFmcOtamVtbsOpPGJyPsSNdGVuw60hXCIpO1xyXG4gfSk7XHJcbn0iXSwibmFtZXMiOlsiVnVlIiwiTmF2YmFyIiwiRm9vdGVyQ29sbGFwc2libGUiLCJVaUFwcCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztFQUFBLFdBQWMsR0FBRztFQUNqQixFQUFFLElBQUksRUFBRSxPQUFPO0VBQ2YsRUFBRSxJQUFJLEVBQUUsT0FBTztFQUNmLEVBQUUsSUFBSSxFQUFFLE9BQU87RUFDZixFQUFFLElBQUksRUFBRSxRQUFRO0VBQ2hCLEVBQUUsS0FBSyxFQUFFLFFBQVE7RUFDakIsQ0FBQzs7RUNKRCxNQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ2hFO0VBQ08sTUFBTSxXQUFXLEdBQUcsVUFBVSxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRTtFQUM3RCxFQUFFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0VBQ3pDLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7RUFDbkMsR0FBRztFQUNILENBQUMsQ0FBQztBQUNGO0VBQ08sU0FBUyxjQUFjLEdBQUc7RUFDakMsRUFBRSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxXQUFXLElBQUksQ0FBQyxFQUFFLE1BQU0sQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLENBQUM7RUFDckYsQ0FBQztBQUNEO0VBQ08sU0FBUyxjQUFjLEdBQUc7RUFDakMsRUFBRSxPQUFPLGNBQWMsRUFBRSxJQUFJLFlBQVksQ0FBQztFQUMxQzs7OztBQ0ZBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7O0VBRUE7RUFDQTtFQUNBO0VBQ0E7O0VDbERBLFNBQVMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLGdCQUFnQixvQkFBb0IsVUFBVSxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxvQkFBb0IsRUFBRTtFQUM3TCxJQUFJLElBQUksT0FBTyxVQUFVLEtBQUssU0FBUyxFQUFFO0VBQ3pDLFFBQVEsaUJBQWlCLEdBQUcsY0FBYyxDQUFDO0VBQzNDLFFBQVEsY0FBYyxHQUFHLFVBQVUsQ0FBQztFQUNwQyxRQUFRLFVBQVUsR0FBRyxLQUFLLENBQUM7RUFDM0IsS0FBSztFQUNMO0VBQ0EsSUFBSSxNQUFNLE9BQU8sR0FBRyxPQUFPLE1BQU0sS0FBSyxVQUFVLEdBQUcsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7RUFDM0U7RUFDQSxJQUFJLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUU7RUFDckMsUUFBUSxPQUFPLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7RUFDekMsUUFBUSxPQUFPLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7RUFDM0QsUUFBUSxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztFQUNqQztFQUNBLFFBQVEsSUFBSSxvQkFBb0IsRUFBRTtFQUNsQyxZQUFZLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0VBQ3RDLFNBQVM7RUFDVCxLQUFLO0VBQ0w7RUFDQSxJQUFJLElBQUksT0FBTyxFQUFFO0VBQ2pCLFFBQVEsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7RUFDbkMsS0FBSztFQUNMLElBQUksSUFBSSxJQUFJLENBQUM7RUFDYixJQUFJLElBQUksZ0JBQWdCLEVBQUU7RUFDMUI7RUFDQSxRQUFRLElBQUksR0FBRyxVQUFVLE9BQU8sRUFBRTtFQUNsQztFQUNBLFlBQVksT0FBTztFQUNuQixnQkFBZ0IsT0FBTztFQUN2QixxQkFBcUIsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztFQUMzRCxxQkFBcUIsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztFQUN6RjtFQUNBLFlBQVksSUFBSSxDQUFDLE9BQU8sSUFBSSxPQUFPLG1CQUFtQixLQUFLLFdBQVcsRUFBRTtFQUN4RSxnQkFBZ0IsT0FBTyxHQUFHLG1CQUFtQixDQUFDO0VBQzlDLGFBQWE7RUFDYjtFQUNBLFlBQVksSUFBSSxLQUFLLEVBQUU7RUFDdkIsZ0JBQWdCLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7RUFDN0QsYUFBYTtFQUNiO0VBQ0EsWUFBWSxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMscUJBQXFCLEVBQUU7RUFDMUQsZ0JBQWdCLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztFQUNwRSxhQUFhO0VBQ2IsU0FBUyxDQUFDO0VBQ1Y7RUFDQTtFQUNBLFFBQVEsT0FBTyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7RUFDcEMsS0FBSztFQUNMLFNBQVMsSUFBSSxLQUFLLEVBQUU7RUFDcEIsUUFBUSxJQUFJLEdBQUcsVUFBVTtFQUN6QixjQUFjLFVBQVUsT0FBTyxFQUFFO0VBQ2pDLGdCQUFnQixLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztFQUNoRyxhQUFhO0VBQ2IsY0FBYyxVQUFVLE9BQU8sRUFBRTtFQUNqQyxnQkFBZ0IsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7RUFDMUQsYUFBYSxDQUFDO0VBQ2QsS0FBSztFQUNMLElBQUksSUFBSSxJQUFJLEVBQUU7RUFDZCxRQUFRLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtFQUNoQztFQUNBLFlBQVksTUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQztFQUNsRCxZQUFZLE9BQU8sQ0FBQyxNQUFNLEdBQUcsU0FBUyx3QkFBd0IsQ0FBQyxDQUFDLEVBQUUsT0FBTyxFQUFFO0VBQzNFLGdCQUFnQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0VBQ25DLGdCQUFnQixPQUFPLGNBQWMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7RUFDbEQsYUFBYSxDQUFDO0VBQ2QsU0FBUztFQUNULGFBQWE7RUFDYjtFQUNBLFlBQVksTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztFQUNsRCxZQUFZLE9BQU8sQ0FBQyxZQUFZLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7RUFDakYsU0FBUztFQUNULEtBQUs7RUFDTCxJQUFJLE9BQU8sTUFBTSxDQUFDO0VBQ2xCOzs7RUR2RUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUVFQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTs7O0VBdkJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1dBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBOztFQUVBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTs7O0VBekRBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0RBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7OztFQUhBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUNTQTtBQUNBO0FBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0FBQ0FBLHlCQUFHLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRUMsbUJBQU0sQ0FBQyxDQUFDO0FBQ25DRCx5QkFBRyxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsRUFBRUUsbUJBQWlCLENBQUMsQ0FBQztBQVExRDtBQUNBO0VBQ0EsTUFBTSxVQUFVLEdBQUcsQ0FBQyxFQUFFLEVBQUUsS0FBSyxLQUFLO0VBQ2xDO0VBQ0EsRUFBRSxJQUFJRix1QkFBRyxDQUFDO0VBQ1YsSUFBSSxFQUFFO0VBQ04sSUFBSSxVQUFVLEVBQUU7RUFDaEIsYUFBTUcsbUJBQUs7RUFDWCxLQUFLO0VBQ0wsR0FBRyxDQUFDLENBQUM7RUFDTCxDQUFDLENBQUM7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBLFNBQVMsbUJBQW1CLENBQUMsRUFBRSxFQUFFO0VBQ2pDLEVBQUUsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0VBQzlDLEVBQUUsT0FBTyxVQUFVLENBQUMsRUFBUyxDQUFDLENBQUM7RUFDL0IsQ0FBQztBQUNEO0FBQ0E7RUFDQSxTQUFTLElBQUksQ0FBQyxLQUFLLEVBQUU7RUFDckI7RUFDQSxFQUFFLFdBQVcsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztFQUM1RTtFQUNBLENBQUM7QUFDRDtFQUNBLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztBQUNwRDtFQUNBLElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDN0Q7RUFDQSxHQUFHLGFBQWEsS0FBSyxJQUFJLEVBQUU7RUFDM0IsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0VBQ3BELEdBQUcsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0VBQ3RCLEdBQUcsSUFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztFQUN4QztFQUNBO0VBQ0E7RUFDQSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0VBQzVELEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ25CLDRDQUE0QyxDQUFDLEdBQUcsY0FBYyxHQUFHLENBQUM7QUFDbEUsVUFBVSxDQUFDLENBQUM7RUFDWixFQUFFLENBQUMsQ0FBQztFQUNKLENBQUM7QUFDRDtFQUNBO0FBQ0E7RUFDQTtBQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNBLFNBQVMsYUFBYSxDQUFDLElBQUksRUFBRTtFQUM3QixDQUFDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7RUFDMUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztFQUNwQixDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0VBQ3hCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7RUFDbkMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7RUFDaEIsQ0FBQztFQUNELFNBQVMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztFQUM5QyxFQUFFLElBQUksZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBQztFQUNuRSxFQUFFLGVBQWUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDO0VBQ3BDLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFLENBQUMsQ0FBQztFQUNKLEVBQUUsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0VBQ3hELEVBQUUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7RUFDekMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztFQUN0RCxJQUFJLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztFQUN2QixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0VBQzNCLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztFQUM3QixJQUFJLFVBQVUsQ0FBQyxVQUFVO0VBQ3pCLEtBQUssTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDO0VBQzVDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztFQUNiLElBQUksQ0FBQyxDQUFDO0VBQ04sR0FBRztFQUNILENBQUM7QUFDRDtFQUNBLElBQUksWUFBWSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7QUFDM0Q7RUFDQSxHQUFHLFlBQVksS0FBSyxJQUFJLEVBQUU7RUFDMUIsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0VBQ3BELEVBQUUsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0VBQ3JCLEVBQUUsb0JBQW9CLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7RUFDekQsRUFBRSxDQUFDLENBQUM7RUFDSixDQUFDO0FBQ0Q7RUFDQTtFQUNBLElBQUksVUFBVSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDbkQ7RUFDQSxHQUFHLFVBQVUsS0FBSyxJQUFJLEVBQUU7RUFDeEIsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0VBQ2pELEVBQUUsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0VBQ3JCLEVBQUUsb0JBQW9CLENBQUMsWUFBWSxDQUFDLG9CQUFvQixDQUFDLENBQUM7RUFDMUQsRUFBRSxDQUFDLENBQUM7RUFDSjs7Ozs7OyJ9
