# Generated by Django 3.1.7 on 2021-03-23 05:31

import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("czech_inspirational", "0005_auto_20210323_0536"),
    ]

    operations = [
        migrations.AddField(
            model_name="czechinspirationalchapterpage",
            name="extra_text",
            field=wagtail.fields.RichTextField(
                blank=True, verbose_name="extra modrý blok"
            ),
        ),
    ]
