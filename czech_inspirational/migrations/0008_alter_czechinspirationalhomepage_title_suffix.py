# Generated by Django 5.0.6 on 2024-06-13 10:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("czech_inspirational", "0007_czechinspirationalhomepage_title_suffix"),
    ]

    operations = [
        migrations.AlterField(
            model_name="czechinspirationalhomepage",
            name="title_suffix",
            field=models.CharField(
                blank=True,
                help_text="Umožňuje přidat příponu k základnímu titulku stránky. Pokud je např. titulek stránky pojmenovaný 'Kontakt' a do přípony vyplníte 'MS Pardubice', výsledný titulek bude 'Kontakt | Piráti MS Pardubice'. Pokud příponu nevyplníte, použije se název domovské stránky a text 'Piráti', např. 'Kontakt | Piráti Pardubice'.",
                max_length=100,
                null=True,
                verbose_name="Přípona titulku stránky",
            ),
        ),
    ]
