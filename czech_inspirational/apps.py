from django.apps import AppConfig


class CzechInspirationalConfig(AppConfig):
    name = "czech_inspirational"
