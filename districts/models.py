from django.db import models
from django.utils.translation import gettext_lazy
from wagtail.admin.panels import CommentPanel, FieldPanel, HelpPanel, MultiFieldPanel
from wagtail.models import Page
from wagtailmetadata.models import MetadataPageMixin

from shared.models import ExtendedMetadataHomePageMixin
from tuning import admin_help


class DistrictsHomePage(ExtendedMetadataHomePageMixin, MetadataPageMixin, Page):
    ### FIELDS

    matomo_id = models.IntegerField(
        "Matomo ID pro sledování návštěvnosti", blank=True, null=True
    )

    ### PANELS

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(admin_help.build(admin_help.IMPORTANT_TITLE)),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    settings_panels = [
        FieldPanel("matomo_id"),
        FieldPanel("title_suffix"),
        FieldPanel("meta_title_suffix"),
        CommentPanel(),
    ]

    ### RELATIONS

    subpage_types = []

    ### OTHERS

    class Meta:
        verbose_name = "Krajský rozcestník"
