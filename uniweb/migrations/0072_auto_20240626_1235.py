from django.db import migrations, transaction


def migrate_button_blocks(apps, schema_editor):
    model_fields = {
        "UniwebHomePage": ["content"],
        "UniwebFlexiblePage": ["content"],
        "UniwebFormPage": ["content_before", "content_after", "content_landing"],
    }

    def process_block(block):
        # Perform your block transformation logic here
        # For example, replace some blocks with different blocks
        if block.block_type == "button":
            new_block_value = {
                "title": block.value["text"],
                "color": "pirati-yellow",
                "hoveractive": False,
                "page": block.value["page"],
                "link": block.value["url"],
                "align": "auto",
            }

            new_block = ("new_button", new_block_value)

            return new_block
        elif block.block_type == "title":
            new_block_value = {
                "headline": block.value,
                "tag": "h1",
                "style": "head-alt-xl",
                "align": "auto",
            }

            new_block = ("headline", new_block_value)

            return new_block
        elif block.block_type == "advanced_title":
            new_block_value = {
                "headline": block.value["title"],
                "tag": "h1",
                "style": "head-alt-xl",
                "align": "auto" if block.value["align"] != "center" else "center",
            }

            new_block = ("headline", new_block_value)

            return new_block
        elif block.block_type == "gallery":
            new_block_value = {"gallery_items": []}

            for item in block.value:
                new_block_value["gallery_items"].append(item)

            new_block = ("new_gallery", new_block_value)

            return new_block

        return (block.block_type, block.value)

    for model_name, fields in model_fields.items():
        model = apps.get_model("uniweb", model_name)

        for instance in model.objects.all():
            for field in fields:
                new_values = []

                try:
                    with transaction.atomic():
                        for block in getattr(instance, field):
                            new_block = process_block(block)
                            new_values.append(new_block)
                except Exception:
                    # Skip instances that, for whichever reason, generate errors.
                    continue

                # Clean the old field (e.g., instance.content = [])
                setattr(instance, field, [])

                # Put all the values back in the field, maintaining the order
                stream_value = [(block_type, value) for block_type, value in new_values]
                setattr(instance, field, stream_value)

                instance.save()


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0071_alter_uniwebflexiblepage_content_and_more"),
    ]

    operations = [
        migrations.RunPython(migrate_button_blocks),
    ]
