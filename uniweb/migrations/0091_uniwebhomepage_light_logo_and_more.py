# Generated by Django 5.0.6 on 2024-07-10 07:45

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0090_rename_logo_uniwebhomepage_dark_logo_and_more"),
        ("wagtailimages", "0026_delete_uploadedimage"),
    ]

    operations = [
        migrations.AddField(
            model_name="uniwebhomepage",
            name="light_logo",
            field=models.ForeignKey(
                blank=True,
                help_text="Pokud žádné nezadáte, použije se default logo pirátů",
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="uniweb_light_logo_image",
                to="wagtailimages.image",
                verbose_name="Logo pro web (světlá verze)",
            ),
        ),
        migrations.AlterField(
            model_name="uniwebhomepage",
            name="dark_logo",
            field=models.ForeignKey(
                blank=True,
                help_text="Pokud žádné nezadáte, použije se default logo pirátů",
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="uniweb_dark_logo_image",
                to="wagtailimages.image",
                verbose_name="Logo pro web (tmavá verze)",
            ),
        ),
    ]
