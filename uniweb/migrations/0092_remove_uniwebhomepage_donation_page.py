# Generated by Django 5.0.6 on 2024-07-10 09:00

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0091_uniwebhomepage_light_logo_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="uniwebhomepage",
            name="donation_page",
        ),
    ]
