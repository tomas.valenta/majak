# Generated by Django 4.1.5 on 2023-01-31 19:42

import django.db.models.deletion
import modelcluster.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0034_alter_uniwebarticlepage_content_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="uniwebarticletag",
            name="content_object",
            field=modelcluster.fields.ParentalKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="tagged_items",
                to="uniweb.uniwebarticlepage",
            ),
        ),
    ]
