# Generated by Django 3.2.12 on 2022-02-13 11:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0019_auto_20211006_2258"),
    ]

    operations = [
        migrations.AddField(
            model_name="uniwebhomepage",
            name="show_logo",
            field=models.BooleanField(
                default=True, help_text="Zobrazit logo", verbose_name="zobrazit logo"
            ),
        ),
        migrations.AddField(
            model_name="uniwebhomepage",
            name="show_pirate_buttons",
            field=models.BooleanField(
                default=True,
                help_text="Zobrazit pirátská tlačítka",
                verbose_name="zobrazit pirátská tlačítka",
            ),
        ),
        migrations.AddField(
            model_name="uniwebhomepage",
            name="show_social_links",
            field=models.BooleanField(
                default=True,
                help_text="Zobrazit link na sociální sítě",
                verbose_name="zobrazit soc. linky",
            ),
        ),
    ]
