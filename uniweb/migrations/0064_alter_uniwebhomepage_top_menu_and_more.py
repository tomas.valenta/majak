# Generated by Django 5.0.6 on 2024-06-12 14:12

import wagtail.blocks
import wagtail.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("uniweb", "0063_alter_uniwebcalendarpage_calendar_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="uniwebhomepage",
            name="top_menu",
            field=wagtail.fields.StreamField(
                [
                    (
                        "item",
                        wagtail.blocks.StructBlock(
                            [
                                ("name", wagtail.blocks.CharBlock(label="název")),
                                (
                                    "page",
                                    wagtail.blocks.PageChooserBlock(
                                        label="stránka",
                                        page_type=[
                                            "uniweb.UniwebHomePage",
                                            "uniweb.UniwebFlexiblePage",
                                            "uniweb.UniwebArticlesIndexPage",
                                            "uniweb.UniwebFormPage",
                                            "uniweb.UniwebPeoplePage",
                                            "uniweb.UniwebPersonPage",
                                            "uniweb.UniwebPdfPage",
                                            "district.DistrictPersonPage",
                                        ],
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                verbose_name="horní menu",
            ),
        ),
        migrations.AlterField(
            model_name="uniwebpeoplepage",
            name="content",
            field=wagtail.fields.StreamField(
                [
                    (
                        "text",
                        wagtail.blocks.RichTextBlock(
                            features=[
                                "h2",
                                "h3",
                                "h4",
                                "h5",
                                "bold",
                                "italic",
                                "ol",
                                "ul",
                                "hr",
                                "link",
                                "document-link",
                                "image",
                                "superscript",
                                "subscript",
                                "strikethrough",
                                "blockquote",
                                "embed",
                            ],
                            label="Textový editor",
                        ),
                    ),
                    (
                        "people_group",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "group_title",
                                    wagtail.blocks.CharBlock(
                                        label="Titulek", required=True
                                    ),
                                ),
                                (
                                    "person_list",
                                    wagtail.blocks.ListBlock(
                                        wagtail.blocks.StructBlock(
                                            [
                                                (
                                                    "position",
                                                    wagtail.blocks.CharBlock(
                                                        label="Název pozice",
                                                        required=False,
                                                    ),
                                                ),
                                                (
                                                    "person",
                                                    wagtail.blocks.PageChooserBlock(
                                                        label="Osoba",
                                                        page_type=[
                                                            "uniweb.UniwebPersonPage",
                                                            "district.DistrictPersonPage",
                                                        ],
                                                    ),
                                                ),
                                            ]
                                        ),
                                        label="List osob",
                                    ),
                                ),
                            ]
                        ),
                    ),
                ],
                blank=True,
                verbose_name="Obsah stránky",
            ),
        ),
    ]
