from wagtail.admin.forms import WagtailAdminPageForm

from shared.forms import ArticlesPageForm as SharedArticlesPageForm


class UniwebHomePageAdminForm(WagtailAdminPageForm):
    def clean(self):
        cleaned_data = super().clean()

        # If one logo is set, then both logos must be set.
        if (cleaned_data["dark_logo"] or cleaned_data["light_logo"]) and (
            not cleaned_data["dark_logo"] or not cleaned_data["light_logo"]
        ):
            self.add_error(
                "dark_logo",
                "Pokud je jedno logo nastaveno, musí být obě loga nastaveny.",
            )
            self.add_error(
                "light_logo",
                "Pokud je jedno logo nastaveno, musí být obě loga nastaveny.",
            )


class UniwebArticlesPageForm(SharedArticlesPageForm, WagtailAdminPageForm):
    def __init__(self, *args, **kwargs):
        from shared.models import SharedTag

        from .models import UniwebArticleTag

        super().__init__(*args, **kwargs)

        self.fields["shared_tags"].queryset = SharedTag.objects.order_by("name")

        if self.instance.pk:
            valid_tag_ids = (
                UniwebArticleTag.objects.filter(
                    content_object__in=self.instance.get_children().specific()
                )
                .values_list("id", flat=True)
                .distinct()
            )
            valid_shared_tag_ids = (
                self.instance.shared_tags.values_list("id", flat=True).distinct().all()
            )

            self.fields["displayed_tags"].queryset = (
                UniwebArticleTag.objects.filter(id__in=valid_tag_ids)
                .order_by("tag__name")
                .distinct("tag__name")
            )
            self.fields["displayed_shared_tags"].queryset = (
                SharedTag.objects.filter(id__in=valid_shared_tag_ids)
                .order_by("name")
                .distinct("name")
            )
        else:
            self.fields["displayed_tags"].queryset = UniwebArticleTag.objects.filter(
                id=-1
            )
            self.fields["displayed_shared_tags"].queryset = SharedTag.objects.filter(
                id=-1
            )
