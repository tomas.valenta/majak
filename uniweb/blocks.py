from wagtail.blocks import CharBlock, ListBlock, PageChooserBlock, StructBlock, URLBlock

from shared.blocks import (
    CardLinkBlockMixin,
    CardLinkWithHeadlineBlockMixin,
    PeopleGroupBlockMixin,
    PersonCustomPositionBlockMixin,
    TeamBlockMixin,
)

# TODO: Figure out


class PersonUrlBlock(StructBlock):
    title = CharBlock(label="Název", required=True)
    url = URLBlock(label="URL", required=True)
    custom_icon = CharBlock(
        label="Vlastní ikonka ze styleguide",
        required=False,
        help_text="Pro vlastní ikonku zadejde název ikonky z https://styleguide.pirati.cz/latest/?p=viewall-atoms-icons (bez tečky), např. 'ico--beer'",
    )


# TO REMOVE ↓↓↓


class PersonContactBlock(StructBlock):
    position = CharBlock(label="Název pozice", required=False)
    person = PageChooserBlock(
        label="Osoba",
        page_type=[
            "uniweb.UniwebPersonPage",
            "district.DistrictPersonPage",
        ],
    )

    class Meta:
        icon = "user"
        label = "Osoba s volitelnou pozicí"


class PeopleGroupListBlock(StructBlock):
    group_title = CharBlock(label="Titulek", required=True)
    person_list = ListBlock(
        PersonContactBlock(),
        label="List osob",
    )

    class Meta:
        template = "uniweb/blocks/people_group_block.html"
        icon = "list-ul"
        label = "Skupina členů"


# New blocks


class PersonCustomPositionBlock(PersonCustomPositionBlockMixin):
    page = PageChooserBlock(
        page_type=["uniweb.UniwebPersonPage", "district.DistrictPersonPage"],
        label="Detail osoby",
    )


class PeopleGroupBlock(PeopleGroupBlockMixin):
    person_list = ListBlock(
        PageChooserBlock(
            page_type=["uniweb.UniwebPersonPage", "district.DistrictPersonPage"],
            label="Detail osoby",
        ),
        default=[],
        label="Osoby",
        help_text="S pozicemi z jejich podstránek",
    )

    person_list_with_custom_positions = ListBlock(
        PersonCustomPositionBlock(),
        default=[],
        label="Osoby",
        help_text="S nastavitelnými pozicemi",
    )


class CardLinkBlock(CardLinkBlockMixin):
    page = PageChooserBlock(
        label="Stránka",
        page_type=[
            "uniweb.UniwebFlexiblePage",
            "uniweb.UniwebArticlesIndexPage",
            "uniweb.UniwebFormPage",
            "uniweb.UniwebPeoplePage",
            "uniweb.UniwebCalendarPage",
            "uniweb.UniwebSearchPage",
        ],
        required=False,
    )


class CardLinkWithHeadlineBlock(CardLinkWithHeadlineBlockMixin):
    card_items = ListBlock(
        CardLinkBlock(
            template="styleguide2/includes/molecules/boxes/card_box_block.html"
        ),
        label="Karty s odkazy",
    )


class TeamBlock(TeamBlockMixin):
    team_list = ListBlock(
        CardLinkWithHeadlineBlock(label="Karta týmu"),
        label="Týmy",
    )
