BLACK_ON_WHITE = "black_on_white"
WHITE_ON_BLACK = "white_on_black"
WHITE_ON_BLUE = "white_on_blue"
WHITE_ON_CYAN = "white_on_cyan"
WHITE_ON_VIOLET = "white_on_violet"
BLACK_ON_YELLOW = "black_on_yellow"

COLOR_CHOICES = (
    (BLACK_ON_WHITE, "černá na bílé"),
    (BLACK_ON_YELLOW, "černá na žluté"),
    (WHITE_ON_BLACK, "bílá na černé"),
    (WHITE_ON_BLUE, "bílá na modré"),
    (WHITE_ON_CYAN, "bílá na tyrkysové"),
    (WHITE_ON_VIOLET, "bílá na fialové"),
)

COLOR_CSS = {
    BLACK_ON_WHITE: ["text-black", "bg-white"],
    BLACK_ON_YELLOW: ["text-black", "bg-pirati-yellow"],
    WHITE_ON_BLACK: ["text-white", "bg-black"],
    WHITE_ON_BLUE: ["text-white", "bg-blue-300"],
    WHITE_ON_CYAN: ["text-white", "bg-cyan-300"],
    WHITE_ON_VIOLET: ["text-white", "bg-violet-300"],
}

LEFT = "left"
CENTER = "center"
RIGHT = "right"

ALIGN_CHOICES = (
    (LEFT, "vlevo"),
    (CENTER, "uprostřed"),
    (RIGHT, "vpravo"),
)

ALIGN_CSS = {
    LEFT: ["text-left"],
    CENTER: ["text-center"],
    RIGHT: ["text-right"],
}

ARTICLES_PER_LINE = 3
ARTICLES_PER_PAGE = ARTICLES_PER_LINE * 4

FUTURE = "future"
PAST = "past"

CALENDAR_EVENTS_CHOICES = (
    (FUTURE, "budoucí"),
    (PAST, "proběhlé"),
)
