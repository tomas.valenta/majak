from .constants import (
    AGE_18_29,
    AGE_30_49,
    AGE_50_64,
    AGE_65_PLUS,
    CHILDLESS,
    MATURE,
    OCCUPATION_STUDENT,
    OCCUPATION_WORKING,
    PARENTS,
    SENIORS,
    STUDENTS,
    WORKING_SENIORS,
)


def get_archetype(selection):
    if selection["occupation"] == OCCUPATION_STUDENT:
        return STUDENTS
    if selection["age"] == AGE_50_64:
        return MATURE
    if selection["age"] == AGE_65_PLUS:
        if selection["occupation"] == OCCUPATION_WORKING:
            return WORKING_SENIORS
        return SENIORS
    if selection["age"] in [AGE_18_29, AGE_30_49]:
        if selection["kids"]:
            return PARENTS
        return CHILDLESS
    return None
