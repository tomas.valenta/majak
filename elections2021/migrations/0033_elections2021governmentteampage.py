# Generated by Django 3.2.4 on 2021-07-15 11:48

import django.db.models.deletion
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtailmetadata.models
from django.db import migrations, models

import shared.models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailcore", "0062_comment_models_and_pagesubscription"),
        ("wagtailimages", "0023_add_choose_permissions"),
        ("elections2021", "0032_auto_20210625_1540"),
    ]

    operations = [
        migrations.CreateModel(
            name="Elections2021GovernmentTeamPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "sections",
                    wagtail.fields.StreamField(
                        [
                            (
                                "section",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(label="název"),
                                        ),
                                        (
                                            "members",
                                            wagtail.blocks.ListBlock(
                                                wagtail.blocks.StructBlock(
                                                    [
                                                        (
                                                            "name",
                                                            wagtail.blocks.CharBlock(
                                                                label="jméno"
                                                            ),
                                                        ),
                                                        (
                                                            "photo",
                                                            wagtail.images.blocks.ImageChooserBlock(
                                                                label="fotka"
                                                            ),
                                                        ),
                                                        (
                                                            "occupation",
                                                            wagtail.blocks.CharBlock(
                                                                label="povolání"
                                                            ),
                                                        ),
                                                        (
                                                            "resume",
                                                            wagtail.blocks.TextBlock(
                                                                label="medailonek"
                                                            ),
                                                        ),
                                                    ]
                                                )
                                            ),
                                        ),
                                    ]
                                ),
                            )
                        ],
                        blank=True,
                        verbose_name="sekce vládního týmu",
                    ),
                ),
                (
                    "photo",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="wagtailimages.image",
                        verbose_name="hlavní fotka",
                    ),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Vládní tým",
            },
            bases=(
                shared.models.SubpageMixin,
                wagtailmetadata.models.WagtailImageMetadataMixin,
                "wagtailcore.page",
                models.Model,
            ),
        ),
    ]
