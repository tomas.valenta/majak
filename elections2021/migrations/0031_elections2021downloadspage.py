# Generated by Django 3.2.4 on 2021-06-17 23:26

import django.db.models.deletion
import wagtail.blocks
import wagtail.documents.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtailmetadata.models
from django.db import migrations, models

import shared.models


class Migration(migrations.Migration):
    dependencies = [
        ("wagtailcore", "0062_comment_models_and_pagesubscription"),
        ("wagtailimages", "0023_add_choose_permissions"),
        ("elections2021", "0030_elections2021mythpage_elections2021mythspage"),
    ]

    operations = [
        migrations.CreateModel(
            name="Elections2021DownloadsPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.page",
                    ),
                ),
                (
                    "downloads",
                    wagtail.fields.StreamField(
                        [
                            (
                                "document",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(label="název"),
                                        ),
                                        (
                                            "photo",
                                            wagtail.images.blocks.ImageChooserBlock(
                                                label="náhled"
                                            ),
                                        ),
                                        (
                                            "document",
                                            wagtail.documents.blocks.DocumentChooserBlock(
                                                label="dokument"
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                            (
                                "spotify",
                                wagtail.blocks.StructBlock(
                                    [
                                        (
                                            "title",
                                            wagtail.blocks.CharBlock(label="název"),
                                        ),
                                        (
                                            "photo",
                                            wagtail.images.blocks.ImageChooserBlock(
                                                label="náhled"
                                            ),
                                        ),
                                        (
                                            "url",
                                            wagtail.blocks.URLBlock(
                                                label="odkaz na Spotify"
                                            ),
                                        ),
                                    ]
                                ),
                            ),
                        ],
                        blank=True,
                        verbose_name="soubory ke stažení",
                    ),
                ),
                (
                    "search_image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                        verbose_name="Search image",
                    ),
                ),
            ],
            options={
                "verbose_name": "Soubory ke stažení",
            },
            bases=(
                shared.models.SubpageMixin,
                wagtailmetadata.models.WagtailImageMetadataMixin,
                "wagtailcore.page",
                models.Model,
            ),
        ),
    ]
