# Generated by Django 4.1.13 on 2024-03-04 16:16

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("main", "0077_alter_mainresultspage_content"),
        ("elections2021", "0057_rename_date_elections2021articlepage_timestamp"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="elections2021homepage", name="gdpr_and_cookies_page"
        ),
        migrations.AddField(
            model_name="elections2021homepage",
            name="gdpr_and_cookies_page",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="main.mainsimplepage",
                verbose_name="Stránka GDPR a Cookies",
            ),
        ),
    ]
