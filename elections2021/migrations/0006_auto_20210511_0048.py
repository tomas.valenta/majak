# Generated by Django 3.1.7 on 2021-05-10 22:48

import wagtail.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("elections2021", "0005_auto_20210505_0945"),
    ]

    operations = [
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="already_done",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="co jsme už udělali"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="annotation",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="anotace"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="context",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="kontext problému"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="ideal",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="ideál"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="problem",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="problém"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="proposal",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="navrhovaná opatření"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="sources",
            field=wagtail.fields.RichTextField(
                blank=True, null=True, verbose_name="zdroje"
            ),
        ),
        migrations.AlterField(
            model_name="elections2021programpointpage",
            name="time_horizon_unit",
            field=models.CharField(
                blank=True,
                max_length=20,
                null=True,
                verbose_name="časový horizont jednotka",
            ),
        ),
    ]
