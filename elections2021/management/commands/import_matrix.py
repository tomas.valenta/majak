import csv

from django.core.management.base import BaseCommand
from django.utils.text import slugify

from ...constants import (
    CHILDLESS,
    COUNTRYSIDE,
    CULTURE,
    EDUCATION,
    HEALTH,
    HOUSING,
    MATURE,
    MINISTRY_AGRICULTURE,
    MINISTRY_BUSINESS,
    MINISTRY_COUNTRYSIDE,
    MINISTRY_CULTURE,
    MINISTRY_DEFENSE,
    MINISTRY_ENVIRONMENT,
    MINISTRY_FINANCES,
    MINISTRY_FOREIGN,
    MINISTRY_HEALTH,
    MINISTRY_INTERIOR,
    MINISTRY_JUSTICE,
    MINISTRY_SCHOOLS,
    MINISTRY_SOCIAL,
    MINISTRY_TRANSPORT,
    NATURE,
    PARENTS,
    PLAN_CARE,
    PLAN_CLIMATE,
    PLAN_CORRUPTION,
    PLAN_COUNTRYSIDE,
    PLAN_DIGITAL,
    PLAN_ECONOMICS,
    PLAN_MANAGEMENT,
    PLAN_SAFE_COUNTRY,
    SELF_EMPLOYED,
    SENIORS,
    SOCIALLY_WEAK,
    SPORT,
    STUDENTS,
    TECHNOLOGY,
    WORKING_SENIORS,
)
from ...models import Elections2021ProgramPointPage

OLD_TITLE = "old_title"
TITLE = "title"
ORDER = "order"

FIELDNAMES = (
    "x1",
    OLD_TITLE,
    TITLE,
    "x2",
    CHILDLESS,
    PARENTS,
    MATURE,
    SENIORS,
    WORKING_SENIORS,
    STUDENTS,
    SELF_EMPLOYED,
    SOCIALLY_WEAK,
    NATURE,
    SPORT,
    HEALTH,
    CULTURE,
    TECHNOLOGY,
    COUNTRYSIDE,
    HOUSING,
    EDUCATION,
    PLAN_ECONOMICS,
    PLAN_DIGITAL,
    PLAN_CORRUPTION,
    PLAN_CLIMATE,
    PLAN_COUNTRYSIDE,
    PLAN_MANAGEMENT,
    PLAN_SAFE_COUNTRY,
    PLAN_CARE,
    MINISTRY_TRANSPORT,
    MINISTRY_FINANCES,
    MINISTRY_CULTURE,
    MINISTRY_DEFENSE,
    MINISTRY_SOCIAL,
    MINISTRY_COUNTRYSIDE,
    MINISTRY_BUSINESS,
    MINISTRY_JUSTICE,
    MINISTRY_SCHOOLS,
    MINISTRY_INTERIOR,
    MINISTRY_FOREIGN,
    MINISTRY_HEALTH,
    MINISTRY_AGRICULTURE,
    MINISTRY_ENVIRONMENT,
    ORDER,
)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("csv_file", type=open)

    def handle(self, *args, **options):
        missing = []

        reader = csv.DictReader(options["csv_file"], fieldnames=FIELDNAMES)
        for row in list(reader)[1:]:
            slug = slugify(row[TITLE])
            try:
                page = Elections2021ProgramPointPage.objects.get(slug=slug)
            except Elections2021ProgramPointPage.DoesNotExist:
                missing.append(row[TITLE])
                continue

            print("+", row[TITLE])

            was_live = page.live
            had_unpublished_changes = page.has_unpublished_changes

            page.weight_childless = int(row[CHILDLESS])
            page.weight_parents = int(row[PARENTS])
            page.weight_mature = int(row[MATURE])
            page.weight_seniors = int(row[SENIORS])
            page.weight_working_seniors = int(row[WORKING_SENIORS])
            page.weight_students = int(row[STUDENTS])
            page.weight_self_employed = int(row[SELF_EMPLOYED])
            page.weight_socially_weak = int(row[SOCIALLY_WEAK])
            page.weight_nature = int(row[NATURE])
            page.weight_sport = int(row[SPORT])
            page.weight_health = int(row[HEALTH])
            page.weight_culture = int(row[CULTURE])
            page.weight_technology = int(row[TECHNOLOGY])
            page.weight_countryside = int(row[COUNTRYSIDE])
            page.weight_housing = int(row[HOUSING])
            page.weight_education = int(row[EDUCATION])
            page.weight_plan_economics = int(row[PLAN_ECONOMICS])
            page.weight_plan_digital = int(row[PLAN_DIGITAL])
            page.weight_plan_corruption = int(row[PLAN_CORRUPTION])
            page.weight_plan_climate = int(row[PLAN_CLIMATE])
            page.weight_plan_countryside = int(row[PLAN_COUNTRYSIDE])
            page.weight_plan_management = int(row[PLAN_MANAGEMENT])
            page.weight_plan_safe_country = int(row[PLAN_SAFE_COUNTRY])
            page.weight_plan_care = int(row[PLAN_CARE])
            page.weight_ministry_transport = int(row[MINISTRY_TRANSPORT])
            page.weight_ministry_finances = int(row[MINISTRY_FINANCES])
            page.weight_ministry_culture = int(row[MINISTRY_CULTURE])
            page.weight_ministry_defense = int(row[MINISTRY_DEFENSE])
            page.weight_ministry_social = int(row[MINISTRY_SOCIAL])
            page.weight_ministry_countryside = int(row[MINISTRY_COUNTRYSIDE])
            page.weight_ministry_business = int(row[MINISTRY_BUSINESS])
            page.weight_ministry_justice = int(row[MINISTRY_JUSTICE])
            page.weight_ministry_schools = int(row[MINISTRY_SCHOOLS])
            page.weight_ministry_interior = int(row[MINISTRY_INTERIOR])
            page.weight_ministry_foreign = int(row[MINISTRY_FOREIGN])
            page.weight_ministry_health = int(row[MINISTRY_HEALTH])
            page.weight_ministry_agriculture = int(row[MINISTRY_AGRICULTURE])
            page.weight_ministry_environment = int(row[MINISTRY_ENVIRONMENT])
            page.default_order = int(row[ORDER])

            revision = page.save_revision()
            if was_live and not had_unpublished_changes:
                revision.publish()

        if missing:
            print("\nMISSING:\n")
            print("\n".join(missing))
