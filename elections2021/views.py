import csv

from django.http import HttpResponse, HttpResponseForbidden

from .models import BannerOrder, Token


def banner_orders_csv(request):
    response = HttpResponse(content_type="text/csv;charset=utf-8")

    tokens = Token.objects.filter(
        bearer=request.GET.get("token"), scope="banner-orders"
    )
    if not tokens:
        return HttpResponseForbidden()

    w = csv.writer(response)
    w.writerow(
        [
            "ID",
            "Plachta",
            "Jméno",
            "Příjmení",
            "Bydliště",
            "Narozen",
            "Telefon",
            "Mail",
            "Poznámka",
        ]
    )

    for row in BannerOrder.objects.order_by("id").all():
        w.writerow(
            [
                str(row.id),
                row.code,
                row.name,
                row.surname,
                row.residency,
                str(row.date_of_birth),
                row.phone,
                row.email,
                row.note,
            ]
        )

    return response
