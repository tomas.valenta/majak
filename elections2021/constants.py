WHITE = "white"
BLACK = "black"
YELLOW = "yellow"
GREEN = "green"

STYLE_CHOICES = (
    (WHITE, "bílé pozadí"),
    (BLACK, "černé pozadí"),
    (YELLOW, "žluté pozadí"),
    (GREEN, "zelené pozadí"),
)

STYLE_CSS = {
    WHITE: "",
    BLACK: "emphasized-black",
    YELLOW: "emphasized-lemon",
    GREEN: "emphasized-acidgreen",
}

ARTICLE_RICH_TEXT_FEATURES = [
    "h2",
    "h3",
    "h4",
    "bold",
    "italic",
    "superscript",
    "subscript",
    "strikethrough",
    "ul-elections2021",
    "ol-elections2021",
    "blockquote-elections2021",
    "link",
    "image",
    "document-link",
]

STRATEGIC_RICH_TEXT_FEATURES = [
    "h3",
    "h4",
    "bold",
    "italic",
    "superscript",
    "subscript",
    "strikethrough",
    "ul-elections2021",
    "ol-elections2021",
    "blockquote-elections2021",
    "link",
    "image",
    "document-link",
]

CANDIDATE_RICH_TEXT_FEATURES = [
    "h3",
    "bold",
    "italic",
    "superscript",
    "subscript",
    "strikethrough",
    "ul-elections2021",
    "ol-elections2021",
    "blockquote-elections2021",
    "link",
    "document-link",
]

RESTRICTED_FEATURES = ["superscript", "italic", "link", "document-link"]

STANDARD_FEATURES = [
    "h4",
    "ul-elections2021",
    "superscript",
    "link",
    "italic",
    "document-link",
]
EXTRA_FEATURES = [
    "h4",
    "ul-program-elections2021",
    "superscript",
    "link",
    "italic",
    "document-link",
]

ARTICLES_PER_PAGE = 9
TOP_CANDIDATES_NUM = 12
PROGRAM_POINTS_PER_PAGE = 12
STRATEGIC_PER_PAGE = 9

REGION_PHA = "PHA"
REGION_JHC = "JHC"
REGION_JHM = "JHM"
REGION_KVK = "KVK"
REGION_VYS = "VYS"
REGION_KHK = "KHK"
REGION_LBK = "LBK"
REGION_MSK = "MSK"
REGION_OLK = "OLK"
REGION_PAK = "PAK"
REGION_PLK = "PLK"
REGION_STC = "STC"
REGION_ULK = "ULK"
REGION_ZLK = "ZLK"

REGION_DATA = (
    (REGION_PHA, "praha", "Hlavní město Praha", "Hlavním městě Praha"),
    (REGION_JHC, "jihocesky", "Jihočeský kraj", "Jihočeském kraji"),
    (REGION_JHM, "jihomoravsky", "Jihomoravský kraj", "Jihomoravském kraji"),
    (REGION_KVK, "karlovarsky", "Karlovarský kraj", "Karlovarském kraji"),
    (REGION_VYS, "vysocina", "Kraj Vysočina", "Kraji Vysočina"),
    (REGION_KHK, "kralovehradecky", "Královéhradecký kraj", "Královéhradeckém kraji"),
    (REGION_LBK, "liberecky", "Liberecký kraj", "Libereckém kraji"),
    (REGION_MSK, "moravskoslezsky", "Moravskoslezský kraj", "Moravskoslezském kraji"),
    (REGION_OLK, "olomoucky", "Olomoucký kraj", "Olomouckém kraji"),
    (REGION_PAK, "pardubicky", "Pardubický kraj", "Pardubickém kraji"),
    (REGION_PLK, "plzensky", "Plzeňský kraj", "Plzeňském kraji"),
    (REGION_STC, "stredocesky", "Středočeský kraj", "Středočeském kraji"),
    (REGION_ULK, "ustecky", "Ústecký kraj", "Ústeckém kraji"),
    (REGION_ZLK, "zlinsky", "Zlínský kraj", "Zlínském kraji"),
)

REGION_CHOICES = [(code, name) for code, slug, name, name2 in REGION_DATA]
REGION_NAME_VARIANT = {code: name2 for code, slug, name, name2 in REGION_DATA}
REGION_SLUGS = {slug: code for code, slug, name, name2 in REGION_DATA}
REGION_OPTIONS = [(slug, name) for code, slug, name, name2 in REGION_DATA]

PIRATES = "pirati"
STAN = "stan"
PARTY_CHOICES = ((PIRATES, "Piráti"), (STAN, "STAN"))
PARTY_NAME = {PIRATES: "Pirátská strana", STAN: "Starostové a nezávislí"}

CHILDLESS = "childless"
PARENTS = "parents"
MATURE = "mature"
SENIORS = "seniors"
WORKING_SENIORS = "working_seniors"
STUDENTS = "students"

SELF_EMPLOYED = "self_employed"
SOCIALLY_WEAK = "socially_weak"

NATURE = "nature"
SPORT = "sport"
HEALTH = "health"
CULTURE = "culture"
TECHNOLOGY = "technology"
COUNTRYSIDE = "countryside"
HOUSING = "housing"
EDUCATION = "education"

TOPIC_CHOICES = (
    (NATURE, "příroda"),
    (SPORT, "sport"),
    (HEALTH, "zdraví"),
    (CULTURE, "kultura"),
    (TECHNOLOGY, "technologie"),
    (COUNTRYSIDE, "regiony"),
    (HOUSING, "bydlení"),
    (EDUCATION, "vzdělávání"),
)

PLAN_ECONOMICS = "moderni-ekonomika"
PLAN_DIGITAL = "digitalni-revoluce"
PLAN_CORRUPTION = "razantni-boj-s-korupci"
PLAN_CLIMATE = "duraz-na-klima"
PLAN_COUNTRYSIDE = "rovnocene-regiony"
PLAN_MANAGEMENT = "profesionalni-stat"
PLAN_SAFE_COUNTRY = "sebevedoma-a-bezpecna-zeme"
PLAN_CARE = "jistota-pece-a-pomoci"

PLAN_CHOICES = (
    (PLAN_ECONOMICS, "Moderní ekonomika"),
    (PLAN_DIGITAL, "Digitální revoluce"),
    (PLAN_CORRUPTION, "Razantní boj s korupcí"),
    (PLAN_CLIMATE, "Důraz na klima"),
    (PLAN_COUNTRYSIDE, "Rovnocenné regiony"),
    (PLAN_MANAGEMENT, "Profesionální stát"),
    (PLAN_SAFE_COUNTRY, "Sebevědomá a bezpečná země"),
    (PLAN_CARE, "Jistota péče a pomoci"),
)

PLAN_OPTIONS = [code for code, _ in PLAN_CHOICES]

PLAN_ARCHETYPES = {
    PLAN_ECONOMICS: "finance",
    PLAN_DIGITAL: "vnitro",
    PLAN_CORRUPTION: "spravedlnost",
    PLAN_CLIMATE: "prostredi",
    PLAN_COUNTRYSIDE: "rozvoj",
    PLAN_MANAGEMENT: "prumysl",
    PLAN_SAFE_COUNTRY: "obrana",
    PLAN_CARE: "prace",
}

MINISTRY_TRANSPORT = "doprava"
MINISTRY_FINANCES = "finance"
MINISTRY_CULTURE = "kultura"
MINISTRY_DEFENSE = "obrana"
MINISTRY_SOCIAL = "prace-a-socialni-veci"
MINISTRY_COUNTRYSIDE = "mistni-rozvoj"
MINISTRY_BUSINESS = "prumysl-a-obchod"
MINISTRY_JUSTICE = "spravedlnost"
MINISTRY_SCHOOLS = "skolstvi-mladez-a-telovychova"
MINISTRY_INTERIOR = "vnitro"
MINISTRY_FOREIGN = "eu-zahranici-obrana"
MINISTRY_HEALTH = "zdravotnictvi"
MINISTRY_AGRICULTURE = "zemedelstvi"
MINISTRY_ENVIRONMENT = "zivotni-prostredi"

MINISTRY_CHOICES = (
    (MINISTRY_TRANSPORT, "Doprava"),
    (MINISTRY_FOREIGN, "EU, zahraničí a obrana"),
    (MINISTRY_FINANCES, "Finance"),
    (MINISTRY_CULTURE, "Kultura"),
    # (MINISTRY_DEFENSE, "Obrana"),
    (MINISTRY_SOCIAL, "Práce a sociální věci"),
    (MINISTRY_COUNTRYSIDE, "Místní rozvoj"),
    (MINISTRY_BUSINESS, "Průmysl a obchod"),
    (MINISTRY_JUSTICE, "Spravedlnost"),
    (MINISTRY_SCHOOLS, "Školství, mládež a tělovýchova"),
    (MINISTRY_INTERIOR, "Vnitro"),
    (MINISTRY_HEALTH, "Zdravotnictví"),
    (MINISTRY_AGRICULTURE, "Zemědělství"),
    (MINISTRY_ENVIRONMENT, "Životní prostředí"),
)

MINISTRY_OPTIONS = [code for code, _ in MINISTRY_CHOICES]

MINISTRY_ARCHETYPES = {
    MINISTRY_TRANSPORT: "doprava",
    MINISTRY_FINANCES: "finance",
    MINISTRY_CULTURE: "kultura",
    MINISTRY_DEFENSE: "obrana",
    MINISTRY_SOCIAL: "prace",
    MINISTRY_COUNTRYSIDE: "rozvoj",
    MINISTRY_BUSINESS: "prumysl",
    MINISTRY_JUSTICE: "spravedlnost",
    MINISTRY_SCHOOLS: "vzdelavani",
    MINISTRY_INTERIOR: "vnitro",
    MINISTRY_FOREIGN: "vztahy",
    MINISTRY_HEALTH: "zdravotnictvi",
    MINISTRY_AGRICULTURE: "zemedelstvi",
    MINISTRY_ENVIRONMENT: "prostredi",
}

MINISTRY_CODES = {
    MINISTRY_TRANSPORT: "transport",
    MINISTRY_FINANCES: "finances",
    MINISTRY_CULTURE: "culture",
    MINISTRY_DEFENSE: "defense",
    MINISTRY_SOCIAL: "social",
    MINISTRY_COUNTRYSIDE: "countryside",
    MINISTRY_BUSINESS: "business",
    MINISTRY_JUSTICE: "justice",
    MINISTRY_SCHOOLS: "schools",
    MINISTRY_INTERIOR: "interior",
    MINISTRY_FOREIGN: "foreign",
    MINISTRY_HEALTH: "health",
    MINISTRY_AGRICULTURE: "agriculture",
    MINISTRY_ENVIRONMENT: "environment",
}

BENEFITS = (
    # (id, název, pracovní název)
    ("1", "Pro mladé", "mladí a bezdětní včetně studentů"),
    ("2", "Pro rodiny s dětmi", "rodiny s dětmi"),
    ("3", "Pro lidi ve zralém věku", "zralí"),
    ("4", "Pro seniory", "senioři a důchodci"),
    ("5", "Pro zaměstnance", "zaměstnanci"),
    ("6", "Pro živnostníky a podnikatele", "živnostníci a podnikatelé"),
    ("7", "Pro lidi ve finanční tísni", "ekonomicky ohrožení"),
    ("8", "Pro nás pro všechny", "společnost"),
    ("9", "Pro veřejné finance", "Veřejné finance:"),
    ("10", "Pro rozvoj obcí a měst", "obce"),
    ("11", "Pro lékaře a zdravotníky", "zdravotníci"),
    ("12", "Pro zemědělce", "zemědělci"),
    ("13", "Pro děti", "děti"),
)

BENEFITS_CHOICES = [(num, name) for num, name, _ in BENEFITS]

AGE_18_29 = "18-29"
AGE_30_49 = "30-49"
AGE_50_64 = "50-64"
AGE_65_PLUS = "65-plus"

AGE_CHOICES = (
    (AGE_18_29, "18 - 29 let"),
    (AGE_30_49, "30 - 49 let"),
    (AGE_50_64, "50 - 64 let"),
    (AGE_65_PLUS, "65+ let"),
)

WEALTH_BAD = "bad"
WEALTH_AVERAGE = "average"
WEALTH_GOOD = "good"

WEALTH_CHOICES = (
    (WEALTH_BAD, "nic moc"),
    (WEALTH_AVERAGE, "průměrně"),
    (WEALTH_GOOD, "skvěle"),
)

OCCUPATION_STUDENT = "student"
OCCUPATION_WORKING = "working"
OCCUPATION_BUSINESS = "business"
OCCUPATION_PARENTING = "parenting"
OCCUPATION_RETIRED = "retired"

OCCUPATION_CHOICES = (
    (OCCUPATION_STUDENT, "studuji"),
    (OCCUPATION_WORKING, "pracuji"),
    (OCCUPATION_BUSINESS, "podnikám"),
    (OCCUPATION_PARENTING, "na rodičovské"),
    (OCCUPATION_RETIRED, "v důchodu"),
)
