import wagtail.admin.rich_text.editors.draftail.features as draftail_features
from django.utils.translation import gettext
from wagtail import hooks
from wagtail.admin.rich_text.converters.html_to_contentstate import (
    BlockElementHandler,
    ListElementHandler,
    ListItemElementHandler,
)


@hooks.register("register_rich_text_features")
def register_blockquote_elections2021_feature(features):
    feature_name = "blockquote-elections2021"
    type_ = "blockquote"
    tag = "blockquote"

    features.register_editor_plugin(
        "draftail",
        feature_name,
        draftail_features.BlockFeature(
            {
                "type": type_,
                "icon": "openquote",
                "description": gettext("Blockquote"),
            }
        ),
    )

    features.register_converter_rule(
        "contentstate",
        feature_name,
        {
            "from_database_format": {
                tag: BlockElementHandler(type_),
            },
            "to_database_format": {
                "block_map": {
                    type_: {
                        "element": tag,
                        "props": {"class": "quote quote-pirati-stan"},
                    }
                }
            },
        },
    )


@hooks.register("register_rich_text_features")
def register_ul_elections2021_feature(features):
    feature_name = "ul-elections2021"
    type_ = "unordered-list-item"
    wrapper_tag = "ul"
    item_tag = "li"

    features.register_editor_plugin(
        "draftail",
        feature_name,
        draftail_features.BlockFeature(
            {
                "type": type_,
                "icon": "list-ul",
                "description": gettext("Bulleted list"),
            }
        ),
    )

    features.register_converter_rule(
        "contentstate",
        feature_name,
        {
            "from_database_format": {
                wrapper_tag: ListElementHandler(type_),
                item_tag: ListItemElementHandler(),
            },
            "to_database_format": {
                "block_map": {
                    type_: {
                        "element": item_tag,
                        "wrapper": wrapper_tag,
                        "wrapper_props": {
                            "class": "unordered-list unordered-list-acidgreen-numbers"
                        },
                    }
                }
            },
        },
    )


@hooks.register("register_rich_text_features")
def register_ol_elections2021_feature(features):
    feature_name = "ol-elections2021"
    type_ = "ordered-list-item"
    wrapper_tag = "ol"
    item_tag = "li"

    features.register_editor_plugin(
        "draftail",
        feature_name,
        draftail_features.BlockFeature(
            {
                "type": type_,
                "icon": "list-ol",
                "description": gettext("Numbered list"),
            }
        ),
    )

    features.register_converter_rule(
        "contentstate",
        feature_name,
        {
            "from_database_format": {
                wrapper_tag: ListElementHandler(type_),
                item_tag: ListItemElementHandler(),
            },
            "to_database_format": {
                "block_map": {
                    type_: {
                        "element": item_tag,
                        "wrapper": wrapper_tag,
                        "wrapper_props": {
                            "class": "ordered-list ordered-list-acidgreen-numbers"
                        },
                    }
                }
            },
        },
    )


@hooks.register("register_rich_text_features")
def register_ul_program_elections2021_feature(features):
    feature_name = "ul-program-elections2021"
    type_ = "unordered-list-item"
    wrapper_tag = "ul"
    item_tag = "li"

    features.register_editor_plugin(
        "draftail",
        feature_name,
        draftail_features.BlockFeature(
            {
                "type": type_,
                "icon": "list-ul",
                "description": gettext("Bulleted list"),
            }
        ),
    )

    features.register_converter_rule(
        "contentstate",
        feature_name,
        {
            "from_database_format": {
                wrapper_tag: ListElementHandler(type_),
                item_tag: ListItemElementHandler(),
            },
            "to_database_format": {
                "block_map": {
                    type_: {
                        "element": item_tag,
                        "props": {"class": "mb-4"},
                        "wrapper": wrapper_tag,
                        "wrapper_props": {
                            "class": "unordered-list unordered-list-checks"
                        },
                    }
                }
            },
        },
    )
