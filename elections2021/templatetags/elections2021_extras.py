import re

import bs4
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(is_safe=True)
@stringfilter
def format_sources_block(value):
    soup = bs4.BeautifulSoup(value, "html.parser")
    out = []

    number = None
    text = ""
    for item in soup.find_all("p"):
        for a in item.find_all("a"):
            a["class"] = "text-fxactivecolor underline"

        content = "".join(str(c) for c in item.contents)
        m = re.match(r"^\[(\d+)\]\s(.+)", content)
        if m:
            if number:
                out.append(
                    f"""
                    <div id="zdroj{number}" class="source leading-loose text-sm">
                      <div class="inline-block align-top w-9 text-fxactivecolor">[{number}]</div>
                      <div class="inline-block align-top max-w-2xl">{text}</div>
                    </div>
                    """
                )

            number = m.group(1)
            text = m.group(2)
        else:
            text = f"{text}<br>{content}"

    # append last
    out.append(
        f"""
        <div id="zdroj{number}" class="source leading-loose text-sm">
          <div class="inline-block align-top w-9 text-fxactivecolor">[{number}]</div>
          <div class="inline-block align-top max-w-2xl">{text}</div>
        </div>
        """
    )

    return "".join(out)


@register.filter(is_safe=True)
@stringfilter
def format_sources(value, arg=None):
    soup = bs4.BeautifulSoup(value, "html.parser")
    for sup in soup.find_all("sup"):
        if arg:
            sup.wrap(soup.new_tag("a", href=f"#zdroje_{arg}"))
        else:
            sup.wrap(soup.new_tag("a", href="#zdroje"))
    return str(soup)


@register.filter(is_safe=True)
@stringfilter
def strip_sup(value):
    soup = bs4.BeautifulSoup(value, "html.parser")
    for sup in soup.find_all("sup"):
        sup.decompose()
    return str(soup)


@register.filter
def dictitem(dictionary, key):
    return dictionary.get(key)


@register.filter(is_safe=True)
@stringfilter
def style_h4(value):
    soup = bs4.BeautifulSoup(value, "html.parser")
    for h4 in soup.find_all("h4"):
        h4["class"] = "pl-11 font-bold text-xl mb-4"
    return str(soup)
