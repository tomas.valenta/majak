import pytest

from ..constants import (
    AGE_18_29,
    AGE_30_49,
    AGE_50_64,
    AGE_65_PLUS,
    CHILDLESS,
    MATURE,
    OCCUPATION_BUSINESS,
    OCCUPATION_PARENTING,
    OCCUPATION_RETIRED,
    OCCUPATION_STUDENT,
    OCCUPATION_WORKING,
    PARENTS,
    SENIORS,
    STUDENTS,
    WEALTH_AVERAGE,
    WEALTH_BAD,
    WEALTH_GOOD,
    WORKING_SENIORS,
)
from ..utils import get_archetype

ALL_AGES = (AGE_18_29, AGE_30_49, AGE_50_64, AGE_65_PLUS)
ALL_OCCUPATIONS = (
    OCCUPATION_WORKING,
    OCCUPATION_BUSINESS,
    OCCUPATION_PARENTING,
    OCCUPATION_RETIRED,
    OCCUPATION_STUDENT,
)
ALL_BUT_STUDENT_OCCUPATIONS = (
    OCCUPATION_WORKING,
    OCCUPATION_BUSINESS,
    OCCUPATION_PARENTING,
    OCCUPATION_RETIRED,
)
ALL_WEALTHS = (WEALTH_BAD, WEALTH_AVERAGE, WEALTH_GOOD)
ALL_KIDS = (True, False)


@pytest.mark.parametrize("age", (AGE_18_29, AGE_30_49))
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
@pytest.mark.parametrize("occupation", ALL_BUT_STUDENT_OCCUPATIONS)
def test_get_archetype__childless(age, wealth, occupation):
    selection = {
        "kids": False,
        "age": age,
        "wealth": wealth,
        "occupation": occupation,
        "topics": [],
    }
    assert get_archetype(selection) == CHILDLESS


@pytest.mark.parametrize("age", (AGE_18_29, AGE_30_49))
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
@pytest.mark.parametrize("occupation", ALL_BUT_STUDENT_OCCUPATIONS)
def test_get_archetype__parents(age, wealth, occupation):
    selection = {
        "kids": True,
        "age": age,
        "wealth": wealth,
        "occupation": occupation,
        "topics": [],
    }
    assert get_archetype(selection) == PARENTS


@pytest.mark.parametrize("kids", ALL_KIDS)
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
@pytest.mark.parametrize("occupation", ALL_BUT_STUDENT_OCCUPATIONS)
def test_get_archetype__mature(kids, wealth, occupation):
    selection = {
        "age": AGE_50_64,
        "kids": kids,
        "wealth": wealth,
        "occupation": occupation,
        "topics": [],
    }
    assert get_archetype(selection) == MATURE


@pytest.mark.parametrize("kids", ALL_KIDS)
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
@pytest.mark.parametrize(
    "occupation", (OCCUPATION_BUSINESS, OCCUPATION_PARENTING, OCCUPATION_RETIRED)
)
def test_get_archetype__seniors(kids, wealth, occupation):
    selection = {
        "age": AGE_65_PLUS,
        "kids": kids,
        "wealth": wealth,
        "occupation": occupation,
        "topics": [],
    }
    assert get_archetype(selection) == SENIORS


@pytest.mark.parametrize("kids", ALL_KIDS)
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
def test_get_archetype__working_seniors(kids, wealth):
    selection = {
        "age": AGE_65_PLUS,
        "kids": kids,
        "wealth": wealth,
        "occupation": OCCUPATION_WORKING,
        "topics": [],
    }
    assert get_archetype(selection) == WORKING_SENIORS


@pytest.mark.parametrize("age", ALL_AGES)
@pytest.mark.parametrize("kids", ALL_KIDS)
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
def test_get_archetype__students(age, kids, wealth):
    selection = {
        "age": age,
        "kids": kids,
        "wealth": wealth,
        "occupation": OCCUPATION_STUDENT,
        "topics": [],
    }
    assert get_archetype(selection) == STUDENTS


@pytest.mark.parametrize("age", ALL_AGES)
@pytest.mark.parametrize("kids", ALL_KIDS)
@pytest.mark.parametrize("wealth", ALL_WEALTHS)
@pytest.mark.parametrize("occupation", ALL_OCCUPATIONS)
def test_get_archetype__not_missing_combination(age, kids, wealth, occupation):
    selection = {
        "age": age,
        "kids": kids,
        "wealth": wealth,
        "occupation": occupation,
        "topics": [],
    }
    assert get_archetype(selection) is not None
