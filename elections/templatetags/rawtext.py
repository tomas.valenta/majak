from django import template

register = template.Library()


@register.filter
def rawtext(source_text_block) -> str:
    return str(source_text_block)
