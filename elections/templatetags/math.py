from django import template

register = template.Library()


@register.filter
def add(first_number, second_number) -> int:
    return first_number + second_number
