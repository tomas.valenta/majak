from wagtail.blocks import (
    CharBlock,
    ListBlock,
    PageChooserBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.images.blocks import ImageChooserBlock

from shared.blocks import CandidateBlock as SharedCandidateBlockMixin
from shared.blocks import CandidateListBlock as SharedCandidateListBlockMixin
from shared.blocks import (
    CandidateSecondaryListBlock as SharedCandidateSecondaryListBlockMixin,
)
from shared.blocks import SecondaryCandidateBlock as SharedSecondaryCandidateBlockMixin


class ElectionsCarouselBlock(StructBlock):
    desktop_image = ImageChooserBlock(
        label="Obrázek na pozadí (desktop)",
        help_text="Pokud není vybráno video, ukáže se na desktopu.",
    )

    mobile_image = ImageChooserBlock(
        label="Obrázek (mobil)",
        help_text="Pokud je vybrán, ukáže se místo videa na mobilu.",
        required=False,
    )

    video_url = URLBlock(
        label="URL videa",
        help_text="Pokud je vybráno, ukáže se na desktopech s povoleným autoplayem místo obrázku.",
        required=False,
    )

    mobile_line_1 = TextBlock(label="První mobilní řádek")
    mobile_line_2 = TextBlock(label="Druhý mobilní řádek")

    class Meta:
        template = "styleguide2/includes/molecules/menus/elections/carousel.html"
        icon = "form"
        label = "Carousel"


class CandidateBlock(SharedCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka", page_type=["elections.ElectionsCandidatePage"]
    )


class SecondaryCandidateBlock(SharedSecondaryCandidateBlockMixin):
    page = PageChooserBlock(
        label="Stránka", page_type=["elections.ElectionsCandidatePage"]
    )


class CandidateListBlock(SharedCandidateListBlockMixin):
    candidates = ListBlock(
        CandidateBlock(),
        required=False,
        label=" ",  # Hacky way to show no label
    )


class CandidateSecondaryListBlock(SharedCandidateSecondaryListBlockMixin):
    candidates = ListBlock(
        SecondaryCandidateBlock(),
        min_num=0,
        default=[],
        label="Kandidáti",
    )

    class Meta:
        template = "styleguide2/includes/organisms/candidates/elections/candidate_secondary_list.html"


class CalendarBlock(StructBlock):
    heading = CharBlock(label="Nadpis")

    class Meta:
        template = "styleguide2/includes/organisms/main_section/elections/calendar.html"
        icon = "calendar"
        label = "Kalendář"


class PersonFaqAnswerBlock(StructBlock):
    question = CharBlock(label="Otázka")

    answer = TextBlock(label="Odpověď")

    class Meta:
        icon = "form"
        label = "Odpověď kandidáta na otázku"


class PersonFaqAnswersBlock(StructBlock):
    section_name = CharBlock(
        label="Název sekce", help_text="Např. 'Otevřenost a transparentnost'"
    )

    topic = CharBlock(label="Téma", help_text="Např. 'téma Markéty Gregorové'")

    person_page = PageChooserBlock(
        label="Stránka kandidáta", page_type=["elections.ElectionsCandidatePage"]
    )

    image = ImageChooserBlock(
        label="Obrázek",
        help_text="Pokud není vybrán, použije se obrázek ze stránky kandidáta",
        required=False,
    )

    questions = ListBlock(PersonFaqAnswerBlock(label="Odpověď"), label="Otázky")

    class Meta:
        icon = "form"
        label = "Odpovědi kandidáta na otázky"
        template = "styleguide2/includes/organisms/faq/faq_answer.html"
