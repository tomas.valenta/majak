# Generated by Django 5.0.4 on 2024-05-10 10:35

import wagtail.blocks
import wagtail.fields
from django.db import migrations

import shared.blocks


class Migration(migrations.Migration):
    dependencies = [
        ("elections", "0027_alter_electionshomepage_content"),
    ]

    operations = [
        migrations.AlterField(
            model_name="electionshomepage",
            name="footer_other_links",
            field=wagtail.fields.StreamField(
                [
                    (
                        "other_links",
                        wagtail.blocks.StructBlock(
                            [
                                ("title", wagtail.blocks.CharBlock(label="Titulek")),
                                (
                                    "list",
                                    wagtail.blocks.ListBlock(
                                        shared.blocks.LinkBlock,
                                        label="Seznam odkazů",
                                    ),
                                ),
                            ]
                        ),
                    )
                ],
                blank=True,
                verbose_name="Odkazy v zápatí webu",
            ),
        ),
    ]
