# Generated by Django 5.0.7 on 2024-08-09 12:58

import modelcluster.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("elections", "0054_electionsarticlespage_displayed_shared_tags_and_more"),
        ("shared", "0010_alter_octopusperson_photo"),
    ]

    operations = [
        migrations.AlterField(
            model_name="electionsarticlespage",
            name="displayed_shared_tags",
            field=modelcluster.fields.ParentalManyToManyField(
                blank=True,
                related_name="+",
                to="shared.sharedtag",
                verbose_name="Sdílecí",
            ),
        ),
    ]
