# Generated by Django 4.1.10 on 2024-01-19 18:45

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("elections", "0018_alter_electionshomepage_content"),
    ]

    operations = [
        migrations.AddField(
            model_name="electionsarticlepage",
            name="show_initial_image",
            field=models.BooleanField(
                default=True,
                help_text="Pokud je tato volba zaškrtnutá, obrázek nastavený u tohoto článku se automaticky vloží do prvního odstavce.",
                verbose_name="Ukázat obrázek v textu",
            ),
        ),
    ]
