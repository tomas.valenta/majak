from pirates.models import AbstractUser


class User(AbstractUser):
    def get_username(self):
        """Used in wagtail templates"""
        return self.email or self.sso_id
