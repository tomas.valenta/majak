from wagtail.users.forms import UserCreationForm, UserEditForm

DISABLED_FIELDS = ["sso_id", "first_name", "last_name", "email"]


class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in DISABLED_FIELDS:
            self.fields[name].disabled = True


class CustomUserEditForm(UserEditForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name in DISABLED_FIELDS:
            self.fields[name].disabled = True
