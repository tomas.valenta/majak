from django.db import models
from django.utils.translation import gettext_lazy
from wagtail import blocks
from wagtail.admin.panels import CommentPanel, FieldPanel, HelpPanel, MultiFieldPanel
from wagtail.fields import StreamField
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Page
from wagtailmetadata.models import MetadataPageMixin

from shared.models import ExtendedMetadataHomePageMixin, ExtendedMetadataPageMixin
from tuning import admin_help

RICH_TEXT_FEATURES = [
    "h2",
    "h3",
    "h4",
    "h5",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
]


class MenuItemBlock(blocks.StructBlock):
    name = blocks.CharBlock(label="název")
    page = blocks.PageChooserBlock(
        label="stránka",
        page_type=["green_deal.GreenDealHomePage", "green_deal.GreenDealSubPage"],
    )

    class Meta:
        label = "stránka"


class FaqBlock(blocks.StructBlock):
    question = blocks.TextBlock()
    answer = blocks.RichTextBlock()


class ColumnsTextBlock(blocks.StructBlock):
    left_text = blocks.RichTextBlock(label="levý sloupec", features=RICH_TEXT_FEATURES)
    right_text = blocks.RichTextBlock(
        label="pravý sloupec", features=RICH_TEXT_FEATURES
    )

    class Meta:
        label = "text dva sloupce"
        icon = "doc-full"


class ColumnsFaqBlock(blocks.StructBlock):
    left_text = blocks.RichTextBlock(label="levý sloupec", features=RICH_TEXT_FEATURES)
    faqs = blocks.ListBlock(FaqBlock(), label="faq")


class NewsBlock(blocks.StructBlock):
    header = blocks.CharBlock()
    perex = blocks.RichTextBlock(features=RICH_TEXT_FEATURES)
    img = ImageChooserBlock()
    link = blocks.URLBlock()
    date = blocks.DateBlock()


class StudiesBlock(blocks.StructBlock):
    header = blocks.CharBlock()
    text = blocks.RichTextBlock(features=RICH_TEXT_FEATURES)

    class Meta:
        label = "Text 1 sloupec"
        icon = "doc-full"


class GreenDealHomePage(ExtendedMetadataHomePageMixin, MetadataPageMixin, Page):
    ### FIELDS
    content = StreamField(
        [
            ("text_2_columns", ColumnsTextBlock()),
            ("text_1_column", StudiesBlock()),
            ("FAQ_columns", ColumnsFaqBlock()),
            ("news", blocks.ListBlock(NewsBlock(), label="aktuality")),
        ],
        verbose_name="obsah stránky",
        blank=True,
        use_json_field=True,
    )
    # settings
    matomo_id = models.IntegerField(
        "Matomo ID pro sledování návštěvnosti", blank=True, null=True
    )
    top_menu = StreamField(
        [("item", MenuItemBlock())],
        verbose_name="horní menu",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("content"),
    ]

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(admin_help.build(admin_help.IMPORTANT_TITLE)),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    settings_panels = [
        FieldPanel("matomo_id"),
        FieldPanel("title_suffix"),
        FieldPanel("meta_title_suffix"),
        FieldPanel("top_menu"),
        CommentPanel(),
    ]

    ### RELATIONS

    subpage_types = [
        "green_deal.GreenDealSubPage",
    ]

    ### OTHERS

    class Meta:
        verbose_name = "Zelená dohoda pro regiony"

    @property
    def root_page(self):
        return self


class GreenDealSubPage(Page, ExtendedMetadataPageMixin, MetadataPageMixin):
    ### FIELDS
    content = StreamField(
        [
            ("text_2_columns", ColumnsTextBlock()),
            ("text_1_column", StudiesBlock()),
            ("FAQ_columns", ColumnsFaqBlock()),
            ("news", blocks.ListBlock(NewsBlock(), label="aktuality")),
        ],
        verbose_name="obsah stránky",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("content"),
    ]

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(admin_help.build(admin_help.IMPORTANT_TITLE)),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    settings_panels = [CommentPanel()]

    ### RELATIONS

    parent_page_types = [
        "green_deal.GreenDealHomePage",
    ]
    subpage_types = [
        "green_deal.GreenDealSubPage",
    ]

    ### OTHERS

    class Meta:
        verbose_name = "Zelená dohoda pro regiony"

    @property
    def root_page(self):
        if not hasattr(self, "_root_page"):
            self._root_page = (
                self.get_ancestors().type(GreenDealHomePage).specific().last()
            )
        return self._root_page
