// JavaScript Document
// addon
$(document).ready(function()
{
  // only small screens

    $('.zdroje .item a').click(function(e) {
      var el = $(this).parent().find('div.hide');
      if(el.is(':hidden')) el.slideDown(); else el.slideUp();
    });
    $('.zdroje .item h3 a').click(function(e) {
      var el = $(this).parent().parent().find('div.hide');
      if(el.is(':hidden')) el.slideDown(); else el.slideUp();
    });

});

// PARALAX
$(window).scroll(function() {
  var scrolled = $(window).scrollTop()
  $('.parallax').each(function(index, element) {
    var initY = $(this).offset().top
    var height = $(this).height()
    var endY  = initY + $(this).height()

    // Check if the element is in the viewport.
    var visible = isInViewport(this)
    if(visible) {
      var diff = scrolled - initY
      var ratio = Math.round((diff / height) * 100)
      $(this).css('background-position','left ' + parseInt(-(ratio * 1.5)) + 'px')
    }
  })
})

//// SMOOTH SCROLLING TO ANCHOR
$(window).on('load', function()
{
	var pos = location.href.lastIndexOf("#");
	if(pos > 0){
		var aid = location.href.substring(pos+1); //console.log(aid);
		if(aid && aid.length !== 0){
			var ael = $('#'+aid); //alert(ael.offset().top);
			$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
			$("#"+aid+" a.readmore").trigger( "click" );
		}
	}
	$("#qm0 ul a").click(function(e) {
		var url = $(this).attr("href");
		var view = url.substr(url.lastIndexOf('/') + 1);
		view = view.split('#')[0]; //console.log('view:'+view);
		var thisview = location.pathname.substr(location.pathname.lastIndexOf('/') + 1);
		thisview = thisview.split('#')[0]; //console.log('thisview:'+thisview);
		if(view.indexOf(thisview) > 0)
			e.preventDefault();
		//var aid = window.location.hash.substr(1);
		var aid = url.substring(url.indexOf("#")+1); //alert(aid);
		if(aid && aid.length !== 0){
			var ael = $('#'+aid); //alert(ael.offset().top);
			$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
			$("#"+aid+" a.readmore").trigger( "click" );
		}
	});
	$("#anchorlist a").click(function(e) {//console.log('scroll');
		e.preventDefault();
		var aid = $(this).attr('href');
		var ael = $(aid);
		$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
	});
});
//////// add anchor on mobile
$(document).ready(function()
{
  // only small screens
  if($(window).width() <= 700){
    $('.head_menu .topmenu li a').each(function(){
      var oldUrl = $(this).attr("href"); // Get current url
      var newUrl = oldUrl + "#content"; // Create new url
      $(this).attr("href", newUrl);
    });
  }
});
/////////////////////
///// INCREMENT EFFECT
/*
$(document).ready(function()
{
  var number = 0;
  var target = $('.js_counter_up').text();
  var counter = setInterval(function() {
      console.log(target);
      $('.js_counter_up').text(number);
      if (number >= target) clearInterval(counter);
      number++;
  }, 30);

});
*/
//Kontaktni formular
$(document).ready(function()
{
  $('#kontakt label').on('click', function(){
    $(this).animate({top: '0.5em', fontSize: '75%'}, 300, 'swing');
  });
  $('#kontakt input:text, #kontakt textarea').on('focusout', function(){
    if($(this).val() == '') $(this).prev().animate({top: '2.5em', fontSize: '90%'}, 300, 'swing');
  }).on('click', function(){
    $(this).prev().animate({top: '0.5em', fontSize: '75%'}, 300, 'swing');
  });

});

// STICKY NAV
var scr = 0;
$(window).scroll(function() {
    if ($(this).scrollTop() > 250 && $( window ).width() > 100){
		if(scr == 0) {$('#sticky_menu').addClass("sticky"); $('#sticky_menu').css('top', '-65px'); scr = 1; $('#sticky_menu').stop().animate({top:'0px'},400, "easeOutCirc", function(){$("#sticky_menu").removeAttr("style");}); }
        else $('#sticky_menu').addClass("sticky");

    }
    else{
        $('#sticky_menu').removeClass("sticky");
		    scr = 0;
    }
});

// MENU FROM ANCHORS
function getNavigation()
{
	$('.navi').each(function() {
		var scrollto = $(this);
		var html = $.parseHTML('<a href="javascript:void(0)" class="alink">' + $(this).children(':first').text() + '</a>');
		$('#navigation').append(html);
		$(html).click(function(){
			$('body').animate({scrollTop: (scrollto.offset().top - 80)},'slow');
		});
    });
}

//// READ MORE
$(document).ready(function()
{
	$("#historie a.readmore, #history a.readmore").click(function(event) {
		event.preventDefault();
		var el = $(this).prev();
		var img = $(this).parent().find('img');
		var h = el.prop('scrollHeight'); //console.log(img);
		var hei = ""+h+"px";
		if( el.height() > 53){
			el.animate({height: "53px"}, 500);
			//img.css("max-height","190px");
			if(lang == 'cz') $(this).text('Podrobnosti');
			if(lang == 'en') $(this).text('Read more');
		}
		else{
			el.animate({height: hei}, 500);
			//img.css("max-height","inherit");
			if(lang == 'cz') $(this).text('Zavřít');
			if(lang == 'en') $(this).text('Close');
		}
	});

	$("#aktuality-all a.readmore").click(function(event) {
		event.preventDefault();
		var el = $(this).prev();
		var h = el.prop('scrollHeight'); //console.log(img);
		var hei = ""+h+"px";
		if( el.height() > 54){
			el.animate({height: "54px"}, 500);
			$(this).toggleClass('readmore readmoreup');
			if(lang == 'cz') $(this).text('Více');
			if(lang == 'en') $(this).text('Read more');
		}
		else{
			el.animate({height: hei}, 500);
			$(this).toggleClass('readmore readmoreup');
			//img.css("max-height","inherit");
			if(lang == 'cz') $(this).text('Zavřít');
			if(lang == 'en') $(this).text('Close');
		}
	});

});
// SCROLL UP
/*
$(document).ready(function(){
	var bt = '';
	if(lang == 'cz') bt = 'Nahoru';
	if(lang == 'en') bt = 'Up';
	var el = $('<a href="javascript: void(0);" class="scrollup">'+bt+'</a>');
	$('.sekce').append(el);

	$("a.scrollup").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

});
*/
// LAZY LOADING
$(document).ready(function()
{
  $('#statisticke-udaje').Lazy({
    beforeLoad: function(element) {
      $('.statsLeft').hide();
      $('.middle').hide();
      $('.statsRight').hide();
    },
    statsLoader: function(element,response){
      var number = 0;
      var target = $('.js_counter').text();
      var counter = setInterval(function() {
          console.log(target);
          $('.js_counter').text(number);
          if (number >= target) clearInterval(counter);
          number++;
      }, 50);

      setTimeout(function() {
            //element.show('fade',1200);
            $('.statsLeft').show('slide',1200);
            $('.middle').show('fade',1500);
            $('.statsRight').show('slide',{direction:'right'},1200);
            response(true);
        }, 100);
    }
  });

  $('#galerie_cont').Lazy();

});
/*
$(document).ready(function()
{
	//$('#promo').slideDown('slow');
	$('#promo').animate({
      top: '70'
    }, 1000, 'swing');
});
*/
//// SMOOTH SCROLLING TO ANCHOR
$(window).on('load', function()
{
	var pos = location.href.lastIndexOf("#");
	if(pos > 0){
		var aid = location.href.substring(pos+1); //console.log(aid);
		if(aid && aid.length !== 0){
			var ael = $('#'+aid); //alert(ael.offset().top);
			$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
			$("#"+aid+" a.readmore").trigger( "click" );
		}
	}
	$("#qm0 ul a, .mainmenu a").click(function(e) {
		var url = $(this).attr("href");
		var view = url.substr(url.lastIndexOf('/') + 1);
		view = view.split('#')[0]; //console.log('view:'+view);
		var thisview = location.pathname.substr(location.pathname.lastIndexOf('/') + 1);
		thisview = thisview.split('#')[0]; //console.log('thisview:'+thisview);
		if(view.indexOf(thisview) > 0)
			e.preventDefault();
		//var aid = window.location.hash.substr(1);
		var aid = url.substring(url.indexOf("#")+1); //alert(aid);
		if(aid && aid.length !== 0){
			var ael = $('#'+aid); //alert(ael.offset().top);
			$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
			$("#"+aid+" a.readmore").trigger( "click" );
		}
	});
	$("#anchorlist a").click(function(e) {//console.log('scroll');
		e.preventDefault();
		var aid = $(this).attr('href');
		var ael = $(aid);
		$('body').animate({scrollTop: (ael.offset().top - 80)},'slow');
	});
});
