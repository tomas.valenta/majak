// JavaScript Document
// addon


/////////////////////
function validate(){
  var notice = '';
	if(!$('#souhlas').is(':checked') ) {
    if (lang == null){
      notice = 'Musíte potvrdit souhlas se zpracováním osobních údajů!';
    }
    else{
      switch(lang)
      {
      case 'cz': notice = 'Musíte potvrdit souhlas se zpracováním osobních údajů!'; break;
      case 'en': notice = 'You must agree with processing of your personal data!'; break;
      case 'de': notice = 'Sie müssen der Verarbeitung Ihrer persönlichen Daten zustimmen!'; break;
      default:   notice = 'Musíte potvrdit souhlas se zpracováním osobních údajů!'; break;
      }
    }
    $('#souhlas').focus();
	}
	if(notice != '') { alert (notice); return false;}
	else return true;
}
// RESPONSIVE MENU
$(document).ready(function()
{
	$('#mob_menu_open').click(function() {
		var x = $(this);
		$(this).addClass('rotate');
		$(this).animate({ opacity: 0 }, 250);
		$('#mob_menu_close').removeClass('hide').addClass('rotate_back').animate({ opacity: 1 }, 150);
		setTimeout(function(){x.addClass('hide').removeClass('rotate').css('opacity',1)},250);

		$('.head_menu').animate({ right: '-5%' }, 500);
	});
	$('#mob_menu_close').click(function() {
		var x = $(this);
		$(this).removeClass('rotate_back');
		$(this).animate({ opacity: 0 }, 250);
		$('#mob_menu_open').removeClass('hide').removeClass('rotate').animate({ opacity: 1 }, 250);
		setTimeout(function(){x.addClass('hide').removeClass('rotate_back').css('opacity',1)},250);

		$('.head_menu').animate({ right: '-110%' }, 500);
	});

  // only small screens
  /*if($(window).width() <= 600){
    // show menu on swipe to left
    $('#header').swipeDetector()
    .on("swipeLeft.sd swipeRight.sd", function(event) {
      //swipeLeft.sd swipeRight.sd swipeUp.sd swipeDown.sd
      if (event.type === "swipeLeft") {
        var x = $('#mob_menu_open');
    		x.addClass('rotate');
    		x.animate({ opacity: 0 }, 250);
    		$('#mob_menu_close').removeClass('hide').addClass('rotate_back').animate({ opacity: 1 }, 150);
    		setTimeout(function(){x.addClass('hide').removeClass('rotate').css('opacity',1)},250);

    		$('.head_menu').animate({ right: '-4%' }, 500);
      }
      if (event.type === "swipeRight") {
        var x = $('#mob_menu_close');
        x.removeClass('rotate_back');
        x.animate({ opacity: 0 }, 250);
        $('#mob_menu_open').removeClass('hide').removeClass('rotate').animate({ opacity: 1 }, 250);
        setTimeout(function(){x.addClass('hide').removeClass('rotate_back').css('opacity',1)},250);

        $('.head_menu').animate({ right: '-104%' }, 500);
      }
    });
  }*/
});
/*
$(document).ready(function()
{
	$('#mob_menu_open').click(function() {
		var x = $(this);
		x.addClass('rotate');
		x.animate({ opacity: 0 }, 250);
		$('#mob_menu_close').removeClass('hide').addClass('rotate_back').animate({ opacity: 1 }, 150);
		setTimeout(function(){x.addClass('hide').removeClass('rotate').css('opacity',1)},250);

		if ($( window ).width() < 3650){
			$('#responsive_menu').css('height','auto').removeClass('hide');
			$('#responsive_menu .mainmenu').animate({ left: '0%' }, 500);
		}
		else
			$('#responsive_menu .mainmenu').animate({ left: '0%' }, 500);
	});
	$('#mob_menu_close').click(function() {
		var x = $(this);
		x.removeClass('rotate_back');
		x.animate({ opacity: 0 }, 250);
		$('#mob_menu_open').removeClass('hide').removeClass('rotate').animate({ opacity: 1 }, 250);
		setTimeout(function(){x.addClass('hide').removeClass('rotate_back').css('opacity',1)},250);
    if ($( window ).width() > 3650){
		    $('#responsive_menu .mainmenu').animate({ left: '-60%' }, 500, function(){$('#responsive_menu').addClass('hide'); });
    }
    else $('#responsive_menu .mainmenu').animate({ left: '-115%' }, 500, function(){$('#responsive_menu').css('height','1px').addClass('hide'); });
	});
	if ($( window ).width() < 1100){
		$('#qm0 li').click(function(event) {
			event.preventDefault();
			$(this).find('ul').first().css('left','0');
		});
		$('#qm0 li a').dblclick(function() {
			window.location.href = $(this).attr('href');
		});
	}

});
*/
////////////////////////// POPUPS
function popup(id) {
	var popupBox = $(id);
	//Fade in the Popup and add close button
	$(popupBox).fadeIn(300);
	//Set the center alignment padding + border
	var popMargTop = ($(popupBox).height() + 24) / 2;
	var popMargLeft = ($(popupBox).width() + 24) / 2;
	$(popupBox).css({
		'margin-top' : -popMargTop,
		'margin-left' : -popMargLeft
	});
	// Add the mask to body
	$('body').append('<div id="mask"></div>');
	$('#mask').fadeIn(300);
	return false;
};
function popup_cart() {
	$('#cart_content').fadeIn(300);
	return false;
};
$(function() {
	$(document.body).on('click', '#mask', function() {
	  $('#mask , .popup').fadeOut(300 , function() {
		$('#mask').remove();
	});
	return false;
	});
	$(document.body).on('click', '#intro', function() {
	  $('#mask , .popup').fadeOut(300 , function() {
		$('#mask').remove();
	});
	return false;
	});
	// When clicking on the button close or the mask layer the popup closed
	$(document.body).on('click', 'a.close', function() {
	  $('#mask , .popup').fadeOut(300 , function() {
		$('#mask').remove();
	});
	return false;
	});

	$(document.body).on('click', 'a.cart_close',function() {
	  $('#cart_content').fadeOut(300 , function() {
	});
	return false;
	});
});

function toggleContent(name,n) {
	var i,t='',el = document.getElementById(name);
	if (!el.origCont) el.origCont = el.innerHTML;

	for (i=0;i<n;i++) t += el.origCont;
	el.innerHTML = t;
}
function makeSafe ( kde )
{
  var co = new Array ("- "," ","+",",","á","ä","č","ď","ň","é","ě","í","ĺ","ľ","Ł","ó","ô","ő","ö","ŕ","š","ť","ú","ů","ű","ü","ý","ř","ž","Á","Ä","Č","Ď","É","Ě","Í","Ĺ","Ľ","Ň","Ó","Ô","Ł","Ö","Ŕ","Ř","Š","Ť","Ú","Ů","Ű","Ü","Ý","Ł","Ž");
  var cim = new Array("","-","-","","a","a","c","d","n","e","e","i","l","l","n","o","o","o","o","r","s","t","u","u","u","u","y","r","z","a","a","c","d","e","e","i","l","l","n","o","o","o","o","r","r","s","t","u","u","u","u","y","r","z");
  var safe = str_replace(co, cim, kde);
  safe = safe.toLowerCase();
  safe = safe.trim();
  return safe;
}
function str_replace (search, replace, subject, count) {
    // http://kevin.vanzonneveld.net
    // +   bugfixed by: Oleg Eremeev
    // %          note 1: The count parameter must be passed as a string in order
    // %          note 1:  to find a global variable in which the result will be given
    // *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
    // *     returns 1: 'Kevin.van.Zonneveld'
    // *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
    // *     returns 2: 'hemmo, mars'

    var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0,
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = r instanceof Array, sa = s instanceof Array;
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }

    for (i=0, sl=s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j=0, fl=f.length; j < fl; j++) {
            temp = s[i]+'';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length-s[i].length)/f[j].length;}
        }
    }
    return sa ? s : s[0];
}
function closeAllExcept(e){
	var menu = document.getElementById('menu');
	var lis = menu.getElementsByTagName('li');
	var isLong = '';
	for(i=0; i<lis.length; i++){
		if (lis[i] != e){
			if (lis[i].className.search(/long/) > -1) isLong = ' long';
			if (lis[i].className.search(/open/) > -1) lis[i].className = "close"+isLong;
			if (lis[i].className.search('open') > -1) alert(lis[i].id);
		}
	}
}
function closeAll(){
	var menu = document.getElementById('menu');
	var lis = menu.getElementsByTagName('li');
	var isLong = '';
	for(i=0; i<lis.length; i++){
		if (lis[i].className.search(/long/) > 0) isLong = ' long';
		if (lis[i].className.search(/open/) > 0) lis[i].className = "close"+isLong;
	}
	document.cookie = 'menu=0; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/';
}

function menu_switch(e,ev) {//alert(e.parentNode.parentNode.id);
	if(e.nodeName == 'LI'){
		var d = new Date();
		d.setTime(d.getTime()+(15*60*1000));
		var trida; var ext = '';
		trida = e.className;
		arr = trida.split(' ');
		trida = arr[0];
		for (i=1; i<arr.length; i++){
			ext += ' '+arr[i];
		}
		// disable bubbling
		if (window.event) {
			ev.cancelBubble = true;
		}
		else {
			ev.stopPropagation();
		}

		if ((trida=="close" || trida=="open")) {
   	  		e.className = (trida=="open") ? "close"+ext : "open"+ext;
			if (trida == "close") {document.cookie = 'menu='+e.id+'; expires='+d.toUTCString()+'; path=/'; }
			else document.cookie = 'menu='+e.id+'; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/';
			closeAllExcept(e);
			return false;
  		}
	}
	else
	return true;
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function setCookie(name,val,hours) {
	var d = new Date();
	d.setTime(d.getTime()+(hours*60*1000));
	if (hours)
		document.cookie = name+'='+val+'; expires='+d.toUTCString()+'; path=/';
	else
		document.cookie = name+'='+val+'; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/';
	return false;
}

function onBlur(el) {
    if (el.value == '') {
        el.value = el.defaultValue;
    }
}
function onFocus(el) {
    if (el.value == el.defaultValue) {
        el.value = '';
    }
}
function isInViewport(node) {
  var rect = node.getBoundingClientRect()
  return (
    (rect.height > 0 || rect.width > 0) &&
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  )
}
/*window.onload = function() {
	if($.cookie('popState') != 'shown' && ($('#intro .introimg').length || $('#intro .introimg')[0].length)){
        popup('#intro');
		var date = new Date();
		var minutes = 15;
		date.setTime(date.getTime() + (minutes * 60 * 1000));
        $.cookie('popState', 'shown', { expires: date});
    }
	//document.cookie = "popState=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
};*/


//////////////////////////////////// AJAX

function send_xmlhttprequest(state_change, method, url, content, headers) {
    var xmlhttp = (XMLHttpRequest ? new XMLHttpRequest : (ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : false));
    if (!xmlhttp) {
        return false;
    }
    xmlhttp.open(method, url);
    xmlhttp.onreadystatechange = function () {
        state_change(xmlhttp);
    };
    if (headers) {
        for (var key in headers) {
            xmlhttp.setRequestHeader(key, headers[key]);
        }
    }
    xmlhttp.send(content);
    return true;
}
