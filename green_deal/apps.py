from django.apps import AppConfig


class GreenDealConfig(AppConfig):
    name = "green_deal"
