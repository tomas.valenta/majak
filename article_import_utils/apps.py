from django.apps import AppConfig


class ArticleImportUtilsConfig(AppConfig):
    name = "article_import_utils"
