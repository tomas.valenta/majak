#!/usr/bin/make -f

PYTHON = python
VENV   = .venv
PORT   = 8006

help:
	@echo "Setup:"
	@echo "  venv           Setup virtual environment"
	@echo "  install        Install dependencies to venv"
	@echo "  install-hooks  Install pre-commit hooks"
	@echo "  hooks          Run pre-commit hooks manually"
	@echo "  upgrade        Upgrade requirements"
	@echo ""
	@echo "Application:"
	@echo "  run            Run the application on port ${PORT}"
	@echo "  shell          Run Django shell"
	@echo "  worker         Run Celery worker"
	@echo ""
	@echo "Database:"
	@echo "  migrations     Generate migrations"
	@echo "  migrate        Run migrations"
	@echo ""
	@echo "Testing:"
	@echo "  test           Run tests"
	@echo "  coverage       Coverage report"
	@echo ""

venv: .venv/bin/python
.venv/bin/python:
	${PYTHON} -m venv ${VENV}

install: venv
	${VENV}/bin/pip install -r requirements/base.txt -r requirements/dev.txt

install-hooks:
	pre-commit install --install-hooks

hooks:
	pre-commit run -a

run: venv
	${VENV}/bin/python manage.py runserver ${PORT}

shell: venv
	${VENV}/bin/python manage.py shell_plus

worker: venv
	${VENV}/bin/celery -A majak worker --pool solo -E -l INFO

migrations: venv
	${VENV}/bin/python manage.py makemigrations

migrate: venv
	${VENV}/bin/python manage.py migrate

test:
	${VENV}/bin/pytest

coverage:
	${VENV}/bin/pytest --cov --cov-report term-missing

upgrade:
	(cd requirements && pip-compile -U base.in)
	(cd requirements && pip-compile -U dev.in)
	(cd requirements && pip-compile -U production.in)


.PHONY: help venv install install-hooks hooks run shell upgrade
.PHONY: migrations migrate test coverage

# EOF
