from django.apps import AppConfig


class InstagramUtilsConfig(AppConfig):
    name = "instagram_utils"
