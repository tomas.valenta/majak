import datetime
import logging
import mimetypes
import uuid

from django.db import models

logger = logging.getLogger()


def get_current_datetime() -> datetime.datetime:
    return datetime.datetime.now(tz=datetime.timezone.utc)


logger = logging.getLogger(__name__)


def get_instagram_image_path(instance, filename) -> str:
    mimetypes_instance = mimetypes.MimeTypes()
    guessed_type = mimetypes_instance.guess_type(filename, strict=False)[0]

    extension = ""

    if guessed_type is not None:
        for mapper in mimetypes_instance.types_map_inv:
            if guessed_type not in mapper:
                continue

            extension = mapper[guessed_type]

            if isinstance(extension, list):
                extension = extension[0]

            break

    return f"instagram/{uuid.uuid4()}{extension}"


class InstagramPost(models.Model):
    """
    Model representing an Instgram post obtained from its API through the
    update_instagram management command.
    """

    remote_id = models.CharField(
        verbose_name="ID Postu",
        max_length=64,
        unique=True,
    )
    timestamp = models.DateTimeField(
        verbose_name="Datum a čas vytvoření", default=get_current_datetime
    )

    author_name = models.CharField(
        verbose_name="Jméno autora",
        max_length=64,
    )
    author_username = models.CharField(
        verbose_name="Username autora",
        max_length=64,
    )

    caption = models.TextField(
        verbose_name="Popis",
        blank=True,
        null=True,
    )
    image = models.ImageField(
        verbose_name="Obrázek",
        upload_to=get_instagram_image_path,
    )
    url = models.URLField(
        verbose_name="Odkaz",
        blank=True,
        null=True,
    )

    def __str__(self) -> str:
        return f"@{self.author_username} - {self.caption}"

    class Meta:
        ordering = ("timestamp",)


class InstagramMixin(models.Model):
    def save(self, *args, **kwargs):
        """
        Update Instagram posts when saved, assume they are declared
        """

        from .services import InstagramDownloadService

        try:
            service = InstagramDownloadService()
            service.perform_update()
        except Exception:
            logger.error("Instagram post update failed", exc_info=True)

        super().save(*args, **kwargs)

    class Meta:
        abstract = True
