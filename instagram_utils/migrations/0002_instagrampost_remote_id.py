# Generated by Django 4.1.6 on 2023-04-05 17:18

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("instagram_utils", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="instagrampost",
            name="remote_id",
            field=models.CharField(default="", max_length=64, verbose_name="ID Postu"),
            preserve_default=False,
        ),
    ]
