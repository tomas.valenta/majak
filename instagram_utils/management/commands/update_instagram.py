from django.core.management.base import BaseCommand

from ...services import InstagramDownloadService


class Command(BaseCommand):
    def handle(self, *args, **options):
        service = InstagramDownloadService()
        service.perform_update()

        self.stdout.write("\nInstagram post update finished.")
