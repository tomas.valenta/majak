from django.urls import re_path

from .views import serve_colored_marker, serve_marker_shadow

urlpatterns = [
    re_path(
        r"^marker/(?P<color>.{6})/((?P<number>\d+)/)?$",
        serve_colored_marker,
        name="maps_utils_serve_colored_marker",
    ),
    re_path(
        r"^marker-shadow/$",
        serve_marker_shadow,
        name="maps_utils_serve_marker_shadow",
    ),
]
