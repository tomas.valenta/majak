from functools import cache
from os import path
from typing import Optional

from django.http import HttpResponse


@cache
def get_marker_template(template_name: str):
    marker_path = path.join(
        path.dirname(__file__), f"templates/maps_utils/{template_name}"
    )

    with open(marker_path) as f:
        return f.read()


def serve_colored_marker(request, color: str, number: Optional[int] = None):
    template = get_marker_template(
        "marker-numbered.svg.template" if number is not None else "marker.svg.template"
    )
    value = template.replace("$FILLCOLOR$", color)

    if number is not None:
        value = value.replace("$FILLNUMBER$", number)

    return HttpResponse(
        value,
        content_type="image/svg+xml",
    )


def serve_marker_shadow(request):
    return HttpResponse(
        get_marker_template("marker-shadow.svg"),
        content_type="image/svg+xml",
    )
