from django.conf import settings

MAP_STYLES = (
    ("osm-mapnik", "OSM Mapnik"),
    # These two are not reliable.
    # ("stamen-toner", "Stamen Toner"),
    # ("stamen-terrain", "Stamen Terrain"),
    ("stadia-osm-bright", "Stadia OSM Bright"),
    ("stadia-outdoors", "Stadia Outdoors"),
    ("mapbox-streets", "Mapbox Streets"),
    ("mapbox-outdoors", "Mapbox Outdoors"),
    ("mapbox-light", "Mapbox Light"),
    ("mapbox-dark", "Mapbox Dark"),
    ("mapbox-satellite", "Mapbox Satellite"),
    ("mapbox-pirate", "Mapbox Pirate Theme"),
)

DEFAULT_MAP_STYLE = "osm-mapnik"

SUPPORTED_FEATURE_TYPES = (
    "Point",
    "MultiPoint",
    "Polygon",
    "MultiPolygon",
    "LineString",
    "MultiLineString",
)


TILE_SERVER_CONFIG = {
    "url": settings.MAPS_UTILS_MAPPROXY_URL,
}
