from django.apps import AppConfig


class MapsUtilsConfig(AppConfig):
    name = "maps_utils"
