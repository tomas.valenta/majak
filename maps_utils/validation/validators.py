import json
from os import path
from typing import Mapping, Optional, Sequence

import fastjsonschema


def _validator(schema_file_path: str):
    """Build a JSON schema validator."""
    with open(schema_file_path) as f:
        schema = f.read()

    return fastjsonschema.compile(json.loads(schema))


feature_validator = _validator(
    path.join(path.dirname(__file__), "schemas/Feature.json")
)
feature_collection_validator = _validator(
    path.join(path.dirname(__file__), "schemas/FeatureCollection.json")
)


def check_single_feature(
    value: Mapping, allowed_feature_types: Optional[Sequence[str]] = None
):
    """Validate single feature mapping against schema and make sure feature type is among allowed ones."""
    try:
        feature_validator(value)
    except fastjsonschema.JsonSchemaException as exc:
        raise ValueError(
            f"Hodnota není platný GeoJSON Feature objekt: '{str(exc)}'."
        ) from exc

    if allowed_feature_types:
        if value["geometry"]["type"] not in allowed_feature_types:
            raise ValueError(
                f"Typ GeoJSON feature objektu '{value['geometry']['type']}' není podporován. Musí být jeden z následujících: {', '.join(allowed_feature_types)}."
            )


def normalize_geojson_feature_collection(
    value: str, allowed_types: Optional[Sequence[str]] = None
):
    """Given a string, verify it's valid GeoJSON of a `FeatureCollection` type and normalize it.

    :param value: string to verify & normalize
    :return: cleaned geojson value
    """
    try:
        parsed = json.loads(value)
        # Strip whitespace & normalize
        value = json.dumps(parsed)
    except json.JSONDecodeError as exc:
        raise ValueError("Hodnota není platný JSON.") from exc
    else:
        try:
            feature_collection_validator(parsed)
        except fastjsonschema.JsonSchemaException as exc:
            raise ValueError(
                f"Hodnota není platný GeoJSON FeatureCollection objekt: '{str(exc)}'."
            ) from exc

        for feature in parsed["features"]:
            check_single_feature(feature, allowed_feature_types=allowed_types)

    return value


def normalize_geojson_feature(
    value: str, allowed_types: Optional[Sequence[str]] = None
):
    """Given a string, verify it's valid GeoJSON of a `Feature` type and normalize it.

    :param value: string to verify & normalize
    :return: cleaned geojson value
    """
    try:
        parsed = json.loads(value)
        # Strip whitespace & normalize
        value = json.dumps(parsed)
    except json.JSONDecodeError as exc:
        raise ValueError("Hodnota není platný JSON.") from exc
    else:
        check_single_feature(parsed, allowed_feature_types=allowed_types)

    return value
