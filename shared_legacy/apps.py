from django.apps import AppConfig


class SharedLegacyConfig(AppConfig):
    name = "shared_legacy"
