// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window["pdfjs-dist/build/pdf"];

[...document.querySelectorAll("[data-name='inline-pdf']")].forEach(
  async (script) => {
    if (!script.dataset.init) {
        const parentNode = script.parentNode;
        const docUrl = script.dataset.url;
        const pdf = await pdfjsLib.getDocument(docUrl).promise;
        for (let pageNumber = 1; pageNumber <= pdf.numPages; pageNumber++) {
          const page = await pdf.getPage(pageNumber);
          const viewport = page.getViewport({ scale: 1.5 });

          // Prepare canvas using PDF page dimensions
          const canvas = document.createElement("canvas");
          canvas.setAttribute("aria-hidden", "true");
          const context = canvas.getContext("2d");
          canvas.height = viewport.height;
          canvas.width = viewport.width;

          // Render PDF page into canvas context
          await page.render({
            canvasContext: context,
            viewport: viewport,
          });
          parentNode.appendChild(canvas);
        }
        script.dataset.init = true;
    }
  }
);
