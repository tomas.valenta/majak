Vue.use(VueFormulate, {
  plugins: [VueFormulateI18n.cs.default],
  locale: "cs",
});

const errorDataKey = "mailtrainerror";
const errorMessage = ["Vyskytla se chyba při zpracování požadavku, omlouváme se"];

Vue.options.data ||= {};
Vue.options.data.mailtrainerrors ||= () => [];
Vue.options.methods ||= {};
Vue.options.methods.mailtrainsubmit = async function (data) {
  $(this.$el).data(errorDataKey, "");
  Vue.options.data.mailtrainerrors = () => [];
  $("[data-success]", this.$el).hide();

  const csrftoken = $("[name='csrfmiddlewaretoken']", this.$el).val();
  const headers = new Headers();
  headers.append('X-CSRFToken', csrftoken);

  const createError = () => {
    $(this.$el).data(errorDataKey, errorMessage);
    Vue.options.data.mailtrainerrors = function() {
      return $(this.$el).data(errorDataKey) || [];
    };
  };

  try {
    const response = await fetch("/newsletter/", {
      method: "POST",
      body: JSON.stringify(data),
      headers: headers,
      credentials: "include",
    });
    if (response.status >= 400) {
      createError();
    } else {
      $("[data-success]", this.$el).show();
    }
  } catch {
    createError();
  }
};
