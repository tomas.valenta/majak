import typing
from datetime import datetime

from django.contrib.syndication.views import Feed
from django.template.loader import render_to_string


class LatestArticlesFeed(Feed):
    def __init__(self, articles_page_model, article_page_model, *args, **kwargs):
        self.articles_page_model = articles_page_model
        self.article_page_model = article_page_model

        return super().__init__(*args, **kwargs)

    def get_object(self, request, id: int):
        return self.articles_page_model.objects.get(id=id)

    def title(self, obj) -> str:
        return obj.title

    def link(self, obj) -> str:
        return obj.get_full_url()

    def description(self, obj) -> str:
        return obj.perex

    def items(self, obj) -> list:
        return obj.materialize_shared_articles_query(
            obj.append_all_shared_articles_query(
                self.article_page_model.objects.child_of(obj)
            )[:32]
        )

    def item_title(self, item) -> str:
        return item.title

    def item_description(self, item) -> str:
        return render_to_string(
            "styleguide2/feed_item_description.html",
            {"item": item},
        )

    def item_pubdate(self, item) -> datetime:
        return datetime(
            item.date.year,
            item.date.month,
            item.date.day,
            12,
            0,
        )

    def item_author_name(self, item) -> str:
        if item.author:
            return item.author

        if item.author_page and item.author_page.title:
            return item.author_page.title

        return ""

    def item_categories(self, item) -> list:
        return item.get_tags

    def item_link(self, item) -> str:
        return item.get_full_url()

    def item_enclosure_url(self, item) -> typing.Union[None, str]:
        if item.image is None:
            return None

        return item.image.get_rendition("format-webp").full_url

    item_enclosure_mime_type = "image/webp"

    def item_enclosure_length(self, item) -> int:
        return item.image.file_size
