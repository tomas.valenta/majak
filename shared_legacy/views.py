from django.http import HttpResponse
from wagtail.models import Site


def page_not_found(request, exception=None):
    try:
        site = Site.find_for_request(request)
        root_page = site.root_page.specific
    except:
        root_page = None

    if root_page and hasattr(root_page, "get_404_response"):
        return root_page.get_404_response(request)

    return HttpResponse(
        "<h1>Stránka nenalezena</h1><a href='/'>pokračovat na úvod</a>",
        status=404,
    )
