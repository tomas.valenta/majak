from wagtail import blocks
from wagtail.blocks import (
    CharBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock

from .base import MenuItemBlock as MenuItemBlockBase

# Mixins (or used as such)


PROGRAM_RICH_TEXT_FEATURES = [
    "h3",
    "h4",
    "h5",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
    "superscript",
    "subscript",
    "strikethrough",
    "blockquote",
    "embed",
]


class CTAMixin(StructBlock):
    button_link = URLBlock(label="Odkaz tlačítka")
    button_text = CharBlock(label="Text tlačítka")

    class Meta:
        icon = "doc-empty"
        label = "Výzva s odkazem"


class LinkBlock(StructBlock):
    text = CharBlock(label="Název")
    link = URLBlock(label="Odkaz")

    class Meta:
        icon = "link"
        label = "Odkaz"


# Navbar


class MainMenuItemBlock(MenuItemBlockBase):
    title = blocks.CharBlock(
        label="Titulek",
        help_text="Pokud není odkazovaná stránka na Majáku, použij možnost zadání samotné adresy níže.",
        required=True,
    )

    class Meta:
        label = "Položka v menu"


class NavbarMenuItemBlock(CTAMixin):
    class Meta:
        label = "Tlačítko"
        template = "styleguide2/includes/molecules/navbar/additional_button.html"


class SocialLinkBlock(LinkBlock):
    icon = CharBlock(
        label="Ikona",
        help_text="Seznam ikon - https://styleguide.pirati.cz/latest/?p=viewall-atoms-icons <br/>"
        "Název ikony zadejte bez tečky na začátku",
    )  # TODO CSS class name or somthing better?

    class Meta:
        icon = "link"
        label = "Odkaz"


# Articles


class NewsBlock(StructBlock):
    title = CharBlock(
        label="Titulek",
        help_text="Nejnovější články se načtou automaticky",
    )
    description = TextBlock(label="Popis")

    class Meta:
        icon = "doc-full-inverse"
        label = "Novinky"


class ArticleQuoteBlock(StructBlock):
    quote = CharBlock(label="Citace")
    autor_name = CharBlock(label="Jméno autora")

    class Meta:
        icon = "user"
        label = "Blok citace"
        template = "styleguide2/includes/legacy/article_quote_block.html"


class ArticleDownloadBlock(StructBlock):
    file = DocumentChooserBlock(label="Stáhnutelný soubor")

    class Meta:
        icon = "user"
        label = "Blok stáhnutelného dokumentu"
        template = "styleguide2/includes/molecules/blocks/download_block.html"


# People


class TwoTextColumnBlock(StructBlock):
    text_column_1 = RichTextBlock(label="První sloupec textu")
    text_column_2 = RichTextBlock(label="Druhý sloupec textu")

    class Meta:
        icon = "doc-full"
        label = "Text ve dvou sloupcích"


class PersonContactBoxBlock(StructBlock):
    title = CharBlock(label="Titulek")
    image = ImageChooserBlock(label="Ikona")
    subtitle = CharBlock(label="Podtitulek")

    class Meta:
        icon = "mail"
        label = "Kontakty"


class PersonContactBlock(StructBlock):
    position = CharBlock(label="Název pozice", required=False)
    # email, phone?
    person = PageChooserBlock(
        label="Osoba",
        page_type=["main.MainPersonPage"],
    )

    class Meta:
        icon = "user"
        label = "Osoba s volitelnou pozicí"


# Footer


class OtherLinksBlock(StructBlock):
    title = CharBlock(label="Titulek")
    list = ListBlock(LinkBlock, label="Seznam odkazů s titulkem")

    class Meta:
        icon = "link"
        label = "Odkazy"
        template = "main/blocks/article_quote_block.html"


class ProgramBlockPopout(StructBlock):
    title = CharBlock(label="Titulek vyskakovacího bloku")
    content = RichTextBlock(
        label="Obsah",
        features=PROGRAM_RICH_TEXT_FEATURES,
    )

    class Meta:
        icon = "date"
        label = "Blok programu"


class ProgramPopoutCategory(StructBlock):
    name = CharBlock(label="Název")
    icon = ImageChooserBlock(label="Ikona", required=False)

    description = RichTextBlock(label="Popis", required=False)

    point_list = ListBlock(ProgramBlockPopout(), label="Jednotlivé bloky programu")

    class Meta:
        icon = "date"
        label = "Kategorie programu"


class ProgramGroupBlockPopout(StructBlock):
    categories = ListBlock(ProgramPopoutCategory(), label="Kategorie programu")

    class Meta:
        icon = "date"
        label = "Vyskakovací program"


class FlipCardBlock(StructBlock):
    bg_color = CharBlock(label="Barva pozadí", default="FEC900")

    image = ImageChooserBlock(label="Obrázek", required=False)

    title = TextBlock(label="Nadpis", help_text="Řádkování je manuální.")

    content = RichTextBlock(label="Obsah")

    button_text = CharBlock(
        label="Nadpis tlačítka",
        help_text="Pokud není vyplněn, tlačítko se neukáže.",
        required=False,
    )
    button_url = CharBlock(label="Odkaz tlačítka", required=False)

    class Meta:
        icon = "view"
        label = "Obracecí karta"
        template = "styleguide2/includes/molecules/boxes/flip_card_box.html"
