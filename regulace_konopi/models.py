from django.db import models
from django.utils.translation import gettext_lazy
from wagtail import blocks
from wagtail.admin.panels import CommentPanel, FieldPanel, HelpPanel, MultiFieldPanel
from wagtail.fields import StreamField
from wagtail.images.blocks import ImageChooserBlock
from wagtail.models import Page
from wagtailmetadata.models import MetadataPageMixin

from shared.models import ExtendedMetadataHomePageMixin, ExtendedMetadataPageMixin
from tuning import admin_help

SUPPORT_CHOICES = ((1, "Pro"), (0, "Proti"), (-1, "Nevíme"))

RICH_TEXT_FEATURES = [
    "h2",
    "h3",
    "h4",
    "h5",
    "bold",
    "italic",
    "ol",
    "ul",
    "hr",
    "link",
    "document-link",
    "image",
    "superscript",
    "subscript",
    "strikethrough",
    "blockquote",
]


class MenuItemBlock(blocks.StructBlock):
    name = blocks.CharBlock(label="název")
    page = blocks.PageChooserBlock(
        label="stránka",
        page_type=["regulace_konopi.RegkonHomePage", "regulace_konopi.RegkonSubPage"],
    )

    class Meta:
        label = "stránka"


class MepBlock(blocks.StructBlock):
    name = blocks.CharBlock(label="jméno")
    approved = blocks.ChoiceBlock(choices=SUPPORT_CHOICES, label="hlasoval")

    class Meta:
        label = "Poslanec"


class PartyBlock(blocks.StructBlock):
    name = blocks.CharBlock(label="název")
    mep = blocks.ListBlock(MepBlock())

    class Meta:
        label = "Politická strana"


class FaqBlock(blocks.StructBlock):
    question = blocks.TextBlock()
    answer = blocks.RichTextBlock()


class ThreeColumnsBlock(blocks.StructBlock):
    first_column = blocks.RichTextBlock(features=RICH_TEXT_FEATURES)
    second_column = blocks.RichTextBlock(features=RICH_TEXT_FEATURES)


class RegkonHomePage(ExtendedMetadataHomePageMixin, MetadataPageMixin, Page):
    ### FIELDS
    content = StreamField(
        [
            ("title", blocks.CharBlock(label="nadpis", icon="title")),
            ("text", blocks.RichTextBlock(label="text", features=RICH_TEXT_FEATURES)),
            (
                "text_centered",
                blocks.RichTextBlock(label="text-center", features=RICH_TEXT_FEATURES),
            ),
            (
                "text_justify",
                blocks.RichTextBlock(label="text-justify", features=RICH_TEXT_FEATURES),
            ),
            (
                "carousel",
                blocks.ListBlock(
                    ImageChooserBlock(label="obrázek"), label="carousel", icon="image"
                ),
            ),
            (
                "carousel_text",
                blocks.ListBlock(
                    blocks.RichTextBlock(features=RICH_TEXT_FEATURES),
                    label="carousel-text",
                    icon="paragraph",
                ),
            ),
        ],
        verbose_name="obsah stránky",
        blank=True,
        use_json_field=True,
    )
    # settings
    matomo_id = models.IntegerField(
        "Matomo ID pro sledování návštěvnosti", blank=True, null=True
    )
    top_menu = StreamField(
        [("item", MenuItemBlock())],
        verbose_name="horní menu",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    content_panels = Page.content_panels + [
        FieldPanel("content"),
    ]

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(admin_help.build(admin_help.IMPORTANT_TITLE)),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    settings_panels = [
        FieldPanel("matomo_id"),
        FieldPanel("title_suffix"),
        FieldPanel("meta_title_suffix"),
        FieldPanel("top_menu"),
        CommentPanel(),
    ]

    ### RELATIONS

    subpage_types = [
        "regulace_konopi.RegkonSubPage",
    ]

    ### OTHERS

    class Meta:
        verbose_name = "Regulace konpí"

    @property
    def root_page(self):
        return self


class RegkonSubPage(Page, ExtendedMetadataPageMixin, MetadataPageMixin):
    ### FIELDS

    content = StreamField(
        [
            ("title", blocks.CharBlock(label="nadpis", icon="title")),
            ("text", blocks.RichTextBlock(label="text", features=RICH_TEXT_FEATURES)),
            (
                "text_centered",
                blocks.RichTextBlock(label="text-center", features=RICH_TEXT_FEATURES),
            ),
            (
                "text_justify",
                blocks.RichTextBlock(label="text-justify", features=RICH_TEXT_FEATURES),
            ),
            (
                "carousel",
                blocks.ListBlock(
                    ImageChooserBlock(label="obrázek"), label="carousel", icon="image"
                ),
            ),
            (
                "carousel_text",
                blocks.ListBlock(
                    blocks.RichTextBlock(features=RICH_TEXT_FEATURES),
                    label="carousel-text",
                    icon="image",
                ),
            ),
            (
                "FAQ",
                blocks.ListBlock(
                    FaqBlock(template="regulace_konopi/blocks/faq.html"), label="faq"
                ),
            ),
            ("meps", PartyBlock(template="regulace_konopi/blocks/party.html")),
            (
                "text_3_columns",
                blocks.StreamBlock(
                    [
                        (
                            "column",
                            blocks.RichTextBlock(
                                label="column", features=RICH_TEXT_FEATURES
                            ),
                        )
                    ],
                    max_num=3,
                    icon="doc-full",
                ),
            ),
        ],
        verbose_name="obsah stránky",
        blank=True,
        use_json_field=True,
    )

    ### PANELS

    promote_panels = [
        MultiFieldPanel(
            [
                FieldPanel("slug"),
                FieldPanel("seo_title"),
                FieldPanel("search_description"),
                FieldPanel("search_image"),
                HelpPanel(
                    admin_help.build(
                        admin_help.NO_SEO_TITLE, admin_help.NO_SEARCH_IMAGE
                    )
                ),
            ],
            gettext_lazy("Common page configuration"),
        ),
    ]

    content_panels = Page.content_panels + [
        FieldPanel("content"),
    ]

    settings_panels = [CommentPanel()]

    ### RELATIONS

    parent_page_types = [
        "regulace_konopi.RegkonHomePage",
        "regulace_konopi.RegkonSubPage",
    ]
    subpage_types = ["regulace_konopi.RegkonSubPage"]

    ### OTHERS

    class Meta:
        verbose_name = "Podstránka"

    @property
    def root_page(self):
        if not hasattr(self, "_root_page"):
            self._root_page = self.get_ancestors().type(RegkonHomePage).specific().get()
        return self._root_page

    def get_meta_image(self):
        return self.search_image or self.root_page.get_meta_image()
