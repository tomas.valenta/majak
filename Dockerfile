FROM python:3.11

RUN apt-get update && apt-get install -y \
    # requirements for OpenCV
    libgl1-mesa-dev \
    # requirements for WeasyPrint
    python3-cffi \
    # requirements for Wand (GIF image resizing)
    libmagickwand-dev \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

COPY requirements requirements/
RUN pip install -r requirements/base.txt -r requirements/production.txt

COPY . .

RUN bash -c 'adduser --disabled-login --quiet --gecos app app &&  \
             chmod -R o+r /app/ && \
             mkdir /app/media_files && \
             mkdir /app/static_files && \
             chown -R app:app /app/media_files && \
             chown -R app:app /app/static_files && \
             chmod o+x /app/run.sh'
USER app

ENV DJANGO_SETTINGS_MODULE "majak.settings.production"

# fake values for required env variables used to run collectstatic during build
RUN DJANGO_SECRET_KEY=x DATABASE_URL=postgres://x/x DJANGO_ALLOWED_HOSTS=x \
    OIDC_RP_CLIENT_ID=x OIDC_RP_CLIENT_SECRET=x OIDC_RP_REALM_URL=x \
    python manage.py collectstatic

EXPOSE 8000

CMD ["bash", "run.sh"]
