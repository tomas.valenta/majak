from django.db import models

from shared.storages import OverwriteStorage


class TweetQueryset(models.QuerySet):
    def username(self, username):
        return self.filter(author_username=username)

    def username_list(self, username_list):
        return self.filter(author_username__in=username_list)


class Tweet(models.Model):
    """
    Model pro ukládání Tweetů getnutých v manage commandu update_tweets z API Twitteru.
    ID Tweetu ukládáme jako string, protože i limit BigInt je stejného řádu jako ID
    nejnovějších Tweetů (2022).
    """

    author_img = models.ImageField(
        storage=OverwriteStorage, upload_to="twitter_accounts"
    )
    author_name = models.CharField(max_length=128, default="Piráti")
    author_username = models.CharField(max_length=128, default="PiratskaStrana")

    image = models.ImageField(null=True, upload_to="twitter")
    text = models.TextField()
    twitter_id = models.CharField(max_length=32, unique=True)

    objects = TweetQueryset.as_manager()
